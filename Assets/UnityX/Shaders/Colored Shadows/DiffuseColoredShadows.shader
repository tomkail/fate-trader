﻿//http://kylehalladay.com/blog/tutorial/bestof/2014/05/16/Coloured-Shadows-In-Unity.html
Shader "UnityX/Colored Shadows/Diffuse" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_ShadowColor ("Shadow Color", Color) = (1,1,1,1)
		_DiffuseVal ("Diffuse", Range (0.01, 1)) = 1
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
	
		CGPROGRAM
		#pragma surface surf LambertColoredShadows
		
		sampler2D _MainTex;
		fixed4 _Color;
		float _DiffuseVal;
		fixed4 _ShadowColor;
		
		#include "ColoredShadows.cginc"
		
		struct Input {
			float2 uv_MainTex;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	
	Fallback "VertexLit"
}