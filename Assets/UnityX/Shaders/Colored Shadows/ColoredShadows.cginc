﻿//http://kylehalladay.com/blog/tutorial/bestof/2014/05/16/Coloured-Shadows-In-Unity.html

fixed4 LightingLambertColoredShadows (SurfaceOutput s, half3 lightDir, half atten) {
	
	fixed diff = max (0, dot (s.Normal, lightDir));

	fixed4 c;
	c.rgb = s.Albedo * _LightColor0.rgb * (diff * atten * 2);
	
	//shadow colorization
	c.rgb += _ShadowColor.xyz * max(0.0,(1.0-(diff * atten * 2))) * _DiffuseVal;
	c.a = s.Alpha;
	return c;
}