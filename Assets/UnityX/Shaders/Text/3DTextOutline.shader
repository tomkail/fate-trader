﻿// To use this shader, create a material using it, assign the texture of the font to it, and apply the font and the material to the text mesh renderer.
Shader "UnityX/GUI/3D Text Outline" { 
	Properties {
		_MainTex ("Font Texture", 2D) = "white" {}
		[HideInInspector]
		_Color ("Text Color", Color) = (1,1,1,1)
		_LineSize ("Size", Range(0,1)) = 1
		_LineColor ("Line Color", Color) = (1,1,1,.5)
	}
 
	SubShader {
		Tags {
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
		}
		Lighting Off
		Cull Off
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform fixed4 _Color;
			uniform float _LineSize;
			uniform float4 _LineColor;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color * _Color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				//float _LineThicknessX = _LineSize;
				//float _LineThicknessY = _LineSize
				//float _LineIntensity = _LineSize;
		
				float2 uv = i.texcoord;

				fixed4 col = i.color;
				col.a *= tex2D(_MainTex, uv).a;
				//return col;

				float h = 0;
				half4 outline = 0;

				half4 sample1 = tex2D(_MainTex, uv + float2(_LineSize,0.0));
				half4 sample2 = tex2D(_MainTex, uv + float2(-_LineSize,0.0));
				half4 sample3 = tex2D(_MainTex, uv + float2(.0,_LineSize));
				half4 sample4 = tex2D(_MainTex, uv + float2(.0,-_LineSize));
				//sample1 + sample2 + sample3 + sample4
				//if(col.a < h)
				//{
					if (sample1.a > h || sample2.a > h || sample3.a > h || sample4.a > h)
					{
						outline = _LineColor;
					}
				//}
				if(col.a < 0.1) {
					return outline;
				} else {
					return col;
				}
			}
			ENDCG
		}
	}
}