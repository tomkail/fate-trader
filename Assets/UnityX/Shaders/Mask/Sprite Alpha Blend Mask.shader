﻿Shader "UnityX/UI/Sprite Alpha Blend Mask" {
	Properties {
		[PerRendererData] _MainTex ("Texture", 2D) = "white" {}
		_MaskTex ("Mask Texture", 2D) = "white" {}
//		_Color ("Tint", Color) = (1,1,1,1)
	}

	Category {
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
		Fog { Mode Off }
 
	 	SubShader {
			Tags {"Queue"="Transparent" "RenderType"="Transparent"}
			CGPROGRAM
			#pragma surface surf Unlit alpha
			half4 LightingUnlit (SurfaceOutput s, half3 lightDir, half atten) {
				half4 c;
				c.rgb = s.Albedo;
				c.a = s.Alpha;
				return c;
			}

			sampler2D _MainTex;
			sampler2D _MaskTex;
			fixed4 _Color;
			
			float angleBetween(float2 a, float2 b) {
				return atan2(-(b.y - a.y), b.x - a.x) + (0.01745329 * 90);
			}
			
			struct Input {
				float2 uv_MainTex;
				float2 uv_MaskTex;
				float4 color : Color;
			};
			
			void surf (Input IN, inout SurfaceOutput o) {
				half4 tex = tex2D (_MainTex, IN.uv_MainTex);
				half4 backgroundTex = tex2D (_MaskTex, IN.uv_MaskTex);
				
				o.Albedo = 0.5 * tex.rgb * (2 * (backgroundTex.rgb));
				o.Alpha = IN.color.a * tex.a * backgroundTex.a;
			}
			ENDCG
		}
	}
}


//Shader "Tutorial/Textured Colored" {
//	Properties {
//		_Color ("Main Color", Color) = (1,1,1,0.5)
//		[PerRendererData] _MainTex ("Texture", 2D) = "white" { }
//		_AlphaTex ("Texture", 2D) = "white" {}
//	}
//	SubShader {
//		Tags {
//			"Queue"="Transparent" 
//			"IgnoreProjector"="True" 
//			"RenderType"="Transparent"
//			"PreviewType"="Plane"
//			"CanUseSpriteAtlas"="True"
//		}
//
//		Cull Off
//		Lighting Off
//		ZWrite Off
//		Fog { Mode Off }
//		Blend One OneMinusSrcAlpha
//
////		CGPROGRAM
////		#pragma surface surf Lambert vertex:vert
////		#pragma multi_compile DUMMY PIXELSNAP_ON
////
////		sampler2D _MainTex;
////		sampler2D _AlphaTex;
////		fixed4 _Color;
////
////		struct Input {
////			float2 uv_MainTex;
////			float2 uv2_AlphaTex;
////			fixed4 color;
////		};
////		
////		void vert (inout appdata_full v, out Input o) {
////			#if defined(PIXELSNAP_ON) && !defined(SHADER_API_FLASH)
////			v.vertex = UnityPixelSnap (v.vertex);
////			#endif
////			v.normal = float3(0,0,-1);
////			
////			UNITY_INITIALIZE_OUTPUT(Input, o);
////			o.color = v.color * _Color;
////		}
////
////		void surf (Input IN, inout SurfaceOutput o) {
////			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
//////			fixed4 alphaColor = tex2D(_AlphaTex, IN.uv2_AlphaTex);
////			
//////			o.Albedo = c.rgb * alphaColor.rgb * _Color.rgb;
////			o.Albedo = c.rgb * _Color.rgb;
////			o.Alpha = c.a * _Color.a;
////			
//////			alphaColor * IN.color;
////			
//////			half4 texcol = tex2D (_MainTex, i.uv);
//////	//			texcol.a = tex2D (_AlphaTex, i.uv).a;
//////				texcol.rgb *= tex2D (_AlphaTex, i.uv2).rgb;
//////				return texcol * _Color;
////		}
////		ENDCG
//	 
////		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
////		LOD 200
////		Blend SrcAlpha OneMinusSrcAlpha
////		ZTest Less
////		
////		Pass {
////			CGPROGRAM
////			#pragma vertex vert
////			#pragma fragment frag
////			#include "UnityCG.cginc"
////			
////			float4 _Color;
////			sampler2D _MainTex;
////			sampler2D _AlphaTex;
////			
////			struct v2f {
////			    float4 pos : SV_POSITION;
////			    float2 uv : TEXCOORD0;
////			    float2 uv2 : TEXCOORD1;
////			};
////			
////			float4 _MainTex_ST;
////			
////			v2f vert (appdata_base v)
////			{
////			    v2f o;
////			    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
////			    o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
////			    return o;
////			}
////			
////			half4 frag (v2f i) : COLOR
////			{
////			   half4 texcol = tex2D (_MainTex, i.uv);
////	//			texcol.a = tex2D (_AlphaTex, i.uv).a;
////				texcol.rgb *= tex2D (_AlphaTex, i.uv2).rgb;
////				return texcol * _Color;
////			}
////			ENDCG
////		}
//	}
// Fallback "VertexLit"
// } 