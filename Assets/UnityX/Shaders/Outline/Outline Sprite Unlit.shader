﻿Shader "UnityX/Outline/Outline Sprite Unlit"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		_GlowColor ("Glow Color", Color) = (1,1,1,1)
	    _GlowIntensity ("Glow", Range(0,1)) = 0.5

	    _OutlineColor ("Outline Color", Color) = (1,1,1,1)
	    _OutlineSize ("Outline Size", Range(0,1)) = 1
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float2 texcoord  : TEXCOORD0;
			};
			
			fixed4 _Color;
			fixed4 _GlowColor;
			fixed _GlowIntensity;
			fixed4 _OutlineColor;
			fixed _OutlineSize;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _AlphaTex;
			float _AlphaSplitEnabled;

			fixed4 SampleSpriteTexture (float2 uv)
			{
				fixed4 color = tex2D (_MainTex, uv);

#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
				if (_AlphaSplitEnabled)
					color.a = tex2D (_AlphaTex, uv).r;
#endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

				return color;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
//				c.rgb *= c.a;
//				return c;

				float _GlowStrength = 1+(_GlowIntensity * (_GlowIntensity*0.2));
				float _GlowSize = _GlowIntensity;
				
			    fixed4 glowC = c * _GlowColor;
			    float clampedGlowSize = clamp(glowC.a-(1-_GlowSize),0,1);
			    float glow = clampedGlowSize + ((_GlowStrength*_GlowStrength)-1)*clampedGlowSize;
			    
			    float outlineStrength = 50;
			    fixed4 outlineC = c * _OutlineColor;
			    float clampedOutlineSize = clamp(outlineC.a-(1-_OutlineSize),0,1);
			    float outline = outlineStrength*clampedOutlineSize;
			    
			    float total = outline + glow;
			    float outlineFraction = outline/total;
			    float glowFraction = glow/total;
			    fixed3 outlineColor = lerp(fixed3(0,0,0), outlineC.rgb, outline);
			    fixed3 glowColor = lerp(fixed3(0,0,0), glowC.rgb, glow);
			    c.rgb = lerp(outlineC.rgb, glowC.rgb, glowFraction);
	//		    o.Albedo = outlineColor + glowColor;
			    c.a = glow + outline;
			    //o.Alpha = glowC.a;
			    return c;
			}
		ENDCG
		}
	}
}
