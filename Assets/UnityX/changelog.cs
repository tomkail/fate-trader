/** \file
 * This file contains the changelog for Tom Kail's UnityX.
 */

/** \page changelog Changelog

- 1.0
	- Highlights
		- A highlight!
	- Fixes
		- A fix!
	- Changes
		- A change!
	- Bugs
		- A bug!
	- Note
		- Update really too small to be an update by itself, but I was updating the build scripts I use for the project and had to upload a new version because of technical reasons.


- x. releases are major rewrites or updates to the system.
- .x releases are quite big feature updates
- ..x releases are the most common updates, fix bugs, add some features etc.
- ...x releases are quickfixes, most common when there was a really bad bug which needed fixing ASAP.

 */