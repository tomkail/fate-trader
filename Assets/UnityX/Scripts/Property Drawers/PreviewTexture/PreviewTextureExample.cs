using UnityEngine;
using System.Collections;

public class PreviewTextureExample : MonoBehaviour
{
    [PreviewTexture(80)]
    public string textureURL = "https://www.hogehoge.com/image.png";
	// GUIStyle style = new GUIStyle();

    [PreviewTexture()]
    public Texture hoge;
}
