// https://github.com/anchan828/property-drawer-collection
// Copyright (C) 2014 Keigo Ando
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System;
#endif

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(PopupAttribute))]
public class PopupDrawer : PropertyDrawer
{
    private Action<int> setValue;
    private Func<int, int> validateValue;
    private string[] _list = null;

    private string[] list
    {
        get
        {
            if (_list == null)
            {
                _list = new string[popupAttribute.list.Length];
                for (int i = 0; i < popupAttribute.list.Length; i++)
                {
                    _list[i] = popupAttribute.list[i].ToString();
                }
            }
            return _list;
        }
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //This line allows validate and setvalue functions to be cached, which is probably great for performance, but copies the attribute when used on the same object
        // if (validateValue == null && setValue == null)
        SetUp(property);


        if (validateValue == null && setValue == null)
        {
            base.OnGUI(position, property, label);
            return;
        }

        int selectedIndex = list.IndexOf(property.stringValue);
        if(selectedIndex == -1) {
            selectedIndex = 0;
            setValue(selectedIndex);
        }

        for (int i = 0; i < list.Length; i++)
        {
            selectedIndex = validateValue(i);
            if (selectedIndex != 0)
                break;
        }

        EditorGUI.BeginChangeCheck();
        selectedIndex = EditorGUI.Popup(position, label.text, selectedIndex, list);
        if (EditorGUI.EndChangeCheck())
        {
            // property.stringValue = list[selectedIndex];
            setValue(selectedIndex);
        }
    }

    void SetUp(SerializedProperty property)
    {
        if (variableType == typeof(string))
        {

            validateValue = (index) =>
            {
                return property.stringValue == list[index] ? index : 0;
            };

            setValue = (index) =>
            {
                property.stringValue = list[index];
            };
        }
        else if (variableType == typeof(int))
        {

            validateValue = (index) =>
            {
                return property.intValue == Convert.ToInt32(list[index]) ? index : 0;
            };

            setValue = (index) =>
            {
                property.intValue = Convert.ToInt32(list[index]);
            };
        }
        else if (variableType == typeof(float))
        {
            validateValue = (index) =>
            {
                return property.floatValue == Convert.ToSingle(list[index]) ? index : 0;
            };
            setValue = (index) =>
            {
                property.floatValue = Convert.ToSingle(list[index]);
            };
        }

    }

    PopupAttribute popupAttribute
    {
        get { return (PopupAttribute)attribute; }
    }

    private Type variableType
    {
        get
        {
            return popupAttribute.list[0].GetType();
        }
    }
}

#endif