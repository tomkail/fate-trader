#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof(FolderPathAttribute))]
class FolderPathDrawer : PropertyDrawer {
	
	// Draw the property inside the given rect
	public override void OnGUI (Rect position , SerializedProperty property, GUIContent label) {
		
		// First get the attribute since it contains the range for the slider
		var attr = (FolderPathAttribute)this.attribute;
		
		if(property.propertyType == SerializedPropertyType.String) {
			var path = property.stringValue;
			var contentRect = EditorGUI.PrefixLabel( position, label );
			var textRect = contentRect;
			var buttonRect = contentRect;
			
			textRect.width -= 20;
			buttonRect.width = 20;
			buttonRect.x = textRect.xMax;
			
			path = EditorGUI.TextField( textRect, path );
			if(GUI.Button( buttonRect, "..." )) {
				string relativePath = "";
				if(attr.relativeTo == FolderPathAttribute.RelativeTo.Assets) {
					relativePath = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("/Assets"));
				} else if(attr.relativeTo == FolderPathAttribute.RelativeTo.Project) {
					relativePath = Application.dataPath;
				}
				path = relativePath+path;
				path = EditorUtility.OpenFolderPanel( "Select Folder", path, "" );
				
				path = path.Split(new string[]{relativePath}, System.StringSplitOptions.RemoveEmptyEntries)[0];
				if(path.IndexOf('/') == 0) path = path.Remove(0,1);
			}
			property.stringValue = path;
			
		} else {
			EditorGUI.LabelField( position, label.text, "Use FilePathDrawer with string." );
		}

	}
}
#endif