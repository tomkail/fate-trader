using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(SetCaseAttribute))]
public class SetCaseDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (!IsSupported(property))
        {
            return;
        }
        position.height = 16;
		string password = property.stringValue;
		password = EditorGUI.TextField(position, label, password);
		property.stringValue = SetCase(password);
    }

	string SetCase(string myString) {
		if(setCaseAttribute.caseType == SetCaseAttribute.CaseType.Upper) {
			return myString.ToUpper();
		} else if(setCaseAttribute.caseType == SetCaseAttribute.CaseType.Lower) {
			return myString.ToLower();
		} else {
			return myString;
		}
	}
	
    bool IsSupported(SerializedProperty property) {
        return property.propertyType == SerializedPropertyType.String;
    }
    

	SetCaseAttribute setCaseAttribute {
        get {
            return (SetCaseAttribute)attribute;
        }
    }

}
#endif