// https://github.com/anchan828/property-drawer-collection
// Copyright (C) 2014 Keigo Ando
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(FloatValuePopupAttribute))]
public class FloatValuePopupDrawer : PropertyDrawer
{
//    private Action<int> setValue;
//    private Func<int, int> validateValue;
	
	private string[] _stringList = null;
	private string[] stringList
	{
		get
		{
			if (_stringList == null)
			{
				_stringList = new string[popupAttribute.stringList.Length];
				for (int i = 0; i < popupAttribute.stringList.Length; i++)
				{
					_stringList[i] = popupAttribute.stringList[i];
				}
			}
			return _stringList;
		}
	}
	
	private float[] _floatList = null;
	private float[] floatList
	{
		get
		{
			if (_floatList == null)
			{
				_floatList = new float[popupAttribute.floatList.Length];
				for (int i = 0; i < popupAttribute.floatList.Length; i++)
				{
					_floatList[i] = popupAttribute.floatList[i];
				}
			}
			return _floatList;
		}
	}
	
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
		if(stringList.Length != floatList.Length) Debug.LogError("String and Float arrays must be of the same size.");
		
		int selectedIndex = floatList.IndexOf(property.floatValue);
        if(selectedIndex == -1) {
        	selectedIndex = MathX.ClosestIndex(_floatList, property.floatValue);
        }

		for (int i = 0; i < stringList.Length; i++)
        {
            if (selectedIndex != 0)
                break;
        }

        EditorGUI.BeginChangeCheck();
        selectedIndex = EditorGUI.Popup(position, label.text, selectedIndex, stringList);
        if (EditorGUI.EndChangeCheck())
        {
             property.floatValue = floatList[selectedIndex];			
        }
    }
	
	FloatValuePopupAttribute popupAttribute
    {
        get { return (FloatValuePopupAttribute)attribute; }
    }
}

//#endif