﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif 
public class OnChangeAttribute : PropertyAttribute
{
    public string[] callbackNames;

    public OnChangeAttribute(params string[] callbackNames)
    {
        this.callbackNames = callbackNames;
    }
}