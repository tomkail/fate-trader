﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(OnChangeAttribute))]
public class OnChangeDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginChangeCheck();
		EditorGUI.PropertyField(position, property, label);
		if (EditorGUI.EndChangeCheck())
		{
			if (IsMonoBehaviour(property))
			{
				MonoBehaviour mono = (MonoBehaviour)property.serializedObject.targetObject;

				foreach (var callbackName in onChangeAttribute.callbackNames)
				{
					mono.Invoke(callbackName, 0);
				}

			}
		}
	}

	bool IsMonoBehaviour(SerializedProperty property)
	{
		return property.serializedObject.targetObject.GetType().IsSubclassOf(typeof(MonoBehaviour));
	}

	OnChangeAttribute onChangeAttribute
	{
		get
		{
			return (OnChangeAttribute)attribute;
		}
	}
}
#endif