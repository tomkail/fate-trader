﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
#endif 

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(EnumButtonsAttribute))]
public class EnumButtonsDrawer : PropertyDrawer {
	private new EnumButtonsAttribute attribute {
		get {
			return (EnumButtonsAttribute)attribute;
		}
	}

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        if (property.propertyType == SerializedPropertyType.Enum) {
//			GUI.BeginGroup(position);
			EditorGUI.PrefixLabel(new Rect(position.x, position.y, EditorGUIUtility.labelWidth, position.height), label);
			int newVal = GUI.Toolbar (new Rect(position.x+EditorGUIUtility.labelWidth, position.y, position.width-EditorGUIUtility.labelWidth, position.height), property.enumValueIndex, property.enumNames);
			if(property.enumValueIndex != newVal) {
				property.enumValueIndex = newVal;
			}
//			float width = position.width/property.enumNames.Length;
//			float height = position.height;
//			for(int i = 0; i < property.enumNames.Length; i++) {
//				if(GUI.Button(new Rect(width * i, 0, width, height), property.enumNames[i])) {
//					property.enumValueIndex = i;
//				}
//			}
//			GUI.EndGroup();
        }
    }	
}

#endif