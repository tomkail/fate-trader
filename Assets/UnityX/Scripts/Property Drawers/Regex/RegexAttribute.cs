﻿using UnityEngine;
using System.Collections.Generic;

public class RegexAttribute : PropertyAttribute {
	
    public readonly string pattern;
    public readonly string helpMessage;
   
	public RegexAttribute (string pattern, string helpMessage) {
        this.pattern = pattern;
        this.helpMessage = helpMessage;
    }
    
	public RegexAttribute (RegexHelper.CommonRegexPattern commonRegex, string helpMessage) {
		this.pattern = RegexHelper.commonRegexPatterns[commonRegex];
		this.helpMessage = helpMessage;
	}
}