#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;


[CustomPropertyDrawer(typeof(SceneNameAttribute))]
public class SceneNameDrawer : PropertyDrawer
{
	private static float lastTimeCheckedSceneNames = -1;
	private static string[] allSceneNames;
	
    private SceneNameAttribute sceneNameAttribute
    {
        get
        {
            return (SceneNameAttribute)attribute;
        }
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        string[] sceneNames = GetSceneNames();

        if (sceneNames.Length == 0)
        {
            EditorGUI.LabelField(position, ObjectNames.NicifyVariableName(property.name), "No Scenes in build.");
            return;
        }

        int[] sceneNumbers = new int[sceneNames.Length];

        SetSceneNambers(sceneNumbers, sceneNames);

        if (!string.IsNullOrEmpty(property.stringValue))
            sceneNameAttribute.selectedValue = GetIndex(sceneNames, property.stringValue);

        sceneNameAttribute.selectedValue = EditorGUI.IntPopup(position, label.text, sceneNameAttribute.selectedValue, sceneNames, sceneNumbers);

        property.stringValue = sceneNames[sceneNameAttribute.selectedValue];
    }

    private string[] GetSceneNames()
    {
		if(sceneNameAttribute.findMethod == SceneNameAttribute.SceneFindMethod.AllInProject) {
			if(lastTimeCheckedSceneNames != Time.realtimeSinceStartup) {
				lastTimeCheckedSceneNames = Time.realtimeSinceStartup;
				allSceneNames = SceneManagerX.GetScenePathsInProject();
			}
			return allSceneNames;
		}
		
        List<EditorBuildSettingsScene> scenes = null;
		if(sceneNameAttribute.findMethod == SceneNameAttribute.SceneFindMethod.AllInBuild) scenes = EditorBuildSettings.scenes.ToList();
		else if(sceneNameAttribute.findMethod == SceneNameAttribute.SceneFindMethod.EnabledInBuild) scenes = EditorBuildSettings.scenes.Where(scene => scene.enabled).ToList();
        HashSet<string> sceneNames = new HashSet<string>();
        scenes.ForEach(scene =>
        {
            sceneNames.Add(scene.path.Substring(scene.path.LastIndexOf("/") + 1).Replace(".unity", string.Empty));
        });
        return sceneNames.ToArray();
    }
    
//    private string[] GetAllSceneNames () {
//		string[] files = System.IO.Directory.GetFiles(Application.dataPath, "*.scene");
//		DebugX.LogList (files);
//    }

    private void SetSceneNambers(int[] sceneNumbers, string[] sceneNames)
    {
        for (int i = 0; i < sceneNames.Length; i++)
        {
            sceneNumbers[i] = i;
        }
    }

    private int GetIndex(string[] sceneNames, string sceneName)
    {
        int result = 0;
        for (int i = 0; i < sceneNames.Length; i++)
        {
            if (sceneName == sceneNames[i])
            {
                result = i;
                break;
            }
        }
        return result;
    }
}
#endif