using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif 
public class ParticleSystemInfoAttribute : PropertyAttribute {
	public bool disableField = false;
	public ParticleSystemInfoAttribute () {}
	public ParticleSystemInfoAttribute (bool disableField) {this.disableField = disableField;}
}