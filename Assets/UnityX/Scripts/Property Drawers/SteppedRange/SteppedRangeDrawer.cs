#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof (SteppedRangeAttribute))]
public class SteppedRangeDrawer : PropertyDrawer {
	SteppedRangeAttribute steppedRangeAttribute { 
		get { 
			return ((SteppedRangeAttribute)attribute); 
		} 
	}
	
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		EditorGUI.Slider(position, property, steppedRangeAttribute.min, steppedRangeAttribute.max);
		property.floatValue = property.floatValue.RoundToNearest(steppedRangeAttribute.step);
    }
}
#endif