﻿using UnityEngine;
using System;
using System.Collections;
#if !UNITY_WINRT
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.IO;
using System.Net.Mime;
using System.Threading;
using System.ComponentModel;
#endif

public static class Email {

	#if !UNITY_WINRT
	public static MailMessage CreateEmail (string senderAddress, string senderDisplayName, string receiverAddress, string receiverDisplayName, string subject, string body) {
		MailAddress sender = new MailAddress(senderAddress, senderDisplayName);
		MailAddress receiver = new MailAddress(receiverAddress, receiverDisplayName);
		MailMessage message = new MailMessage(sender, receiver);

		message.Subject = subject;
		message.Body = body;
		message.IsBodyHtml = true;

		// message.Attachments.Add(new Attachment(Application.dataPath + "/../Screenshot.png", @"image/png"));

		MailAddress bcc = new MailAddress(senderAddress);
		message.Bcc.Add(bcc);

		return message;
	}
	
	public static void SendEmail (string senderAddress, string smtpHost, string smtpPassword, MailMessage message) {
		SmtpClient smtpServer = new SmtpClient(smtpHost);
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential(senderAddress, smtpPassword) as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { 
			return true; 
		};

		smtpServer.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
		smtpServer.SendAsync(message, "SendCompletedCallback");
		// If not async, use: smtpServer.Send(message);
	}

	private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e) {
		// Get the unique identifier for this asynchronous operation.
		String token = (string) e.UserState;

		if (e.Cancelled) {
			Debug.Log("[{0}] Send canceled. "+ token);
		}
		if (e.Error != null) {
			Debug.Log("[{0}] {1}" +" "+ token + " " + e.Error.ToString());
		} else {
			Debug.Log("Message sent.");
		}
	}
	#endif
}