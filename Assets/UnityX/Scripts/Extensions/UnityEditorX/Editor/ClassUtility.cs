﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;
using System; 
using System.IO;
using System.Text;

public class ClassUtility {

	public static void CreateRawCSharp(string name, string content) {
		string path = (FileX.SelectedAssetFolderPath());
		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/"+name + ".cs");
		
		using (StreamWriter outfile = new StreamWriter(assetPathAndName)) {
			outfile.WriteLine(content);
		}
		
		AssetDatabase.Refresh();
		EditorUtility.FocusProjectWindow ();
	}
	
	public static void Create(string name, string extends = "MonoBehaviour") {
		string path = (FileX.SelectedAssetFolderPath());
		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/"+name + ".cs");
		
//		if(File.Exists(assetPathAndName)){ // do not overwrite
//			Debug.LogError ("File already exists at path "+assetPathAndName+". Select a different folder to save, or choose a different file name");
//			return;
//		}
//		
//		Debug.Log("Creating Classfile: " + assetPathAndName);
		
		using (StreamWriter outfile = new StreamWriter(assetPathAndName)) {
			outfile.WriteLine("using UnityEngine;");
			outfile.WriteLine("using System.Collections;");
			outfile.WriteLine("");
			outfile.WriteLine("public class "+name+" : MonoBehaviour {");
			outfile.WriteLine("\t");
			outfile.WriteLine("\tvoid Start () {");
			outfile.WriteLine("\t\t");
			outfile.WriteLine("\t}");
			outfile.WriteLine("\t");         
			outfile.WriteLine("\t");
			outfile.WriteLine("\t");
			outfile.WriteLine("\tvoid Update () {");
			outfile.WriteLine("\t\t");
			outfile.WriteLine("\t}");
			outfile.WriteLine("}");
		}
		
		AssetDatabase.Refresh();
		EditorUtility.FocusProjectWindow ();
	}
}
#endif