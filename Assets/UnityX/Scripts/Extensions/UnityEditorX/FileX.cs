﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

public static class FileX {
	#if UNITY_EDITOR
	// From http://forum.unity3d.com/threads/how-to-get-currently-selected-folder-for-putting-new-asset-into.81359/	
	public static string SelectedAssetFolderPath(){
		string path = "Assets";
		foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
		{
			path = AssetDatabase.GetAssetPath(obj);
			if (File.Exists(path))
			{
				path = Path.GetDirectoryName(path);
			}
			break;
		}
		return path;
	}
	
	private static bool AttemptCreatePath(string path, bool relativeToCurrent = true){
		if(relativeToCurrent){
			path = Path.Combine(Directory.GetCurrentDirectory(), path);
		}
		
		if (Directory.Exists(path)) {
			return false;
		} else {
			Directory.CreateDirectory(path);
			AssetDatabase.Refresh();
			//OpenInFileBrowser(path);
			return true;
		}
	}
	
	
	public static Object AttemptCreateAsset(string path, string assetName, Object obj, bool forceOverwrite = false){
		string localPath = Path.Combine(path, assetName+".prefab");
		
		if(!Directory.Exists(path)) {
			AttemptCreatePath(path);
		}
		
		if(forceOverwrite){
			return CreateAsset(localPath, obj);
		} else {
			Object loadedObj = AssetDatabase.LoadAssetAtPath(localPath, typeof(Object));
			if (loadedObj != null) {
				int dialogResponse = EditorUtility.DisplayDialogComplex("File Merge", "An asset already exists with the name '"+assetName+"'. Do you want to overwrite it?", "Overwrite", "Save with new name", "Cancel");
				if (dialogResponse == 0){
					return CreateAsset(localPath, obj);
				} else if (dialogResponse == 1){
					localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);
					return CreateAsset(localPath, obj);
				} else {
					return null;
				}
			} else {
				return CreateAsset(localPath, obj);
			}
		}
	}
	
	
	//Path must be local to project
	public static Object AttemptCreatePrefab(string path, string assetName, GameObject go, bool forceOverwrite = false){
		string localPath = path +"/"+ assetName+".prefab";
		
		if(!Directory.Exists(path)) {
			//if (EditorUtility.DisplayDialog("Directory not found!", "Directory '"+path+"' does not exist.\nCreate it?", "Create", "Cancel")){
			AttemptCreatePath(path);
			//} else {
			//	return null;
			//}
		}
		
		if(forceOverwrite){
			return CreatePrefab(localPath, go);
		} else {
			Object loadedObj = AssetDatabase.LoadAssetAtPath(localPath, typeof(Object));
			if (loadedObj != null) {
				int dialogResponse = EditorUtility.DisplayDialogComplex("File Merge", "A prefab already exists with the name '"+assetName+"'. Do you want to overwrite it?", "Overwrite", "Save with new name", "Cancel");
				if (dialogResponse == 0){
					return CreatePrefab(localPath, go);
				} else if (dialogResponse == 1){
					localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);
					return CreatePrefab(localPath, go);
				} else {
					return null;
				}
			} else {
				return CreatePrefab(localPath, go);
			}
		}
	}
	
	public static Object CreateAsset(string localPath, Object obj) {
		//AssetDatabase.DeleteAsset(localPath);
		AssetDatabase.CreateAsset(obj, localPath);
		AssetDatabase.SaveAssets();
		return AssetDatabase.LoadAssetAtPath(localPath, typeof(Object)) as Object;
	}
	
	public static Object CreatePrefab(string localPath, GameObject gameObject){
		Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
		PrefabUtility.ReplacePrefab(gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
		return AssetDatabase.LoadAssetAtPath(localPath, typeof(Object)) as Object;
	}
	
	public static bool OpenInFileBrowser(string path) {
		if (SystemInfoX.IsWinOS) {
			return OpenInWinFileBrowser(path);
		} else if (SystemInfoX.IsMacOS) {
			return OpenInMacFileBrowser(path);	
		} else {
			DebugX.LogError ("Could not open in file browser because OS is unrecognized. OS is "+UnityEngine.SystemInfo.operatingSystem);
			return false;
		}
	}
	
	public static bool OpenInMacFileBrowser(string path) {
		bool openInsidesOfFolder = false;
		
		// try mac
		string macPath = path.Replace("\\", "/"); // mac finder doesn't like backward slashes
		// if path requested is a folder, automatically open insides of that folder
		if ( Directory.Exists(macPath) ) {
			openInsidesOfFolder = true;
		}
		
		if ( !macPath.StartsWith("\"") ) {
			macPath = "\"" + macPath;
		}
		
		if ( !macPath.EndsWith("\"") ){
			macPath = macPath + "\"";
		}
		
		string arguments = (openInsidesOfFolder ? "" : "-R ") + macPath;
		
		try {
			System.Diagnostics.Process.Start("open", arguments);
		} catch ( System.ComponentModel.Win32Exception e ) {
			// tried to open mac finder in windows
			// just silently skip error
			// we currently have no platform define for the current OS we are in, so we resort to this
			e.HelpLink = ""; // do anything with this variable to silence warning about not using it
			return false;
		}
		return true;
	}
	
	public static bool OpenInWinFileBrowser(string path) {
		bool openInsidesOfFolder = false;
		
		// try windows
		string winPath = path.Replace("/", "\\"); // windows explorer doesn't like forward slashes
		// if path requested is a folder, automatically open insides of that folder
		if ( Directory.Exists(winPath) ) {
			openInsidesOfFolder = true;
		}
		
		try {
			System.Diagnostics.Process.Start("explorer.exe", (openInsidesOfFolder ? "/root," : "/select,") + winPath);
		} catch ( System.ComponentModel.Win32Exception e ) {
			// tried to open win explorer in mac
			// just silently skip error
			// we currently have no platform define for the current OS we are in, so we resort to this
			e.HelpLink = ""; // do anything with this variable to silence warning about not using it
			return false;
		}
		return true;
	}
	
	/// <summary>
	/// Determine whether a given path is a directory.
	/// </summary>
	public static bool PathIsDirectory (string absolutePath)
	{
		FileAttributes attr = File.GetAttributes(absolutePath);
		if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
			return true;
		else
			return false;
	}
	
	
	/// <summary>
	/// Given an absolute path, return a path rooted at the Assets folder.
	/// </summary>
	/// <remarks>
	/// Asset relative paths can only be used in the editor. They will break in builds.
	/// </remarks>
	/// <example>
	/// /Folder/UnityProject/Assets/resources/music returns Assets/resources/music
	/// </example>
	public static string AssetsRelativePath (string absolutePath)
	{
		if (absolutePath.StartsWith(Application.dataPath)) {
			return "Assets" + absolutePath.Substring(Application.dataPath.Length);
		}
		else {
			throw new System.ArgumentException("Full path does not contain the current project's Assets folder", "absolutePath");
		}
	}
	
	
	/// <summary>
	/// Get all available Resources directory paths within the current project.
	/// </summary>
	public static string[] GetResourcesDirectories ()
	{
		List<string> result = new List<string>();
		Stack<string> stack = new Stack<string>();
		// Add the root directory to the stack
		stack.Push(Application.dataPath);
		// While we have directories to process...
		while (stack.Count > 0) {
			// Grab a directory off the stack
			string currentDir = stack.Pop();
			try {
				foreach (string dir in Directory.GetDirectories(currentDir)) {
					if (Path.GetFileName(dir).Equals("Resources")) {
						// If one of the found directories is a Resources dir, add it to the result
						result.Add(dir);
					}
					// Add directories at the current level into the stack
					stack.Push(dir);
				}
			}
			catch {
				Debug.LogError("Directory " + currentDir + " couldn't be read from.");
			}
		}
		return result.ToArray();
	}
	#endif
	/*public static string pathForBundleResource( string file )
	{
		var path = Application.dataPath.Replace( "Data", "" );
		return Path.Combine( path, file );
	}


	public static string pathForDocumentsResource( string file )
	{
		return Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.Personal ), file );
	}*/
}