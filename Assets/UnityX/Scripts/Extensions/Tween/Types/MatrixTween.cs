using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MatrixTween : TypeTween<Matrix4x4> {
	
	public MatrixTween () : base () {}
	public MatrixTween (Matrix4x4 myCurrentValue) : base (myCurrentValue) {}
	public MatrixTween (Matrix4x4 myStartValue, Matrix4x4 myTargetValue, float myLength) : base (myStartValue, myTargetValue, myLength) {}
	public MatrixTween (Matrix4x4 myStartValue, Matrix4x4 myTargetValue, float myLength, AnimationCurve myLerpCurve) : base (myStartValue, myTargetValue, myLength, myLerpCurve) {}
	
	protected override Matrix4x4 Lerp (Matrix4x4 myLastTarget, Matrix4x4 myTarget, float lerp) {
		return MatrixX.Lerp(startValue, targetValue, easingCurve.Evaluate(lerp));
	}
	
	protected override void SetDeltaValue (Matrix4x4 myLastValue, Matrix4x4 myCurrentValue) {
		deltaValue = MatrixX.Subtract (myCurrentValue, myLastValue);
	}
	
	//----------------- IOS GENERIC INHERITANCE EVENT CRASH BUG WORKAROUND ------------------
	//http://angrytroglodyte.net/cave/index.php/blog/11-unity-ios-doesn-t-like-generic-events
	public new event OnChangeEvent OnChange;
	public new event OnInterruptEvent OnInterrupt;
	public new event OnCompleteEvent OnComplete;
	
	protected override void ChangedCurrentValue () {
		base.ChangedCurrentValue();
		if(OnChange != null) OnChange(currentValue);
	}
	
	protected override void TweenComplete () {
		base.TweenComplete();
		if(OnComplete != null)OnComplete();
	}
	
	public override void Interrupt () {
		base.Interrupt();
		if(OnInterrupt != null) OnInterrupt();
	}
}