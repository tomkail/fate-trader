using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoundsTween : TypeTween<Bounds> {

	public BoundsTween () : base () {}
	public BoundsTween (Bounds myStartValue) : base (myStartValue) {}
	public BoundsTween (Bounds myStartValue, Bounds myTargetValue, float myLength) : base (myStartValue, myTargetValue, myLength) {}
	public BoundsTween (Bounds myStartValue, Bounds myTargetValue, float myLength, AnimationCurve myLerpCurve) : base (myStartValue, myTargetValue, myLength, myLerpCurve) {}

	protected override Bounds Lerp (Bounds myLastTarget, Bounds myTarget, float lerp) {
		return BoundsX.Lerp(startValue, targetValue, easingCurve.Evaluate(lerp));
	}

	protected override void SetDeltaValue (Bounds myLastValue, Bounds myCurrentValue) {
		deltaValue = myCurrentValue.Subtract(myLastValue);
	}
	
	//----------------- IOS GENERIC INHERITANCE EVENT CRASH BUG WORKAROUND ------------------
	//http://angrytroglodyte.net/cave/index.php/blog/11-unity-ios-doesn-t-like-generic-events
	public new event OnChangeEvent OnChange;
	public new event OnInterruptEvent OnInterrupt;
	public new event OnCompleteEvent OnComplete;
	
	protected override void ChangedCurrentValue () {
		base.ChangedCurrentValue();
		if(OnChange != null) OnChange(currentValue);
	}
	
	protected override void TweenComplete () {
		base.TweenComplete();
		if(OnComplete != null)OnComplete();
	}
	
	public override void Interrupt () {
		base.Interrupt();
		if(OnInterrupt != null) OnInterrupt();
	}
}