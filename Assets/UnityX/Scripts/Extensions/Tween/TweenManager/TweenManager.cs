﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TweenManager<T, Q> where T : TypeTween<Q> {
	public bool tweening = false;
	public Q currentValue;
	public Q deltaValue;
	public List<T> tweens;
	public int liveTweens;

	public TweenManager () {
		tweens = new List<T>();
	}

	public void ClearAllTweens(){
		for(int i = 0; i < tweens.Count; i++){
			tweens[i].tweening = false;
		}
		tweens.Clear();
		// ChangeNumTweens();
	}


	public void AddNewTween(T _newTween){
		_newTween.OnComplete += TweenComplete;
		tweens.Add(_newTween);
		// ChangeNumTweens();
	}
	
	private void TweenComplete () {
		// for(int i = 0; i < )
	}

	// public void AddNewTween(float _startValue, float _targetValue, float _time){
	// 	AddNewTween(_startValue, _targetValue, _time, AnimationCurve.EaseInOut(0,0,1,1));
	// }

	// public void AddNewTween(float _startValue, float _targetValue, float _time){
	// 	AddNewTween(_startValue, _targetValue, _time, AnimationCurve.EaseInOut(0,0,1,1));
	// }

	// public void AddNewTween(float _startValue, float _targetValue, float _time, AnimationCurve _lerpCurve){
	// 	bool foundIdleTween = false;
	// 	T idleTween = GetIdleTween();
	// 	if(idleTween != null) {
	// 		idleTween.Tween(_startValue, _targetValue, _time, _lerpCurve);
	// 		break;
	// 	} else {
	// 		T newTween = new T(_startValue, _targetValue, _time, _lerpCurve);
	// 		tweens.Add(newTween);
	// 	}
	// 	ChangeNumTweens();
	// }

	// private T GetIdleTween () {
	// 	for(int i = 0; i < tweens.Count; i++){
	// 		if(!tweens[i].tweening){
	// 			return tweens[i];
	// 		}
	// 	}
	// 	return null;
	// }
	
	public Q Loop(){
		return Loop(Time.deltaTime);
	}

	public Q Loop(float _deltaTime){
		deltaValue = currentValue = default(Q);
		if(tweens == null || tweens.Count == 0){
			return currentValue;
		}

		for(int i = 0; i < tweens.Count; i++){
			if(tweens[i].tweening){
				// currentValue += tweens[i].Loop(_deltaTime);
				// deltaValue += tweens[i].deltaValue;
			}
		}
		
		return currentValue;
	}

	// private void ChangeNumTweens(){
	// 	liveTweens = 0;
	// 	for(int i = 0; i < tweens.Count; i++){
	// 		if(tweens[i].tweening){
	// 			liveTweens++;
	// 		}
	// 	}
		
	// 	if(liveTweens == 0){
	// 		tweening = false;
	// 	} else {
	// 		tweening = true;
	// 	}
	// }
}
