using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityX.Geometry.Polygon;

public class PolygonEditor {
	
	bool firstDraw = true;

	float zoomSpeed = 0.0025f;
	
	bool canScroll = true;
	public bool CanScroll {
		get {
			return canScroll;
		}
		set {
			if(canScroll == value) return;
			canScroll = value;
			if(!canScroll)
				savedScrollPos = scrollPos;
		}
	}

	Vector2 savedScrollPos;
	
	float editorDisplayScale = 1;
	Vector2 scrollPos = new Vector2(0.0f, 0.0f);
	bool drawNormals = true;
	
	Color handleInactiveColor = new Color(0.5f, 0.65f, 0.4f);
//	Color handleActiveColor = new Color(0.7f, 0.7f, 0.1f);
	
	float createNewPointDistance = 75f;
//	float padding = 200;
	
	float snapInterval = 0.1f;
	
//	bool dragging = false;
	
	Rect viewRect;
	
	bool selecting = false;
	Rect selectionRect;
	
	Polygon polygon;
	Rect polygonEditorRect;
	
	public PolygonEditor () {
		selectionRect = new Rect();
		firstDraw = true;
	}
	
	public void SetProperties (Polygon _polygon) {
		polygon = _polygon;
	}
	
	private void MousePositionToWorldSpace () {
//		ev.mousePosition
	}
	
	private void FitPolygonInRect (){
		Bounds polygonBounds = polygon.GetBounds();
		
		float excessSpace = 1.2f;
		float rectAspectRatio = polygonEditorRect.size.x / polygonEditorRect.size.y;
		float polygonAspectRatio = polygonBounds.size.x / polygonBounds.size.y;
		if (polygonAspectRatio >= rectAspectRatio) {
			editorDisplayScale = polygonEditorRect.size.x / (polygonBounds.size.x * excessSpace);
		} else {
			editorDisplayScale = polygonEditorRect.size.y / (polygonBounds.size.y * excessSpace);
		}
		
		float viewRectWidth = polygonBounds.size.x * editorDisplayScale;
		float viewRectHeight = polygonBounds.size.y * editorDisplayScale;
		float viewRectX = (polygonBounds.min.x * editorDisplayScale) + (viewRectWidth - polygonEditorRect.size.x) * 0.5f;
		float viewRectY = (polygonBounds.min.y * editorDisplayScale) + (viewRectHeight - polygonEditorRect.size.y) * 0.5f;
		
		viewRect = new Rect(viewRectX, viewRectY, viewRectWidth, viewRectHeight);
	}
	
	private void UpdatePolygonEditorRect () {
		float padding = 4;
		polygonEditorRect = GUILayoutUtility.GetRect(128.0f, 128.0f, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
		polygonEditorRect.x += padding;
		polygonEditorRect.y += padding;
		polygonEditorRect.width -= padding * 2;
		polygonEditorRect.height -= padding * 2;
	}
	
	public void DrawPolygonView() {
		
		
		drawNormals = true;	
		GUILayout.BeginVertical();
		
		UpdatePolygonEditorRect();
		
		if(firstDraw) {
			Debug.Log (polygonEditorRect);
			FitPolygonInRect();
			firstDraw = false;
		}
		
		EditorGrid.Draw(polygonEditorRect);
		
		Bounds polygonBounds = polygon.GetBounds();
		
		
		
		Event ev = Event.current;
		if(ev != null) {
			CanScroll = !ev.alt;
			
			if (ev.type == EventType.MouseDrag) {
				if (ev.alt) {
					viewRect.position -= ev.delta;
				} else {
//					selectionRect.width = 
				}
			} else if (ev.type == EventType.MouseDown) {
				selecting = true;
				selectionRect.position = ev.mousePosition;
			} else if (ev.type == EventType.MouseUp) {
				selecting = false;
			} else if(ev.type == EventType.ScrollWheel) {
				
				if (!CanScroll && ev.alt) {
					float lastEditorDisplayScale = editorDisplayScale;
					editorDisplayScale -= ev.delta.y * zoomSpeed * editorDisplayScale;
					editorDisplayScale = Mathf.Clamp (editorDisplayScale, 0.1f,1000f);
					
					viewRect.x += (editorDisplayScale-lastEditorDisplayScale)*((viewRect.x+(ev.mousePosition.x))/lastEditorDisplayScale);
					viewRect.y += (editorDisplayScale-lastEditorDisplayScale)*((viewRect.y+(ev.mousePosition.y))/lastEditorDisplayScale);
				}
			} else if (ev.type == EventType.KeyDown) {
				switch (ev.keyCode) {
					case KeyCode.F: {
						FitPolygonInRect();
						ev.Use();
						break;
					}
				}
			}
		}
		
		
		
//		viewRect = new Rect(polygonBounds.min.x, polygonBounds.min.y, polygonBounds.size.x * 0.5f * editorDisplayScale, polygonBounds.size.y * 0.5f * editorDisplayScale);
		scrollPos = GUI.BeginScrollView(polygonEditorRect, scrollPos, viewRect);
//		new Rect(viewRect.x * editorDisplayScale, viewRect.y * editorDisplayScale, viewRect.width * editorDisplayScale, viewRect.height * editorDisplayScale));
		
		
		
		
		DrawPolygonEditor(polygon, false);
		
		if(selecting)
			GUI.Box(selectionRect, "");
		
		GUI.Box(new Rect(polygon.GetBounds().min.x * editorDisplayScale, polygon.GetBounds().min.y * editorDisplayScale, polygon.GetBounds().size.x * editorDisplayScale, polygon.GetBounds().size.y * editorDisplayScale), "");
		GUI.EndScrollView();
		
		GUILayout.BeginHorizontal(GUI.skin.box, GUILayout.ExpandWidth(true), GUILayout.MinHeight(60), GUILayout.MaxHeight(60));
		GUILayout.BeginVertical();
		
		GUILayout.Label ("Draw Normals: "+drawNormals);
		GUILayout.Label ("Polygon: "+polygon);
		GUILayout.Label ("Polygon Bounds: "+polygonBounds);
		GUILayout.Label ("Can Scroll: "+CanScroll);
		
		snapInterval = GUILayout.HorizontalSlider (snapInterval, 0, 1);
		GUILayout.EndVertical();
		GUILayout.BeginVertical();
		
		GUILayout.Label ("Scroll Pos: "+scrollPos);
		GUILayout.Label ("Polygon editor Rect: "+polygonEditorRect);
		GUILayout.Label ("View Rect: "+viewRect);
		
		GUILayout.Label ("Display Scale: "+editorDisplayScale);
		viewRect = EditorGUILayout.RectField ("View Rect", viewRect);
		if(GUILayout.Button("")) {
			viewRect.x = polygon.GetBounds().min.x * editorDisplayScale;
			viewRect.y = polygon.GetBounds().min.y * editorDisplayScale;
		}
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
		
		
		if(!CanScroll) {
			scrollPos = savedScrollPos;
		}
	}
	
	private void DrawPolygonEditor(Polygon polygon, bool forceClosed) {
		// Not sure what this does?
		Vector2 origin = Vector2.zero;
		
		Color previousHandleColor = Handles.color;
		bool insertPoint = false;
		
		if (Event.current.type == EventType.MouseDown) {
			if (Event.current.clickCount == 1) {
//				GUIUtility.hotControl = 0;
//				GUIUtility.keyboardControl = 0;
//				Event.current.Use();
			} else if (Event.current.clickCount == 2) {
				insertPoint = true;
				Event.current.Use();
			}
		}
		
		float closestDistanceSq = 1.0e32f;
		Vector2 closestPoint = Vector2.zero;
		int closestPreviousPoint = 0;
		
		Handles.color = handleInactiveColor;

		Vector2 ov = (polygon.vertices.Length>0)?polygon.vertices[polygon.vertices.Length-1]:Vector2.zero;
		for (int i = 0; i < polygon.vertices.Length; ++i)
		{
			Vector2 v = polygon.vertices[i];
			
			// Don't draw last connection if its not connected
//			if (i == 0) {
//				ov = v;
//				continue;
//			}
			
			if (insertPoint) {
				Vector2 localMousePosition = (Event.current.mousePosition - origin) / editorDisplayScale;
				Vector2 closestPointToCursor = ClosestPointOnLine(localMousePosition, ov, v);
				float lengthSq = (closestPointToCursor - localMousePosition).sqrMagnitude;
				if (lengthSq < closestDistanceSq)
				{
					closestDistanceSq = lengthSq;
					closestPoint = closestPointToCursor;
					closestPreviousPoint = i;
				}
			}
			
			if (drawNormals) {
				Vector2 l = (ov - v).normalized;
				Vector2 n = new Vector2(l.y, -l.x);
				Vector2 c = (v + ov) * 0.5f * editorDisplayScale + origin;
				Handles.DrawLine(c, c + n * 16.0f);
			}
			
			Handles.DrawLine(v * editorDisplayScale + origin, ov * editorDisplayScale + origin);
			ov = v;
		}
		Handles.color = previousHandleColor;
		
		if (insertPoint && closestDistanceSq < createNewPointDistance)
		{
			var tmpList = new List<Vector2>(polygon.vertices);
			tmpList.Insert(closestPreviousPoint, closestPoint);
			polygon.vertices = tmpList.ToArray();
			HandleUtility.Repaint();
		}
		
		int deletedIndex = -1;
		bool flipIsland = false;
		
		Event ev = Event.current;
		
		for (int i = 0; i < polygon.vertices.Length; ++i)
		{
			Vector3 cp = polygon.vertices[i];
			int id = "PolygonEditor".GetHashCode() + 10000 + i;
			Vector2 guiPosition = cp * editorDisplayScale + (Vector3)origin;
			cp = (EditorGuiUtility.Handle(EditorSkin.MoveHandle, id, guiPosition, true) - origin) / editorDisplayScale;
			
			
			if (GUIUtility.keyboardControl == id) {
				if(ev.control) {
//					if(holding)
					cp = SnapPointToInterval(cp, snapInterval);
				} else if(ev.type == EventType.KeyDown) {
				
					switch (ev.keyCode) {
					case KeyCode.Backspace: 
					case KeyCode.Delete: {
						GUIUtility.keyboardControl = 0;
						GUIUtility.hotControl = 0;
						deletedIndex = i;
						ev.Use();
						break;
					}
					case KeyCode.Y: {
						flipIsland = true;
						GUIUtility.keyboardControl = 0;
						GUIUtility.hotControl = 0;
						ev.Use();
						break;
					}
					case KeyCode.Escape: {
						GUIUtility.hotControl = 0;
						GUIUtility.keyboardControl = 0;
						ev.Use();
						break;
					}
					}
				} 
			}

			EditorGuiUtility.SetPositionHandleValue(id, new Vector2(cp.x, cp.y));
			
			Vector2 labelSize = new Vector2(100,20);
			GUI.Box(new Rect(guiPosition.x - labelSize.x * 0.5f, guiPosition.y + 10, labelSize.x, labelSize.y), cp.ToString());
			
			polygon.vertices[i] = cp;
		}
		
		if (flipIsland) {
			System.Array.Reverse(polygon.vertices);
		}
		
		if (deletedIndex != -1 && polygon.vertices.Length > 3) {
			var tmpList = new List<Vector2>(polygon.vertices);
			tmpList.RemoveAt(deletedIndex);
			polygon.vertices = tmpList.ToArray();
		}
	}
	
	private Vector2 SnapPointToInterval (Vector2 point, float interval) {
		point.x = Mathf.Round(point.x / interval) * interval;
		point.y = Mathf.Round(point.y / interval) * interval;
		return point;
	}
		
	private Vector2 ClosestPointOnLine(Vector2 p, Vector2 p1, Vector2 p2) {
		float magSq = (p2 - p1).sqrMagnitude;
		if (magSq < float.Epsilon)
			return p1;
		
		float u = ((p.x - p1.x) * (p2.x - p1.x) + (p.y - p1.y) * (p2.y - p1.y)) / magSq;
		if (u < 0.0f || u > 1.0f)
			return p1;
		
		return p1 + (p2 - p1) * u;
	}
}