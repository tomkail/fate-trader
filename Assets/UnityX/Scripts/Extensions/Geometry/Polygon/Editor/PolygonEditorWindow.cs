﻿using UnityEngine;
using UnityEditor;
using UnityX.Geometry.Polygon;

public class PolygonEditorWindow : EditorWindow {
	
//	public static PolygonEditorWindow inst;
	public new static Vector2 minSize = new Vector2(400,200);
	
	public Polygon polygon;
	
	private PolygonEditor polygonEditor;
	
	void OnEnable() {
		polygonEditor = new PolygonEditor();
	}
	
	void OnDisable() {
		polygonEditor = null;
	}
	
	public void SetProperties(Polygon _polygon) {
		polygon = _polygon;
		polygonEditor.SetProperties(polygon);
	}
	
	void OnGUI() {
		polygonEditor.DrawPolygonView();
		Repaint();
	}
	
	void OnLostFocus() {
		this.Close();
	}
}