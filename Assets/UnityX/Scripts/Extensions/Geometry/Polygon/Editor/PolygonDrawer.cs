﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityX.Geometry.Polygon;

using System.Linq;
using System;
using System.Reflection;

[CustomPropertyDrawer (typeof (Polygon))]
public class PolygonDrawer : PropertyDrawer {
	
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
		GUI.Label(position, property.displayName);
		
		if (GUI.Button (new Rect(position.x + 120, position.y, position.width - 120, position.height), "Open Polygon Editor")) {
			Polygon polygon = property.GetBaseProperty<Polygon>();
			if(polygon.vertices.IsNullOrEmpty()) {
				polygon = new RegularPolygon(6, 0, 5).ToPolygon();
				property.SetBaseProperty<Polygon>(polygon);
			}
			PolygonEditorWindow polygonEditorWindow = EditorWindow.GetWindow( typeof(PolygonEditorWindow), true, "Polygon" ) as PolygonEditorWindow;
			polygonEditorWindow.SetProperties(polygon);
		}

//		if (GUI.Button (new Rect(pos.x + 120, pos.y, pos.width - 120, pos.height), "Set And Print")) {
//			Polygon polygon = prop.GetBaseProperty<Polygon>();
//			if(!polygon.IsValid()) {
//				polygon.vertices = new RegularPolygon(6).ToPolygon().vertices;
//			}
//			Debug.Log (polygon);
//			prop.SetBaseProperty<Polygon>(new RegularPolygon(6, 0, 5).ToPolygon());
//		}
	}
}