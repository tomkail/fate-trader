﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace UnityX.Geometry {

	[System.Serializable]
	public class Line3 {
		public Vector3 start;
		public Vector3 end;
	
		public Line3 (Vector3 _start, Vector3 _end) {
			start = _start;
			end = _end;
		}
	
		public Line3 (Vector3[] _startEnd) {
			start = _startEnd[0];
			end = _startEnd[1];
		}
	
		public override string ToString() {
			return "Start: " + start + " End: " + end;
		}
	
		public Vector3 direction {
			get { return Vector3X.Direction(start, end); }
		}
		
		public Vector3 normalizedDirection {
			get { return direction.normalized; }
		}
		
		public float length {
			get { return Vector3.Distance(start, end); }
		}
	
		public float sqrLength {
			get { return Vector3X.SqrDistance(start, end); }
		}
	
		public static Line3 Add(Line3 left, Line3 right){
			return new Line3(left.start+right.start, left.end+right.end);
		}
	
		public static Line3 Add(Line3 left, Vector3 right){
			return new Line3(left.start + right, left.end + right);
		}
	
		public static Line3 Add(Vector3 left, Line3 right){
			return Line3.Add(right, left);
		}
	
	
		public static Line3 Subtract(Line3 left, Line3 right){
			return new Line3(left.start-right.start, left.end-right.end);
		}
	
		public static Line3 Subtract(Line3 left, Vector3 right){
			return new Line3(left.start - right, left.end - right);
		}
	
		public static Line3 Subtract(Vector3 left, Line3 right){
			return Line3.Subtract(right, left);
		}
	
		public override bool Equals(System.Object obj) {
			// If parameter is null return false.
			if (obj == null) {
				return false;
			}
	
			// If parameter cannot be cast to Line return false.
			Line3 l = obj as Line3;
			if ((System.Object)l == null) {
				return false;
			}
	
			// Return true if the fields match:
			return (start == l.start && end == l.end) || (start == l.end && end == l.start);
		}
	
		public bool Equals(Line3 l) {
			// If parameter is null return false:
			if ((object)l == null) {
				return false;
			}
	
			// Return true if the fields match:
			return (start == l.start && end == l.end) || (start == l.end && end == l.start);
		}
	
		public override int GetHashCode() {
			Debug.LogError("The method or operation is not implemented.");
			return 0;
		}
	
		public static bool operator == (Line3 left, Line3 right) {
			if (System.Object.ReferenceEquals(left, right))
			{
				return true;
			}
	
			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}
			
			return (left.start == right.start && left.end == right.end) || (left.start == right.end && left.end == right.start);
		}
	
		public static bool operator != (Line3 left, Line3 right) {
			return !(left == right);
		}
	
		public static Line3 operator +(Line3 left, Line3 right) {
			return Add(left, right);
		}
		
		public static Line3 operator +(Line3 left, Vector3 right) {
			return Add(left, right);
		}
		
		public static Line3 operator -(Line3 left, Line3 right) {
			return Subtract(left, right);
		}
		
		public static Line3 operator -(Line3 left, Vector3 right) {
			return Subtract(left, right);
		}
		
		/// <summary>
		/// Get the perpendicular point on a line from a point outside the line
		/// http://stackoverflow.com/questions/9368436/3d-perpendicular-point-on-line-from-3d-point?rq=1
		/// </summary>
		public Vector3 PerpendicularPointOnLine (Vector3 q) {
			Vector3 u = end - start;
			Vector3 pq = q - start;
			Vector3 w2 = pq - u * (Vector3.Dot(pq, u) / u.sqrMagnitude);
			return q - w2;
		}
		
		public float DistanceToPerpendicularPointOnLine (Vector3 q) {
			Vector3 u = end - start;
			Vector3 pq = q - start;
			return Vector3.Cross(pq, u).magnitude / u.magnitude;
		}
	}
}