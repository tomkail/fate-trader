﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace UnityX.Geometry {

	[System.Serializable]
	public class Line {
		public Vector2 start;
		public Vector2 end;
	
		public Line (Vector2 _start, Vector2 _end) {
			start = _start;
			end = _end;
		}
	
		public Line (Vector2[] _startEnd) {
			start = _startEnd[0];
			end = _startEnd[1];
		}
	
		public override string ToString() {
			return "Start: " + start + " End: " + end;
		}
	
		public float length {
			get { return Vector2.Distance(start, end); }
		}
	
		public float sqrLength {
			get { return Vector2X.SqrDistance(start, end); }
		}
	
		public static Line Add(Line left, Line right){
			return new Line(left.start+right.start, left.end+right.end);
		}
	
		public static Line Add(Line left, float right){
			return new Line(left.start.Add(right), left.end.Add(right));
		}
	
		public static Line Add(float left, Line right){
			return Line.Add(right, left);
		}
	
	
		public static Line Subtract(Line left, Line right){
			return new Line(left.start-right.start, left.end-right.end);
		}
	
		public static Line Subtract(Line left, float right){
			return new Line(left.start.Subtract(right), left.end.Subtract(right));
		}
	
		public static Line Subtract(float left, Line right){
			return Line.Subtract(right, left);
		}
	
		public override bool Equals(System.Object obj) {
			// If parameter is null return false.
			if (obj == null) {
				return false;
			}
	
			// If parameter cannot be cast to Line return false.
			Line l = obj as Line;
			if ((System.Object)l == null) {
				return false;
			}
	
			// Return true if the fields match:
			return (start == l.start && end == l.end) || (start == l.end && end == l.start);
		}
	
		public bool Equals(Line l) {
			// If parameter is null return false:
			if ((object)l == null) {
				return false;
			}
	
			// Return true if the fields match:
			return (start == l.start && end == l.end) || (start == l.end && end == l.start);
		}
	
		public override int GetHashCode() {
			Debug.LogError("The method or operation is not implemented.");
			return 0;
		}
	
		public static bool operator == (Line left, Line right) {
			if (System.Object.ReferenceEquals(left, right))
			{
				return true;
			}
	
			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}
			
			return (left.start == right.start && left.end == right.end) || (left.start == right.end && left.end == right.start);
		}
	
		public static bool operator != (Line left, Line right) {
			return !(left == right);
		}
	
		public static Line operator +(Line left, Line right) {
			return Add(left, right);
		}
	
		public static Line operator -(Line left, Line right) {
			return Subtract(left, right);
		}
		
		public static Vector2? LineIntersectionPoint(Line line1, Line line2) {
			// Get A,B,C of first line - points : line1.start to line1.end
			float A1 = line1.end.y - line1.start.y;
			float B1 = line1.start.x - line1.end.x;
			float C1 = A1 * line1.start.x + B1 * line1.start.y;
		 
			// Get A,B,C of second line - points : line2.start to line2.end
			float A2 = line2.end.y - line2.start.y;
			float B2 = line2.start.x - line2.end.x;
			float C2 = A2 * line2.start.x + B2 * line2.start.y;
		 
			// Get delta and check if the lines are parallel
			float delta = A1*B2 - A2*B1;
			if(delta == 0)
			return null;
		 
			// now return the Vector2 intersection point
			return new Vector2((B2*C1 - B1*C2)/delta, (A1*C2 - A2*C1)/delta);
		}
	
		//Bresenham's Line Algorithm
		//A way of drawing a line segment onto a square grid.
		//http://www.roguebasin.com/index.php?title=Bresenham%27s_Line_Algorithm
	
		private static void Swap<T>(ref T lhs, ref T rhs) { T temp; temp = lhs; lhs = rhs; rhs = temp; }
	
		/// <summary>
		/// The plot function delegate
		/// </summary>
		/// <param name="x">The x co-ord being plotted</param>
		/// <param name="y">The y co-ord being plotted</param>
		/// <returns>True to continue, false to stop the algorithm</returns>
		public delegate bool PlotFunction(int x, int y);
	
		/// <summary>
		/// Plot the line from (x0, y0) to (x1, y10
		/// </summary>
		/// <param name="x0">The start x</param>
		/// <param name="y0">The start y</param>
		/// <param name="x1">The end x</param>
		/// <param name="y1">The end y</param>
		/// <param name="plot">The plotting function (if this returns false, the algorithm stops early)</param>
		public static void Plot(int x0, int y0, int x1, int y1, PlotFunction plot)
		{
			bool steep = Mathf.Abs(y1 - y0) > Mathf.Abs(x1 - x0);
			if (steep) { Swap<int>(ref x0, ref y0); Swap<int>(ref x1, ref y1); }
			if (x0 > x1) { Swap<int>(ref x0, ref x1); Swap<int>(ref y0, ref y1); }
			int dX = (x1 - x0), dY = Mathf.Abs(y1 - y0), err = (dX / 2), ystep = (y0 < y1 ? 1 : -1), y = y0;
	
			for (int x = x0; x <= x1; ++x)
			{
				if (!(steep ? plot(y, x) : plot(x, y))) return;
				err = err - dY;
				if (err < 0) { y += ystep;  err += dX; }
			}
		}
	
		public static Point[] PointsOnLine(int x0, int y0, int x1, int y1) {
			List<Point> points = new List<Point>();
			int dx = Mathf.Abs(x1 - x0);
			int dy = Mathf.Abs(y1 - y0);
			int sx = x0 < x1 ? 1 : -1;
			int sy = y0 < y1 ? 1 : -1;
			int err = dx - dy;
	
			while (true){
				points.Add(new Point(x0, y0));
	
				if (x0==x1 && y0==y1)
					return points.ToArray();
	
				int e2 = err * 2;
				if (e2 > -dx) {
					err -= dy;
					x0 += sx;
				}
				if (e2 < dx){
					err += dx;
					y0 += sy;
				}
			}
		}
	}
}