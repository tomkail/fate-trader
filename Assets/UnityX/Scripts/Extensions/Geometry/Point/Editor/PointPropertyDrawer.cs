﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
namespace UnityX.Geometry {

	[CustomPropertyDrawer(typeof (Point))]
	public class PointPropertyDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			EditorGUI.BeginProperty (position, label, property);
			position = EditorGUI.PrefixLabel (position, label);
			var oneThird = ((position.width+3) / 3);

			var xRect = new Rect(position.x, position.y, oneThird, position.height-1);
			var yRect = new Rect(position.x + oneThird, position.y, oneThird, position.height-1);

			EditorGUI.LabelField(xRect.CopyWithWidth(12), new GUIContent("X"));
			EditorGUI.PropertyField (xRect.CopyWithX(xRect.x + 12).CopyWithWidth(xRect.width - 12), property.FindPropertyRelative ("x"), GUIContent.none);

			EditorGUI.LabelField(yRect.CopyWithWidth(12), new GUIContent("Y"));
			EditorGUI.PropertyField (yRect.CopyWithX(yRect.x + 12).CopyWithWidth(xRect.width - 12), property.FindPropertyRelative ("y"), GUIContent.none);

			EditorGUI.EndProperty ();
		}
	}
}

#endif