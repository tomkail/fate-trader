﻿using UnityEngine;
using System.Collections.Generic;

namespace UnityX {
	namespace Geometry {
		
		[System.Serializable]
		public struct PointRect {
			public int x, y, width, height;
			
			public int xMax {
				get {
					return width + x;
				} set {
					width = value - x;
				}
			}
			
			public int xMin {
				get {
					return x;
				} set {
					x = value;
					width = xMax - x;
				}
			}
			
			public int yMax {
				get {
					return height + y;
				}
				set {
					height = value - y;
				}
			}
			
			public int yMin {
				get {
					return y;
				} set {
					y = value;
					height = yMax - y;
				}
			}
			
			public Point min {
				get {
					return new Point(xMin, yMin);
				}
			}

			public Point max {
				get {
					return new Point(xMax, yMax);
				}
			}

			public Point position {
				get {
					return new Point(x, y);
				} set {
					x = value.x;
					y = value.y;
				}
			}
		
			public Point size {
				get {
					return new Point(width, height);
				} set {
					width = value.x;
					height = value.y;
				}
			}

			public PointRect(int _x, int _y, int _width, int _height) {
				x = _x;
				y = _y;
				width = _width;
				height = _height;
			}
		
			public PointRect(float _x, float _y, float _width, float _height) : this (Mathf.RoundToInt(_x), Mathf.RoundToInt(_y), Mathf.RoundToInt(_width), Mathf.RoundToInt(_height)) {}
			
			/// <summary>
			/// Initializes a new instance of the <see cref="UnityX.Geometry.PointRect"/> class.
			/// </summary>
			/// <param name="_position">The position of the top-left corner.</param>
			/// <param name="_size">The width and height.</param>
			public PointRect(Point _position, Point _size) : this (_position.x, _position.y, _size.x, _size.y) {}
		
			public PointRect (int[] xywh) : this (xywh[0], xywh[1], xywh[2], xywh[3]) {}

			public static PointRect CreateEncapsulating (params Point[] points) {
				PointRect rect = new PointRect(points[0].x, points[0].y, 0, 0);
				for(int i = 1; i < points.Length; i++)
					rect.Encapsulate(points[i]);
				return rect;
			}

			public static PointRect CreateEncapsulating (IList<Point> points) {
				PointRect rect = new PointRect(points[0].x, points[0].y, 0, 0);
				for(int i = 1; i < points.Count; i++)
					rect.Encapsulate(points[i]);
				return rect;
			}

			public static PointRect MinMaxRect (Point topLeft, Point bottomRight) {
				return new PointRect(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
			}

			public void Set(int _x, int _y, int _width, int _height) {
				x = _x;
				y = _y;
				width = _width;
				height = _height;
			}

			public Point[] GetPoints() {
				Point[] points = new Point[width * height];
//				for(int i = 0; i < points.Length; i++) {
//					Point point = new Point(Grid.ArrayIndexToGridPoint(i, width));
//					point += position;
//					points[i] = point;
//				}
				for(int y = 0; y < height; y++) {
					for(int x = 0; x < width; x++) {
						Point gridPoint = new Point(x, y);
						points[Grid.GridPointToArrayIndex(gridPoint, width)] = gridPoint + position;
					}
				}
				return points;
			}
		
			public static PointRect FromRect(Rect rect) {
				return new PointRect(Mathf.RoundToInt(rect.x), Mathf.RoundToInt(rect.y), Mathf.RoundToInt(rect.width), Mathf.RoundToInt(rect.height));
			}
		
			public static Rect ToRect(PointRect pointRect) {
				return new Rect(pointRect.x, pointRect.y, pointRect.width, pointRect.height);
			}

			public override string ToString() {
				return "X: " + x + " Y: " + y + " W: " + width + " H: " + height;
			}
			
			public bool Contains (Vector2 point) {
				return point.x >= this.xMin && point.x < this.xMax && point.y >= this.yMin && point.y < this.yMax;
			}
			
			public static PointRect Add(PointRect left, PointRect right){
				return new PointRect(left.x+right.x, left.y+right.y, left.width+right.width, left.height+right.height);
			}
		
		
			public static PointRect Subtract(PointRect left, PointRect right){
				return new PointRect(left.x-right.x, left.y-right.y, left.width-right.width, left.height-right.height);
			}
		
		
			public static PointRect Multiply(PointRect left, PointRect right){
				return new PointRect(left.x*right.x, left.y*right.y, left.width*right.width, left.height*right.height);
			}
			
			
			public static PointRect Divide(PointRect left, PointRect right) {
				return new PointRect(left.x/right.x, left.y/right.y, left.width/right.width, left.height/right.height);
			}

			public void Encapsulate(Point point) {
				PointRect minMaxRect = MinMaxRect (Point.Min (min, point), Point.Max (max, point+Point.one));
				this.x = minMaxRect.x;
				this.y = minMaxRect.y;
				this.width = minMaxRect.width;
				this.height = minMaxRect.height;
			}

			public void Encapsulate(PointRect rect) {
				Encapsulate(rect.min);
				Encapsulate(rect.max);
			}

			/// <summary>
			/// Clamps a point inside the rect.
			/// </summary>
			/// <param name="r">The red component.</param>
			/// <param name="point">Point.</param>
			public Point Clamp(Point point) {
				return Point.Clamp(point, min, max);
			}
			
			public static PointRect Clamp(PointRect r, PointRect container) {
				PointRect ret = new PointRect(0,0,0,0);
				ret.x = MathX.Clamp0Infinity(Mathf.Max(r.x, container.x));
				ret.y = MathX.Clamp0Infinity(Mathf.Max(r.y, container.y));
				ret.width = MathX.Clamp0Infinity(Mathf.Min(r.x + r.width, container.x + container.width) - ret.x);
				ret.height = MathX.Clamp0Infinity(Mathf.Min(r.y + r.height, container.y + container.height) - ret.y);
				
				int offsetX = MathX.Clamp0Infinity(r.x - Mathf.Max(ret.width, container.width));
				int offsetY = MathX.Clamp0Infinity(r.y - Mathf.Max(ret.height, container.height));
				ret.x -= offsetX;
				ret.y -= offsetY;
				return ret;
			}
			
			public static Rect ClampKeepSize(PointRect r, PointRect container) {
				Rect ret = new Rect();
				ret.x = MathX.Clamp0Infinity(Mathf.Max(r.x, container.x));
				ret.y = MathX.Clamp0Infinity(Mathf.Max(r.y, container.y));
				ret.width = Mathf.Min(r.width, container.width);
				ret.height = Mathf.Min(r.height, container.height);
				
				int offsetX = MathX.Clamp0Infinity((r.x + r.width) - (container.x + container.width));
				int offsetY = MathX.Clamp0Infinity((r.y + r.height) - (container.y + container.height));
				ret.x -= MathX.ClampMax(offsetX, ret.x);
				ret.y -= MathX.ClampMax(offsetY, ret.y);
				
				return ret;
			}

			public override bool Equals(System.Object obj) {
				// If parameter is null return false.
				if (obj == null) {
					return false;
				}
		
				// If parameter cannot be cast to PointRect return false.
				PointRect p = (PointRect)obj;
				if ((System.Object)p == null) {
					return false;
				}
		
				// Return true if the fields match:
				return (x == p.x) && (y == p.y) && (width == p.width) && (height == p.height);
			}
		
			public bool Equals(PointRect p) {
				// If parameter is null return false:
				if ((object)p == null) {
					return false;
				}
		
				// Return true if the fields match:
				return (x == p.x) && (y == p.y) && (y == p.width) && (y == p.height);
			}
		
			public override int GetHashCode() {
				Debug.LogError("The method or operation is not implemented.");
				return x * y;
			}
		
			public static bool operator == (PointRect left, PointRect right) {
				if (System.Object.ReferenceEquals(left, right))
				{
					return true;
				}
		
				// If one is null, but not both, return false.
				if (((object)left == null) || ((object)right == null))
				{
					return false;
				}
				if(left.x == right.x && left.y == right.y && left.width == right.width && left.height == right.height)return true;
				return false;
			}
		
			public static bool operator != (PointRect left, PointRect right) {
				return !(left == right);
			}
		
			public static PointRect operator +(PointRect left, PointRect right) {
				return Add(left, right);
			}
		
			public static PointRect operator -(PointRect left, PointRect right) {
				return Subtract(left, right);
			}
		
			public static PointRect operator *(PointRect left, PointRect right) {
				return Add(left, right);
			}
		
			public static PointRect operator /(PointRect left, PointRect right) {
				return Subtract(left, right);
			}
		
			public static implicit operator PointRect(Rect src) {
				return FromRect(src);
			}
			
			public static implicit operator Rect(PointRect src) {
				return PointRect.ToRect(src);
			}
		}
	}
}