﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Serializable implementation of transform class. 
/// Useful when doing Transform math without the need for a transform.
/// </summary>
public class SerializableTransform {

	public SerializableTransform () {
		this.position = Vector3.zero;
		this.rotation = Quaternion.identity;
		this.localScale = Vector3.one;
	}
	
	public SerializableTransform (Vector3 position) {
		this.position = position;
		this.rotation = Quaternion.identity;
		this.localScale = Vector3.one;
	}
	
	public SerializableTransform (Vector3 position, Quaternion rotation) {
		this.position = position;
		this.rotation = rotation;
		this.localScale = Vector3.one;
	}
	
	public SerializableTransform (Vector3 position, Vector3 eulerAngles) {
		this.position = position;
		this.eulerAngles = eulerAngles;
		this.localScale = Vector3.one;
	}
	
	public SerializableTransform (Vector3 position, Quaternion rotation, Vector3 localScale) {
		this.position = position;
		this.rotation = rotation;
		this.localScale = localScale;
	}
	
	public SerializableTransform (Vector3 position, Vector3 eulerAngles, Vector3 localScale) {
		this.position = position;
		this.eulerAngles = eulerAngles;
		this.localScale = localScale;
	}
	
	public SerializableTransform (Transform transform) {
		this.position = transform.position;
		this.rotation = transform.rotation;
		this.localScale = transform.localScale;
	}
	
	
	public Vector3 eulerAngles
	{
		get
		{
			return this.rotation.eulerAngles;
		}
		set
		{
			this.rotation = Quaternion.Euler (value);
		}
	}
	
	public Vector3 forward
	{
		get
		{
			return this.rotation * Vector3.forward;
		}
		set
		{
			this.rotation = Quaternion.LookRotation (value);
		}
	}
	
	private Vector3 _localScale;
	public Vector3 localScale
	{
		get
		{
			return _localScale;
		}
		set
		{
			_localScale = value;
		}
	}
	
	private Vector3 _position;
	public Vector3 position
	{
		get
		{
			return _position;
		}
		set
		{
			_position = value;
		}
	}
	
	public Vector3 right
	{
		get
		{
			return this.rotation * Vector3.right;
		}
		set
		{
			this.rotation = Quaternion.FromToRotation (Vector3.right, value);
		}
	}
	
	private Quaternion _rotation;
	public Quaternion rotation
	{
		get
		{
			return _rotation;
		}
		set
		{
			_rotation = value;
		}
	}
	
	public Vector3 up
	{
		get
		{
			return this.rotation * Vector3.up;
		}
		set
		{
			this.rotation = Quaternion.FromToRotation (Vector3.up, value);
		}
	}
	
	//
	// Methods
	//
	public void Rotate (float xAngle, float yAngle, float zAngle, Space relativeTo = Space.Self)
	{
		this.Rotate (new Vector3 (xAngle, yAngle, zAngle), relativeTo);
	}
	
	public void Rotate (Vector3 axis, float angle, Space relativeTo = Space.Self)
	{
		rotation = rotation.Rotate(Quaternion.AngleAxis (angle, axis), relativeTo);
	}
	
	public void Rotate (Vector3 eulerAngles, Space relativeTo = Space.Self)
	{
		rotation = rotation.Rotate(Quaternion.Euler(eulerAngles), relativeTo);
	}
	
	public void RotateAround (Vector3 point, Vector3 axis, float angle)
	{
		Vector3 vector = position;
		Quaternion rotation = Quaternion.AngleAxis (angle, axis);
		Vector3 vector2 = vector - point;
		vector2 = rotation * vector2;
		vector = point + vector2;
		position = vector;
		Rotate (axis, angle, Space.World);
	}
	
	public void Translate (Vector3 translation, Transform relativeTo)
	{
		if (relativeTo)
		{
			this.position += relativeTo.TransformDirection (translation);
		}
		else
		{
			this.position += translation;
		}
	}
	
	public void Translate (float x, float y, float z, Transform relativeTo)
	{
		this.Translate (new Vector3 (x, y, z), relativeTo);
	}
	
	public void Translate (float x, float y, float z, Space relativeTo = Space.Self)
	{
		this.Translate (new Vector3 (x, y, z), relativeTo);
	}
	
	public void Translate (Vector3 translation, Space relativeTo = Space.Self)
	{
		if (relativeTo == Space.World)
		{
			this.position += translation;
		}
		else
		{
			this.position += rotation * translation;
		}
	}
	
	
	
	
	
	
	public Vector3 InverseTransformDirection (float x, float y, float z) {
		return this.InverseTransformDirection (new Vector3 (x, y, z));
	}
	
	public Vector3 InverseTransformDirection (Vector3 direction) {
		Matrix4x4 transformMatrix = Matrix4x4.TRS(this.position, this.rotation, Vector3.one);
		Vector3 localPoint = transformMatrix.inverse.MultiplyVector(direction);
		return localPoint;
	}
	
	public Vector3 InverseTransformPoint (float x, float y, float z) {
		return this.InverseTransformPoint (new Vector3 (x, y, z));
	}
	
	public Vector3 InverseTransformPoint (Vector3 position) {
		Matrix4x4 transformMatrix = Matrix4x4.TRS(this.position, this.rotation, this.localScale);
		Vector3 localPoint = transformMatrix.inverse.MultiplyPoint(position);
		return localPoint;
	}
	
	public Vector3 InverseTransformVector (float x, float y, float z) {
		return this.InverseTransformVector (new Vector3 (x, y, z));
	}
	
	public Vector3 InverseTransformVector (Vector3 vector) {
		Matrix4x4 transformMatrix = Matrix4x4.TRS(this.position, this.rotation, this.localScale);
		Vector3 localPoint = transformMatrix.inverse.MultiplyVector(vector);
		return localPoint;
	}
	
	
	public Vector3 TransformDirection (float x, float y, float z) {
		return this.TransformDirection (new Vector3 (x, y, z));
	}
	
	public Vector3 TransformDirection (Vector3 direction) {
		Matrix4x4 transformMatrix = Matrix4x4.TRS(this.position, this.rotation, Vector3.one);
		Vector3 localPoint = transformMatrix.MultiplyVector(direction);
		return localPoint;
	}
	
	public Vector3 TransformPoint (float x, float y, float z) {
		return this.TransformPoint (new Vector3 (x, y, z));
	}
	
	public Vector3 TransformPoint (Vector3 position) {
		Matrix4x4 transformMatrix = Matrix4x4.TRS(this.position, this.rotation, this.localScale);
		Vector3 localPoint = transformMatrix.MultiplyPoint(position);
		return localPoint;
	}
	
	public Vector3 TransformVector (Vector3 vector) {
		Matrix4x4 transformMatrix = Matrix4x4.TRS(this.position, this.rotation, this.localScale);
		Vector3 localPoint = transformMatrix.MultiplyVector(vector);
		return localPoint;
	}
	
	public Vector3 TransformVector (float x, float y, float z) {
		return this.TransformVector (new Vector3 (x, y, z));
	}
	
}