﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Helper class for functions in CameraX. 
/// Used when camera calculations are executed without actually needing a camera.
/// </summary>
public class SerializableCamera  {
	private const float defaultFieldOfView = 60;
	private const float defaultAspectRatio = 1;
	private const float defaultNearClipPlane = 0.01f;
	private const float defaultFarClipPlane = 10000000;
	
	public SerializableTransform transform;
	
	public Vector3 position {
		get {
			return transform.position;
		} set {
			if(transform == null) transform = new SerializableTransform();
			transform.position = value;
		}
	}
	
	public Quaternion rotation {
		get {
			return transform.rotation;
		} set {
			if(transform == null) transform = new SerializableTransform();
			transform.rotation = value;
		}
	}
	
	[NotLessThan(1)]
	public float fieldOfView;
	
	[NotLessThan(0.01f)]
	public float aspectRatio;
	
	[NotLessThan(0.01f)]
	public float nearClipPlane;
	
	[NotLessThan(0.01f)]
	public float farClipPlane;
	
	public SerializableCamera () {
		this.transform = new SerializableTransform();
		this.fieldOfView = defaultFieldOfView;
		this.aspectRatio = defaultAspectRatio;
		this.nearClipPlane = defaultNearClipPlane;
		this.farClipPlane = defaultFarClipPlane;
	}
	
	public SerializableCamera (Camera camera) {
		this.transform = new SerializableTransform(camera.transform);
		this.fieldOfView = camera.fieldOfView;
		this.aspectRatio = camera.aspect;
		this.nearClipPlane = camera.nearClipPlane;
		this.farClipPlane = camera.farClipPlane;
	}
	
	public SerializableCamera (Vector3 position, Quaternion rotation, float fieldOfView, float aspectRatio) {
		this.transform = new SerializableTransform(position, rotation);
		this.fieldOfView = fieldOfView;
		this.aspectRatio = aspectRatio;
		this.nearClipPlane = defaultNearClipPlane;
		this.farClipPlane = defaultFarClipPlane;
	}
	
	public SerializableCamera (Vector3 position, Quaternion rotation, float fieldOfView, float aspectRatio, float nearClipPlane, float farClipPlane) {
		this.transform = new SerializableTransform(position, rotation);
		this.fieldOfView = fieldOfView;
		this.aspectRatio = aspectRatio;
		this.nearClipPlane = nearClipPlane;
		this.farClipPlane = farClipPlane;
	}
	
	public Vector2 WorldToViewportPoint (Vector3 worldPoint) {
		Matrix4x4 cameraTransformMatrix = Matrix4x4.TRS(position, rotation, Vector3.one);
		Matrix4x4 cameraPerspectiveMatrix = Matrix4x4.Perspective(fieldOfView, aspectRatio, nearClipPlane, farClipPlane);
		
		Matrix4x4 viewProjectionMatrix = cameraPerspectiveMatrix * cameraTransformMatrix.inverse;
		worldPoint = viewProjectionMatrix.MultiplyPoint(worldPoint);
		
		return new Vector2(0.5f + (worldPoint.x * -0.5f), 0.5f + (worldPoint.y * -0.5f));
	}
	
	public Vector2 WorldToScreenPoint (Vector3 worldPoint) {
		return Vector2.Scale(WorldToViewportPoint(worldPoint), new Vector2(Screen.width, Screen.height));
	}
	
	public Vector3 ViewportToWorldPoint (Vector3 viewportPoint) {
		Matrix4x4 cameraTransformMatrix = Matrix4x4.TRS(position, rotation, Vector3.one);
		Matrix4x4 cameraPerspectiveMatrix = Matrix4x4.Perspective(fieldOfView, aspectRatio, nearClipPlane, farClipPlane);
		
		float distance = viewportPoint.z;
		viewportPoint.z = 0;
		
		viewportPoint.x = (viewportPoint.x - 0.5f) * -2;
		viewportPoint.y = (viewportPoint.y - 0.5f) * -2;
		
		Vector3 p1 = Vector3.zero;
		Vector3 p2 = new Vector3(viewportPoint.x * nearClipPlane, viewportPoint.y * nearClipPlane, nearClipPlane);
		
		Vector3 worldPointNear = cameraPerspectiveMatrix.inverse.MultiplyPoint(p1);
		Vector3 worldPointFar = cameraPerspectiveMatrix.inverse.MultiplyPoint(p2);
		
		// Create a direction that can be projected forward in world space.
		Vector3 dir = worldPointNear-worldPointFar;
		Vector3 normalizedDir = dir.normalized;
		
		// Get the length of the vector required to reach the target direction when using the direction we just calculated.
		float dotLength = Vector3.Dot (Vector3.forward, normalizedDir);
		Vector3 point = normalizedDir * distance/dotLength;
		
		// Get this point relative to the camera
		point = cameraTransformMatrix.MultiplyPoint(point);
		return point;
	}
	
	public Vector2 ScreenToWorldPoint (Vector3 worldPoint) {
		return ViewportToWorldPoint(Vector2.Scale(worldPoint, new Vector2(1f/Screen.width, 1f/Screen.height)));
	}
	
	
	public Vector2[] WorldToViewportPoints (params Vector3[] input) {
		return WorldToViewportPoints(input);
	}
	
	public Vector2[] WorldToViewportPoints (IList<Vector3> input) {
		Vector2[] output = new Vector2[input.Count];
		for(int i = 0; i < output.Length; i++) {
			output[i] = WorldToViewportPoint(input[i]);
		}
		return output;
	}
	
	
	public Vector2[] WorldToScreenPoints (params Vector3[] input) {
		return WorldToScreenPoints(input);
	}
	public Vector2[] WorldToScreenPoints (IList<Vector3> input) {
		Vector2[] output = new Vector2[input.Count];
		for(int i = 0; i < output.Length; i++)
			output[i] = WorldToScreenPoint(input[i]);
		return output;
	}
	
	
	public Vector3[] ViewportToWorldPoints (params Vector3[] input) {
		return ViewportToWorldPoints(input);
	}
	
	public Vector3[] ViewportToWorldPoints (IList<Vector3> input) {
		Vector3[] output = new Vector3[input.Count];
		for(int i = 0; i < output.Length; i++)
			output[i] = ViewportToWorldPoint(input[i]);
		return output;
	}
	
	
	public float GetHorizontalFieldOfView () {
		return CameraX.GetHorizontalFieldOfView(fieldOfView, aspectRatio);
	}
	
	public float GetFrustrumHeightAtDistance (float distance) {
		return CameraX.GetFrustrumHeightAtDistance(distance, fieldOfView);
	}
	
	public float GetFrustrumWidthAtDistance (float distance) {
		return CameraX.GetFrustrumWidthAtDistance(distance, fieldOfView, aspectRatio);
	}
	
	public float GetDistanceAtFrustrumHeight (float frustumHeight) {
		return CameraX.GetDistanceAtFrustrumHeight(frustumHeight, fieldOfView);
	}
	
	public float GetDistanceAtFrustrumWidth (float frustumWidth) {
		return CameraX.GetDistanceAtFrustrumWidth(frustumWidth, fieldOfView, aspectRatio);
	}
	
	public float GetFOVAngleAtWidthAndDistance (float frustumWidth, float distance) {
		return CameraX.GetFOVAngleAtWidthAndDistance(frustumWidth, distance, aspectRatio);
	}
	
	public float ConvertFrustumWidthToFrustumHeight (float frustumWidth) {
		return CameraX.ConvertFrustumWidthToFrustumHeight(frustumWidth, aspectRatio);
	}
	
	public float ConvertFrustumHeightToFrustumWidth (float frustumHeight) {
		return CameraX.ConvertFrustumHeightToFrustumWidth(frustumHeight, aspectRatio);
	}
	
	public override string ToString ()
	{
		return string.Format ("[SerializableCamera: position={0}, rotation={1}, field of view={2}, aspect={3}, near={4}, far={5}]", position, rotation, fieldOfView, aspectRatio, nearClipPlane, farClipPlane);
	}
}