﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Implementation of the "Typesafe Enum" pattern. The major advantage is that it can be overriden.
/// Copied from http://stackoverflow.com/questions/4704201/what-is-the-best-way-to-override-enums
/// </summary>
public class TypesafeEnum {
	protected TypesafeEnum (string value) {
		this.Value = value;
	}
	
	public string Value { get; private set; }
	
	public override string ToString () {
		return Value;
	}
}


//public class MyDerivedEnum : TypesafeEnum
//{
//	public static readonly MyDerivedEnum A = new MyDerivedEnum("A");
//	
//	private MyDerivedEnum(string value)
//		: base(value)
//	{
//	}
//}
