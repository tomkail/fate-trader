using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityX.Geometry;

public class Gradient2DEditor
{
//	Vector2 scrollPos = new Vector2(0.0f, 0.0f);
	
//	Color handleInactiveColor = new Color(0.5f, 0.65f, 0.4f);
//	Color handleActiveColor = new Color(0.7f, 0.7f, 0.1f);
	
	float createNewPointDistance = 75f;
	
//	float snapInterval = 0.1f;
	
//	bool dragging = false;
	
	Rect viewRect;
	
	Gradient2D gradient2D;
	Rect gradientEditorRect;
	
	public Gradient2DEditor () {
		
	}
	
	public void SetProperties (Gradient2D gradient2D) {
		this.gradient2D = gradient2D;
	}
	
	public void DrawGradient2DView() {
		
		GUILayout.BeginVertical();
		
		float padding = 4;
		gradientEditorRect = new Rect(0,0,140,140);
//		gradientEditorRect = GUILayoutUtility.GetRect(128.0f, 128.0f, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
		gradientEditorRect.x += padding;
		gradientEditorRect.y += padding;
		gradientEditorRect.width -= padding * 2;
		gradientEditorRect.height -= padding * 2;
		EditorGrid.Draw(gradientEditorRect);
		
//		EditorGUILayout.LabelField ("Mouse Position: ", Event.current.mousePosition.ToString ());
		
		Texture2D texture = null;
		if(texture != null) MonoBehaviour.DestroyImmediate(texture);
		int width = gradientEditorRect.width.RoundToInt();
		int height = gradientEditorRect.height.RoundToInt();
		Color[] colors = gradient2D.ToColorArray(width, height);
		texture = TextureX.Create(new Point(width, height), colors);
		texture.name = "gradient";
		texture.Apply();
		GUILayout.Box(texture, GUILayout.Width(width), GUILayout.Height(height));
			
		
//		GUI.BeginGroup(gradientEditorRect);
//		DrawPolygonColliderEditor(gradient2D);
//		GUI.EndGroup();
		
		for (int i = 0; i < gradient2D.colorKeys.Length; ++i)
		{
			GUILayout.BeginVertical(GUI.skin.box);
			gradient2D.colorKeys[i].position = EditorGUILayout.Vector2Field("Position", gradient2D.colorKeys[i].position);
			gradient2D.colorKeys[i].position.x = Mathf.Clamp01(gradient2D.colorKeys[i].position.x);
			gradient2D.colorKeys[i].position.y = Mathf.Clamp01(gradient2D.colorKeys[i].position.y);
			gradient2D.colorKeys[i].color = EditorGUILayout.ColorField("Color", gradient2D.colorKeys[i].color);
			GUILayout.EndVertical();
		}
		
//		GUILayout.BeginHorizontal(GUI.skin.box, GUILayout.ExpandWidth(true), GUILayout.MinHeight(60), GUILayout.MaxHeight(60));
//		GUILayout.BeginVertical();
//		GUILayout.Label ("Draw Normals: "+drawNormals);
//		GUILayout.Label ("Polygon: "+polygon);
		
//		snapInterval = GUILayout.HorizontalSlider (snapInterval, 0, 1);
//		GUILayout.EndVertical();
//		GUILayout.BeginVertical();
		
//		GUILayout.Label ("Scroll Pos: "+scrollPos);
//		GUILayout.Label ("Polygon editor Rect: "+gradientEditorRect);
//		GUILayout.Label ("View Rect: "+viewRect);
//		
//		GUILayout.Label ("Display Scale: "+editorDisplayScale);
//		viewRect = EditorGUILayout.RectField ("View Rect", viewRect);
//		if(GUILayout.Button("")) {
//			viewRect.x = polygon.GetBounds().min.x * editorDisplayScale;
//			viewRect.y = polygon.GetBounds().min.y * editorDisplayScale;
//		}
//		GUILayout.EndVertical();
//		GUILayout.EndHorizontal();
		
//		GUILayout.EndVertical();
	}
	
	private void DrawPolygonColliderEditor(Gradient2D gradient2D) {
//		// Not sure what this does?
		Vector2 origin = Vector2.zero;

		Color previousHandleColor = Handles.color;
		bool insertPoint = false;
//		
		if (Event.current.type == EventType.MouseDown) {
			if (Event.current.clickCount == 2) {
				insertPoint = true;
				Event.current.Use();
			}
		}
//		
		float closestDistanceSq = 1.0e32f;
//		Vector2 closestPoint = Vector2.zero;
//		int closestPreviousPoint = 0;

//		Handles.color = handleInactiveColor;
//
//		Vector2 ov = (gradient2D.colorKeys.Length>0)?gradient2D.colorKeys[gradient2D.colorKeys.Length-1]:Vector2.zero;
		for (int i = 0; i < gradient2D.colorKeys.Length; ++i) {
			Debug.Log (gradient2D.colorKeys[i].position);
//			Vector2 v = gradient2D.colorKeys[i].position;
			
			if (insertPoint) {
			
//				Vector2 localMousePosition = (Event.current.mousePosition - origin);
//				Vector2 closestPointToCursor = ClosestPointToCursor(localMousePosition, ov, v);
//				float lengthSq = (closestPointToCursor - localMousePosition).sqrMagnitude;
//				if (lengthSq < closestDistanceSq)
//				{
//					closestDistanceSq = lengthSq;
//					closestPoint = closestPointToCursor;
//					closestPreviousPoint = i;
//				}
			}
			
//			Handles.DrawLine(v * editorDisplayScale + origin, ov * editorDisplayScale + origin);
			GUI.Button(new Rect(gradient2D.colorKeys[i].position.x * gradientEditorRect.width - 6, gradient2D.colorKeys[i].position.y * gradientEditorRect.height - 6, 12, 12), "");
//			ov = v;
		}
		Handles.color = previousHandleColor;
		
		if (insertPoint && closestDistanceSq < createNewPointDistance)
		{
			var tmpList = new List<Gradient2DColorKey>(gradient2D.colorKeys);
			tmpList.Add(new Gradient2DColorKey(Color.white, Vector2.one * 0.5f));
			gradient2D.colorKeys = tmpList.ToArray();
			HandleUtility.Repaint();
		}
		
		int deletedIndex = -1;
//		
//		Event ev = Event.current;
//		
		for (int i = 0; i < gradient2D.colorKeys.Length; ++i)
		{
			Vector3 cp = new Vector3(gradient2D.colorKeys[i].position.x * gradientEditorRect.x, gradient2D.colorKeys[i].position.y * gradientEditorRect.y, 0);
			int id = "Gradient2DEditor".GetHashCode() + 10000 + i;
			Vector2 guiPosition = cp + (Vector3)origin;
			cp = (EditorGuiUtility.Handle(EditorSkin.MoveHandle, id, guiPosition, true) - origin);
//			
//			
//			if (GUIUtility.keyboardControl == id) {
//				if(ev.control) {
////					if(holding)
//					cp = SnapPointToInterval(cp, snapInterval);
//				} else if(ev.type == EventType.KeyDown) {
//				
//					switch (ev.keyCode) {
//					case KeyCode.Backspace: 
//					case KeyCode.Delete: {
//						GUIUtility.keyboardControl = 0;
//						GUIUtility.hotControl = 0;
//						deletedIndex = i;
//						ev.Use();
//						break;
//					}
//					case KeyCode.LeftControl: {
//						if (!forceClosed) {
//							GUIUtility.keyboardControl = 0;
//							GUIUtility.hotControl = 0;
//							disconnectIsland = true;
//							ev.Use();
//						}
//						break;
//					}
//					
//					case KeyCode.T: {
//						if (!forceClosed) {
//							GUIUtility.keyboardControl = 0;
//							GUIUtility.hotControl = 0;
//							disconnectIsland = true;
//							ev.Use();
//						}
//						break;
//					}
//						
//					case KeyCode.Y: {
//						flipIsland = true;
//						GUIUtility.keyboardControl = 0;
//						GUIUtility.hotControl = 0;
//						ev.Use();
//						break;
//					}
//						
//					case KeyCode.Escape: {
//						GUIUtility.hotControl = 0;
//						GUIUtility.keyboardControl = 0;
//						ev.Use();
//						break;
//					}
//					}
//					
//				} 
//
//			}
//			
//
			EditorGuiUtility.SetPositionHandleValue(id, new Vector2(cp.x, cp.y));
			
			Vector2 labelSize = new Vector2(100,20);
			GUI.Box(new Rect(guiPosition.x - labelSize.x * 0.5f, guiPosition.y + 10, labelSize.x, labelSize.y), cp.ToString());
			
			gradient2D.colorKeys[i].position = new Vector2(cp.x / gradientEditorRect.x, cp.y / gradientEditorRect.y);
		}
//		
//		if (flipIsland) {
//			System.Array.Reverse(gradient2D.vertices);
//		}
//		
//		if (disconnectIsland){
//			gradient2D.connected = !gradient2D.connected;
//			if (gradient2D.connected && gradient2D.vertices.Length < 3)
//			{
//				Vector2 pp = (gradient2D.vertices[1] - gradient2D.vertices[0]);
//				float l = pp.magnitude;
//				pp.Normalize();
//				Vector2 nn = new Vector2(pp.y, -pp.x);
//				System.Array.Resize(ref gradient2D.vertices, gradient2D.vertices.Length + 1);
//				gradient2D.vertices[gradient2D.vertices.Length - 1] = (gradient2D.vertices[0] + gradient2D.vertices[1]) * 0.5f + nn * l * 0.5f;
//			}
//		}
		
		if (deletedIndex != -1 && gradient2D.colorKeys.Length > 1) {
			var tmpList = new List<Gradient2DColorKey>(gradient2D.colorKeys);
			tmpList.RemoveAt(deletedIndex);
			gradient2D.colorKeys = tmpList.ToArray();
		}
	}
	
	private Vector2 SnapPointToInterval (Vector2 point, float interval) {
		point.x = Mathf.Round(point.x / interval) * interval;
		point.y = Mathf.Round(point.y / interval) * interval;
		return point;
	}
	
	private Vector2 ClosestPointToCursor(Vector2 p, Vector2 p1, Vector2 p2) {
		float magSq = (p2 - p1).sqrMagnitude;
		if (magSq < float.Epsilon)
			return p1;
		
		float u = ((p.x - p1.x) * (p2.x - p1.x) + (p.y - p1.y) * (p2.y - p1.y)) / magSq;
		if (u < 0.0f || u > 1.0f)
			return p1;
		
		return p1 + (p2 - p1) * u;
	}
}