﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityX.Geometry;

using System.Linq;
using System;
using System.Reflection;

[CustomPropertyDrawer (typeof (Gradient2D))]
public class Gradient2DDrawer : PropertyDrawer {
	
	public override void OnGUI (Rect pos, SerializedProperty prop, GUIContent label) {
		GUI.Label(pos, prop.displayName);
		
		if (GUI.Button (new Rect(pos.x + 120, pos.y, pos.width - 120, pos.height), "Open Gradient Editor")) {
			Gradient2DEditorWindow gradient2DEditorWindow = EditorWindow.GetWindowWithRect( typeof(Gradient2DEditorWindow), new Rect(0,0,240,420), true, "Gradient2D" ) as Gradient2DEditorWindow;
			gradient2DEditorWindow.SetProperties(prop.GetBaseProperty<Gradient2D>());
		}
	}
}



//using UnityEngine;
//using UnityEditor;
//
//[CustomPropertyDrawer (typeof (Gradient2D))]
//public class Gradient2DDrawer : PropertyDrawer {
//	const int curveWidth = 50;
//	const float min = 0;
//	const float max = 1;
//	public override void OnGUI (Rect pos, SerializedProperty prop, GUIContent label) {
//		SerializedProperty scale = prop.FindPropertyRelative ("colorKeys");
////		SerializedProperty curve = prop.FindPropertyRelative ("curve");
//		
////		Rect r = EditorGUILayout.BeginHorizontal ("Button");
//		if (GUI.Button (pos, "Open Gradient Editor")) {
//			Debug.Log ("Open Gradient Editor");
//			
//			Gradient2DEditorWindow v = EditorWindow.GetWindowWithRect( typeof(Gradient2DEditorWindow), new Rect(0, 0, 600, 400), true, "Gradient 2D" ) as Gradient2DEditorWindow;
////			v.SetProperties(gen);
//			Gradient2DEditorWindow.Show();
//		}
//		
//			
////		GUILayout.Label ("I'm inside the button");
////		GUILayout.Label ("So am I");	
////		EditorGUILayout.EndHorizontal ();
////		// Draw scale
////		EditorGUI.Slider (
////			new Rect (pos.x, pos.y, pos.width - curveWidth, pos.height),
////			scale, min, max, label);
////		
////		// Draw curve
////		int indent = EditorGUI.indentLevel;
////		EditorGUI.indentLevel = 0;
////		EditorGUI.PropertyField (
////			new Rect (pos.width - curveWidth, pos.y, curveWidth, pos.height),
////			curve, GUIContent.none);
////		EditorGUI.indentLevel = indent;
//	}
//}