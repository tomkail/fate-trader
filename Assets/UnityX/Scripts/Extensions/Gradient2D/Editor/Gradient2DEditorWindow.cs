﻿using UnityEngine;
using UnityEditor;

public class Gradient2DEditorWindow : EditorWindow {
	
	public Gradient2D gradient2D;
	
	private Gradient2DEditor gradient2DEditor;
	
	void OnEnable() {
		gradient2DEditor = new Gradient2DEditor();
	}
	
	void OnDisable() {
		gradient2DEditor = null;
	}
	
	public void SetProperties(Gradient2D _gradient2D) {
		gradient2D = _gradient2D;
		gradient2DEditor.SetProperties(gradient2D);
	}
	
	void OnGUI() {
		gradient2DEditor.DrawGradient2DView();
		Repaint();
	}
	
	private EditorWindow colorPickerWindow;
	void OnLostFocus() {
		Debug.Log (EditorWindow.focusedWindow.ToString()+" "+ EditorWindow.focusedWindow.titleContent +" "+(EditorWindow.focusedWindow.ToString() == " (UnityEditor.ColorPicker)").ToString());
//		if(EditorWindow.focusedWindow.ToString() != "(UnityEditor.ColorPicker)")
		if(EditorWindow.focusedWindow.titleContent.ToString() == "Color") {
			colorPickerWindow = EditorWindow.focusedWindow;
			Debug.Log(colorPickerWindow);
		} else {
			this.Close();
		}
	}
}


//public class Gradient2DEditorWindow : EditorWindow {
//	
//	[MenuItem("Window/Gradient2D Editor Test")]
//	public static void Show() {
//		GetWindow<Gradient2DEditorWindow>("Gradient2D Editor Test Window");
//	}
//	
//	Gradient2D gradient;
//	float time;
//	
//	GUIStyle previewStyle;
//	public Texture2D texture;
//	
//	void OnEnable() {
//		previewStyle = new GUIStyle();
//		previewStyle.normal.background = EditorGUIUtility.whiteTexture;
//	}
//	
//	public void SetProperties () {
//	
//	}
//	
//	void OnGUI() {
//		gradient = GUIGradient2DField.GradientField("Test Gradient2D", gradient);
//		
//		if (GUILayout.Button("Set Keys Test")) {
//			OnSetKeysTest();
//			GUIUtility.ExitGUI();
//		}
//		
//		time = Mathf.Clamp01(EditorGUILayout.Slider("Time", time, 0, 1));
//		
//		// Extract color at specific time.
//		Color restoreBackgroundColor = GUI.backgroundColor;
//		GUI.backgroundColor = gradient.Evaluate(new Vector2(time, time));
//		GUILayout.Box(GUIContent.none, previewStyle, GUILayout.Width(200), GUILayout.Height(50));
//		GUI.backgroundColor = restoreBackgroundColor;
//		
//		
//		
//		wantsMouseMove = EditorGUILayout.Toggle ("Receive Movement: ", wantsMouseMove);
//		EditorGUILayout.LabelField ("Mouse Position: ", Event.current.mousePosition.ToString ());
//		
//		
//		if(texture != null) DestroyImmediate(texture);
//		int width = 100;
//		int height = 100;
//		Color[] colors = gradient.Create(width, height);
//		// for(int i = 0; i < colors.Length; i++) Debug.Log(colors[i]);
//		texture = TextureX.Create(new Point(width, height), colors);
//		texture.name = "gradient";
//		texture.Apply();
//		EditorGUILayout.ObjectField(texture, typeof(Texture2D), false, GUILayout.Width(width), GUILayout.Height(height));
//		
//		// Repaint the window as wantsMouseMove doesnt trigger a repaint automatically
//		if (Event.current.type == EventType.MouseMove)
//			Repaint ();
//			
//		
//		BeginWindows();
//		
//		EndWindows();
//		
//		Color x = EditorGUI.ColorField(new Rect(3,150,position.width - 6, 15), "New Color:", Color.white); 
//		
//		
//	}
//	
//	void WindowFunction (int windowID) 
//	{
//		GUI.DragWindow();
//	}
//	
//	void OnSetKeysTest() {
//		Gradient2DColorKey[] colorKeys = new Gradient2DColorKey[] {
//			new Gradient2DColorKey(Color.white, new Vector2(0,0)),
//			new Gradient2DColorKey(Color.red, new Vector2(0,1)),
//			new Gradient2DColorKey(Color.green, new Vector2(1,0)),
//			new Gradient2DColorKey(Color.blue, Vector2.one)
//		};
//		
//		colorKeys = new Gradient2DColorKey[] {
//			new Gradient2DColorKey(new Color(1,0,1,0), new Vector2(0,0)),
//			new Gradient2DColorKey(new Color(0,0,0,0), new Vector2(0,1)),
//			new Gradient2DColorKey(new Color(0,1,1,0), new Vector2(1,0)),
//			new Gradient2DColorKey(new Color(1,1,0,0), new Vector2(1,1))
//		};
//		gradient.SetKeys(colorKeys);
//	}
//}