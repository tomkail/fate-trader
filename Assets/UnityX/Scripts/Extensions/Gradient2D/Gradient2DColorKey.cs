﻿using UnityEngine;
using System.Collections;

public struct Gradient2DColorKey {
	public Color color;
	public Vector2 position;
	public float strength;
	
	public Gradient2DColorKey (Color col, Vector2 pos, float _strength = 1) {
		color = col;
		position = pos;
		strength = _strength;
	}
}