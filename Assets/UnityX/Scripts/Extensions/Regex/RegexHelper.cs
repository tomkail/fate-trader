﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public static class RegexHelper {
	
	public enum CommonRegexPattern {
		URL,
		EmailAddress,
		HexCode,
		IPAddress,
		HTMLTag
	}
	
	public static IDictionary<CommonRegexPattern, string> commonRegexPatterns {
		get {
			return _commonRegexPatterns;
		}
	}
	
	#region private
	
	private static IDictionary<CommonRegexPattern, string> _commonRegexPatterns = new Dictionary<CommonRegexPattern, string>() {
		{CommonRegexPattern.URL,			@"^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$"},
		{CommonRegexPattern.EmailAddress,	@"^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$"},
		{CommonRegexPattern.HexCode,		@"^#?([a-f0-9]{6}|[a-f0-9]{3})$"},
		{CommonRegexPattern.IPAddress,		@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"},
		{CommonRegexPattern.HTMLTag,		@"^<([a-z]+)([^<]+)*(?:>(.*)<\/\1>|\s+\/>)$"}
	};
	
	#endregion
}
