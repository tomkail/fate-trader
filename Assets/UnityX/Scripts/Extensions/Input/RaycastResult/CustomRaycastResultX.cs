using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class CustomRaycastResultX {

	public static CustomRaycastResult[] ScreenPointToSortedRaycastResults (this Camera _camera, Vector3 _position, float _length = 10f) {
		Ray ray = _camera.ScreenPointToRay(_position);
		RaycastHit[] hits = Physics.RaycastAll(ray, _length);
		return CustomRaycastResultX.SortRaycastResultsByDistance(hits);
	}

	public static CustomRaycastResult[] SortRaycastResultsByDistance (RaycastHit[] raycastHits) {
		if(raycastHits.IsNullOrEmpty()) {
			Debug.LogError (DebugX.LogString("RaycastHits is null or empty."));
			return null;
		}
		List<CustomRaycastResult> hitList = new List<CustomRaycastResult>();
		foreach(RaycastHit hit in raycastHits) {
			hitList.Add(new CustomRaycastResult(hit));
		}
		hitList.Sort();
		return hitList.ToArray();
	}

	public static void DisplaySortedHits (Vector3 origin, IList<CustomRaycastResult> _sortedHits) {
		if(_sortedHits.IsNullOrEmpty()) return;
		Debug.DrawLine(origin, _sortedHits[0].point, Color.yellow);
		for(int i = 0; i < _sortedHits.Count; i++) {
			Color color = (Color)new HSLColor(i * 45f, 1f, 0.5f);
			if(i == 0) {
				Debug.DrawLine(origin, _sortedHits[i].point, color);
			} else {
				Debug.DrawLine(_sortedHits[i-1].point, _sortedHits[i].point, color);
			}
		}
	}
}
