﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Gesture {

	public virtual void InitGesture () {
	
	}
	
	public virtual void UpdateGesture () {
	
	}
}
