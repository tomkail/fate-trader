﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Pinch : Gesture {
	public InputPoint firstFinger;
	public InputPoint secondFinger;

	public override void InitGesture () {
		base.InitGesture();
	}
	
	public override void UpdateGesture () {
		base.UpdateGesture();
	}
}
