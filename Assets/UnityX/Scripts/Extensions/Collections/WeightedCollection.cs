using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
	
/// <summary>
/// Weighted collection.
/// </summary>
public class WeightedCollection<TKey> : Dictionary<TKey, float>{
	
	public WeightedCollection():base(){}
	public WeightedCollection(int capacity):base(capacity){}
	
	/// <summary>
	/// Normalizes the values in the collection.
	/// </summary>
	public void Normalize (float max = 1) {
		IList<float> newValues = Values.ToArray().Normalize(max);
		int index = 0;
		foreach (TKey key in Keys.ToList()) {
			this[key] = newValues[index];
			index++;
		}
	}
	
	/// <summary>
	/// Returns a randomly weighted item from the dictionary.
	/// </summary>
	public TKey Generate () {
		if(Count == 0) {
			Debug.LogError("Could not generate because weighted collection length is 0! Returning default.");
			return default(TKey);
		}
		return RandomX.Weighted<TKey>(Keys.ToList(), Values.ToList());
	}
}