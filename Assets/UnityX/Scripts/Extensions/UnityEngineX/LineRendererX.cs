﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public static class LineRendererX {
	
	public static void SetPositions (this LineRenderer lineRenderer, IList<Vector3> positions) {
		lineRenderer.SetVertexCount(positions.Count);
		for(int i = 0; i < positions.Count; i++) {
			lineRenderer.SetPosition(i, positions[i]);
		}
	}
	
	//DrawEllipse(ref line, 20, 5, 5, 45);
	public static void DrawEllipse (ref LineRenderer line, int segments, float xRadius, float yRadius, float startAngle = 0) {
		line.SetVertexCount (segments + 1);
		line.useWorldSpace = false;
		float x,y,z = 0;
		float angle = startAngle;
		for (int i = 0; i <= segments; i++) {
			x = Mathf.Sin (Mathf.Deg2Rad * angle) * xRadius;
			y = Mathf.Cos (Mathf.Deg2Rad * angle) * yRadius;
			line.SetPosition (i, new Vector3(x,y,z));
			angle += (360f / segments);
		}
	}
}