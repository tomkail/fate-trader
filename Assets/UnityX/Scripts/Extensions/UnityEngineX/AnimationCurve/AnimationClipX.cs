﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections;
using System.IO;

public static class AnimationClipX {
	#if UNITY_EDITOR
	/// <summary>
	/// Reverses the animation clip.
	/// Probably doesn't reverse the time of events, as I never use them and haven't tested it.
	/// </summary>
	/// <param name="clip">Clip.</param>
	public static void ReverseAnimationClip (AnimationClip clip) {
//		Replace with something like this - 
//		foreach (var binding in AnimationUtility.GetObjectReferenceCurveBindings (clip)) {
//			ObjectReferenceKeyframe[] keyframes = AnimationUtility.GetObjectReferenceCurve (clip, binding);
//
//			EditorGUILayout.LabelField (binding.path + "/" + binding.propertyName + ", Keys: " + keyframes.Length);
//		}


		AnimationClipCurveData[] curveDatas = AnimationUtility.GetAllCurves(clip, true);
		clip.ClearCurves();
		
		float longest = 0;
		foreach(AnimationClipCurveData curveData in curveDatas) {
			longest = Mathf.Max (longest, curveData.curve.keys.Last().time);
		}
		
		foreach(AnimationClipCurveData curveData in curveDatas) {
			AnimationCurve curve = AnimationClipX.Reverse(curveData.curve, longest);
			clip.SetCurve(curveData.path, curveData.type, curveData.propertyName, curve);
		}
	}
	#endif

	public static AnimationCurve Reverse(AnimationCurve curve, float width){
		Keyframe[] keys = curve.keys;
		for(int i = 0; i < keys.Length; i++) {
			keys[i].time = width - keys[i].time;
			keys[i].inTangent = -keys[i].inTangent;
			keys[i].outTangent = -keys[i].outTangent;
		}
		return new AnimationCurve(keys);
	}
	
	#if UNITY_EDITOR
	/// <summary>
	/// Duplicates the animation clip.
	/// </summary>
	/// <returns>The animation clip.</returns>
	/// <param name="sourceClip">Source clip.</param>
	public static AnimationClip DuplicateAnimationClip (AnimationClip sourceClip, string name = null){
		if (sourceClip == null) return null;
		if(name == null) name = sourceClip.name;
		string path = AssetDatabase.GetAssetPath(sourceClip);
		path = Path.Combine(Path.GetDirectoryName(path), name) + ".anim";
		string newPath = AssetDatabase.GenerateUniqueAssetPath (path);
		AnimationClip newClip = new AnimationClip();
		EditorUtility.CopySerialized(sourceClip, newClip);
		AssetDatabase.CreateAsset(newClip, newPath);
		return newClip;
	}
	#endif
}