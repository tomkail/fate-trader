using UnityEngine;

[System.Serializable]
public class Vector4Curve : BaseCurve<Vector4> {
	public AnimationCurve xCurve;
	public AnimationCurve yCurve;
	public AnimationCurve zCurve;
	public AnimationCurve wCurve;

	public float length {
		get {
			return GetLength();
		}
	}
	
	public Vector4Curve() {
		xCurve = new AnimationCurve();
		yCurve = new AnimationCurve();
		zCurve = new AnimationCurve();
		wCurve = new AnimationCurve();
	}

	public Vector4Curve(AnimationCurve _xCurve, AnimationCurve _yCurve, AnimationCurve _zCurve, AnimationCurve _wCurve) {
		this.xCurve = _xCurve;
		this.yCurve = _yCurve;
		this.zCurve = _zCurve;
		this.wCurve = _zCurve;
	}

	public void Set(AnimationCurve _xCurve, AnimationCurve _yCurve, AnimationCurve _zCurve, AnimationCurve _wCurve){
		this.xCurve = _xCurve;
		this.yCurve = _yCurve;
		this.zCurve = _zCurve;
		this.wCurve = _zCurve;
	}
	
	public override Vector4 Evaluate(float distance){
		return new Vector4(xCurve.Evaluate(distance), yCurve.Evaluate(distance), zCurve.Evaluate(distance), wCurve.Evaluate(distance));
	}

	public void AddKey(float time, float x, float y, float z, float w){
		xCurve.AddKey(time, x);
		yCurve.AddKey(time, y);
		zCurve.AddKey(time, z);
		wCurve.AddKey(time, w);
	}

	public override void AddKey(float time, Vector4 vector){
		AddKey(time, vector.x, vector.y, vector.z, vector.w);
	}

	public void AddKeys(Keyframe xKey, Keyframe yKey, Keyframe zKey, Keyframe wKey){
		xCurve.AddKey(xKey);
		yCurve.AddKey(yKey);
		zCurve.AddKey(zKey);
		zCurve.AddKey(wKey);
	}

	public override void SetPostWrapMode (WrapMode _postWrapMode) {
		xCurve.postWrapMode = _postWrapMode;
		yCurve.postWrapMode = _postWrapMode;
		zCurve.postWrapMode = _postWrapMode;
		wCurve.postWrapMode = _postWrapMode;
	}
	
	public override void Clear(){
		this.xCurve = new AnimationCurve();
		this.yCurve = new AnimationCurve();
		this.zCurve = new AnimationCurve();
		this.wCurve = new AnimationCurve();
	}
	
	public override void RemoveKeysBefore (float time) {
		this.xCurve.RemoveKeysBefore(time);
		this.yCurve.RemoveKeysBefore(time);
		this.zCurve.RemoveKeysBefore(time);
		this.wCurve.RemoveKeysBefore(time);
	}
	
	public override void RemoveKeysBeforeAndIncluding (float time) {
		this.xCurve.RemoveKeysBeforeAndIncluding(time);
		this.yCurve.RemoveKeysBeforeAndIncluding(time);
		this.zCurve.RemoveKeysBeforeAndIncluding(time);
		this.wCurve.RemoveKeysBeforeAndIncluding(time);
	}
	
	public override void RemoveKeysAfter (float time) {
		this.xCurve.RemoveKeysAfter(time);
		this.yCurve.RemoveKeysAfter(time);
		this.zCurve.RemoveKeysAfter(time);
		this.wCurve.RemoveKeysAfter(time);
	}
	
	public override void RemoveKeysAfterAndIncluding (float time) {
		this.xCurve.RemoveKeysAfterAndIncluding(time);
		this.yCurve.RemoveKeysAfterAndIncluding(time);
		this.zCurve.RemoveKeysAfterAndIncluding(time);
		this.wCurve.RemoveKeysAfterAndIncluding(time);
	}
	
	public override void RemoveKeysBetween (float startTime, float endTime) {
		this.xCurve.RemoveKeysBetween(startTime, endTime);
		this.yCurve.RemoveKeysBetween(startTime, endTime);
		this.zCurve.RemoveKeysBetween(startTime, endTime);
		this.wCurve.RemoveKeysBetween(startTime, endTime);
	}

	public override float GetLength () {
		return new float[4]{xCurve.length, yCurve.length, zCurve.length, wCurve.length}.Largest();
	}
	
	public override float GetWidth () {
		return new float[4]{xCurve.GetWidth(), yCurve.GetWidth(), zCurve.GetWidth(), wCurve.GetWidth()}.Largest();
	}
	
	public override float GetHeight () {
		return new float[4]{xCurve.GetHeight(), yCurve.GetHeight(), zCurve.GetHeight(), wCurve.GetHeight()}.Largest();
	}
}