﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class EasingFunctions
{
    /// <summary>
    /// Quadratic ease in function
    /// </summary>
    /// <param name="t">Current time</param>
    /// <param name="b">Start value</param>
    /// <param name="c">Change in value</param>
    /// <param name="d">Duration</param>
    /// <returns></returns>
    public static float EaseInQuad(float t, float b, float c, float d)
    {
        t /= d;
        return c * t * t + b;
    }
}
