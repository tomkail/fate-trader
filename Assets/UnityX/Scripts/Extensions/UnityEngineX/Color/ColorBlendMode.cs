﻿public enum ColorBlendMode {
	Normal,
	Additive,
	Multiply,
	Screen,
	Overlay,
	Darken,
	Lighten,
	Difference,
	Hue,
	Saturation,
	Color,
	Luminosity
}