﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class RendererX {
	
	///
	///Bounds
	///
	
	public static Bounds GetBounds(this Renderer parent, bool getDisabledBounds = false) {
		List<Bounds> bounds = new List<Bounds>();
		parent.GetVisibleBounds(ref bounds, getDisabledBounds);
		return BoundsX.GetCombinedBounds(bounds);
	}
	
	private static void GetVisibleBounds(this Renderer parent, ref List<Bounds> bounds, bool getDisabledBounds) {
		if(parent.enabled || getDisabledBounds)
			bounds.Add(parent.bounds);
		foreach(Transform child in parent.transform) {
			if(child.gameObject.activeInHierarchy) {
				Renderer childRenderer = child.GetComponent<Renderer>();
				if(childRenderer != null)
					GetVisibleBounds(childRenderer, ref bounds, getDisabledBounds);
			}
		}
	}
}
