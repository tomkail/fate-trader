﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public static class AssemblyX {
	
	
	/// <summary>
	/// Shortcut helper function that gets all the types in specified assemblies in the current domain.
	/// </summary>
	/// <returns>The types.</returns>
	/// <param name="assemblyNames">Assembly names.</param>
	public static IList<System.Type> GetTypes (params string[] assemblyNames) {
		List<System.Type> types	= new List<System.Type>();
		Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
		for(int i = 0; i < referencedAssemblies.Length; ++i)
			for(int j = 0; j < assemblyNames.Length; ++j)
				if(referencedAssemblies[i].GetName().Name == assemblyNames[j])
					types.AddMany(referencedAssemblies[i].GetTypes());
		return types;
	}
}
