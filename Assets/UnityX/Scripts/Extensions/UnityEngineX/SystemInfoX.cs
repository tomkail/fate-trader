﻿using UnityEngine;
using System.Collections;

public static class SystemInfoX {
	
	public static bool IsMacOS {
		get {
			return UnityEngine.SystemInfo.operatingSystem.IndexOf("Mac OS") != -1;
//			Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXWebPlayer || Application.platform == RuntimePlatform.OSXDashboardPlayer
		}
	}
	
	public static bool IsWinOS {
		get {
			return UnityEngine.SystemInfo.operatingSystem.IndexOf("Windows") != -1;
//			Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer || Application.platform == RuntimePlatform.WindowsEditor
		}
	}
}