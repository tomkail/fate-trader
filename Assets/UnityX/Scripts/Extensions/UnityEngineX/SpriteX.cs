﻿using UnityEngine;
using System.Collections;

public static class SpriteX {
	
	/// <summary>
	/// Returns the pivot in the coordinate space (0,0) to (1,1), where (0.5,0.5) is the center.
	/// </summary>
	/// <returns>The local pivot.</returns>
	/// <param name="sprite">Sprite.</param>
	public static Vector2 GetLocalPivot (this Sprite sprite) {
		return new Vector2(sprite.pivot.x/sprite.rect.width, sprite.pivot.y/sprite.rect.height);
	}
	
	/// <summary>
	/// Creates a 1x1 colored sprite.
	/// </summary>
	/// <returns>The colored sprite.</returns>
	/// <param name="newColor">New color.</param>
	/// <param name="pixelsPerUnit">Pixels per unit.</param>
	public static Sprite CreateColoredSprite (Color newColor, float pixelsPerUnit) {
		Texture2D newTexture = TextureX.Create(newColor);
		newTexture.Apply();
		Sprite newSprite = Sprite.Create(newTexture, new Rect(0,0,1,1), new Vector2(0.5f,0.5f), pixelsPerUnit);
		return newSprite;
	}
	
	/// <summary>
	/// Gets the sprite alignment as an enum.
	/// </summary>
	/// <returns>The sprite alignment.</returns>
	/// <param name="sprite">Sprite.</param>
	public static SpriteAlignment GetSpriteAlignment(GameObject sprite){
		BoxCollider2D boxCollider = sprite.AddComponent<BoxCollider2D> ();
		float colX = boxCollider.offset.x;
		float colY = boxCollider.offset.y;
		if (colX > 0f && colY < 0f)
			return (SpriteAlignment.TopLeft);
		else if (colX < 0 && colY < 0)
			return (SpriteAlignment.TopRight);
		else if (colX == 0 && colY < 0)
			return (SpriteAlignment.TopCenter);
		else if (colX > 0 && colY == 0)
			return (SpriteAlignment.LeftCenter);
		else if (colX < 0 && colY == 0)
			return (SpriteAlignment.RightCenter);
		else if (colX > 0 && colY > 0)
			return (SpriteAlignment.BottomLeft);
		else if (colX < 0 && colY > 0)
			return (SpriteAlignment.BottomRight);
		else if (colX == 0 && colY > 0)
			return (SpriteAlignment.BottomCenter);
		else if (colX == 0 && colY == 0)
			return (SpriteAlignment.Center);
		else
			return (SpriteAlignment.Custom);
	}
}
