using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public static class HandlesX {
	#if UNITY_EDITOR
	public static void DrawBoxedLabel (Vector3 position, string text, TextAnchor textAnchor = TextAnchor.MiddleCenter) {
		GUIStyle labelStyle = new GUIStyle(EditorStyles.helpBox);
		labelStyle.alignment = textAnchor;
		Handles.Label(position, text, labelStyle);
		Handles.Label(position, text, labelStyle);
		Handles.Label(position, text, labelStyle);
	}
	
	public static void DrawDottedLine (float screenSpaceSize, params Vector3[] points) {
		for(int i = 0; i < points.Length - 1; i++) {
			Handles.DrawDottedLine(points[i], points[i+1], screenSpaceSize);
		}
	}
	
	public static void DrawWireRect (Vector3 topLeft, Vector3 topRight, Vector3 bottomLeft, Vector3 bottomRight) {
		Handles.DrawLine(topLeft, topRight);
		Handles.DrawLine(topRight, bottomRight);
		Handles.DrawLine(bottomRight, bottomLeft);
		Handles.DrawLine(bottomLeft, topLeft);
	}
	
	#endif
}
