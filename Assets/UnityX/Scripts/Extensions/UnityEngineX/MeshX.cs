﻿using UnityEngine;
using System.Collections;

public static class MeshX {

	/// <summary>
	/// Randoms a random point in a triangle.
	/// </summary>
	/// <returns>The point in triangle.</returns>
	/// <param name="a">The first component in the triangle.</param>
	/// <param name="b">The second component in the triangle.</param>
	/// <param name="c">The third component in the triangle.</param>
	public static Vector3 RandomPointInTriangle (Vector3 a, Vector3 b, Vector3 c) {
		return PointInTriangleFromBarycentricCoordinates(a, b, c, RandomBarycentricCoordinates());
	}
	
	/// <summary>
	/// Points the in triangle from barycentric coordinates.
	/// </summary>
	/// <returns>The in triangle from barycentric coordinates.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	/// <param name="c">C.</param>
	/// <param name="barycentricCoordinates">Barycentric coordinates.</param>
	public static Vector3 PointInTriangleFromBarycentricCoordinates (Vector3 a, Vector3 b, Vector3 c, Vector3 barycentricCoordinates) {
		return a * barycentricCoordinates.x + b * barycentricCoordinates.y + c * barycentricCoordinates.z;
	}
	
	/// <summary>
	/// Random barycentric coordinates.
	/// </summary>
	/// <returns>The barycentric coordinates.</returns>
	public static Vector3 RandomBarycentricCoordinates () {
		float x = Random.value;
		float y = (1f - x) * Random.value;
		float z = 1f - x - y;
		return new Vector3(x, y, z);
	}
	
	/// <summary>
	/// Gets the point on mesh.
	/// </summary>
	/// <returns>The point on mesh.</returns>
	/// <param name="mesh">Mesh.</param>
	public static RaycastHit GetPointOnMesh(Mesh mesh) { 
		RaycastHit hit = new RaycastHit(); 
		Vector3[] vertices = mesh.vertices;
		int[] triangles = mesh.triangles;
		
		int triangleIndex = Random.Range(0,mesh.triangles.Length-2); 
		hit.barycentricCoordinate = RandomBarycentricCoordinates();
		
		Vector3 P1 = vertices[triangles[triangleIndex + 0]];
		Vector3 P2 = vertices[triangles[triangleIndex + 1]];
		Vector3 P3 = vertices[triangles[triangleIndex + 2]];
		hit.point = PointInTriangleFromBarycentricCoordinates(P1, P2, P3, hit.barycentricCoordinate);
		
		// Interpolated vertex normal
		if(!mesh.normals.IsNullOrEmpty()) {
			Vector3 N1 = mesh.normals[mesh.triangles[triangleIndex + 0]];
			Vector3 N2 = mesh.normals[mesh.triangles[triangleIndex + 1]];
			Vector3 N3 = mesh.normals[mesh.triangles[triangleIndex + 2]];
			hit.normal = N1*hit.barycentricCoordinate.x + N2*hit.barycentricCoordinate.y + N3*hit.barycentricCoordinate.z;
		}
		
		return hit;
	}
	
	
	
	public static Mesh CreateMesh (Vector3[] vertices, Vector2[] uvs, int[] triangles, bool recalculateNormals = true, bool recalculateBounds = true, bool optimize = true) {
		Mesh mesh = new Mesh();
		return CreateMesh (mesh, vertices, uvs, triangles, recalculateNormals, recalculateBounds, optimize);
	}

	public static Mesh CreateMesh (Mesh mesh, Vector3[] vertices, Vector2[] uvs, int[] triangles, bool recalculateNormals = true, bool recalculateBounds = true, bool optimize = true) {
		mesh.vertices = vertices;
		mesh.uv = uvs;
		mesh.triangles = triangles;
		if(recalculateNormals) mesh.RecalculateNormals();
		if(recalculateBounds) mesh.RecalculateBounds();
		if(optimize) mesh.Optimize();
		return mesh;
	}

	public static void SetMeshFilter (MeshFilter meshFilter, Mesh mesh) {
		if(meshFilter != null && mesh != null){
			//Sets the mesh filter
			meshFilter.mesh = mesh;
		}
	}

	public static void SetMeshCollider (MeshCollider meshCollider, Mesh mesh) {
		if(meshCollider != null && mesh != null){
			//Clears the mesh collider
			meshCollider.sharedMesh = null;
			//Sets the mesh collider
			meshCollider.sharedMesh = mesh;
		}
	}

	public static Mesh ReverseNormal (Mesh mesh) {
		Vector3[] normals = mesh.normals;
		for (int i=0;i<normals.Length;i++)
			normals[i] = -normals[i];
		mesh.normals = normals;
		
		for (int m=0;m<mesh.subMeshCount;m++) {
			int[] triangles = mesh.GetTriangles(m);
			for (int i=0;i<triangles.Length;i+=3) {
				int temp = triangles[i + 0];
				triangles[i + 0] = triangles[i + 1];
				triangles[i + 1] = temp;
			}
			mesh.SetTriangles(triangles, m);
		}
		return mesh;
	}

	public static Mesh Morph (Mesh refMesh, Mesh fromMesh, Mesh toMesh, float t) {
		Vector3[] v0 = fromMesh.vertices;
		Vector3[] v1 = toMesh.vertices;
		Vector3[] vdst = new Vector3[refMesh.vertexCount];
		
		if (!MathX.Same(v0.Length, v1.Length, vdst.Length)) {
			Debug.LogError("Count not Morph meshes because their lengths are different.");
			return refMesh;
		}

		for (int i=0;i<vdst.Length;i++)
			vdst[i] = Vector3.Lerp(v0[i], v1[i], t);

		refMesh.vertices = vdst;
		refMesh.RecalculateBounds();
		return refMesh;
    }
}
