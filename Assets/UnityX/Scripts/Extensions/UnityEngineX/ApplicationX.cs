﻿using UnityEngine;

public static class ApplicationX {

	public static bool isWindows {
		get {
			return Application.platform == RuntimePlatform.WindowsEditor ||
			       Application.platform == RuntimePlatform.WindowsPlayer ||
			       Application.platform == RuntimePlatform.WindowsWebPlayer; } }

	public static bool isOSX {
		get {
			return Application.platform == RuntimePlatform.OSXEditor ||
			       Application.platform == RuntimePlatform.OSXPlayer ||
			       Application.platform == RuntimePlatform.OSXWebPlayer ||
			       Application.platform == RuntimePlatform.OSXDashboardPlayer; } }
}
