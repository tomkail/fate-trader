﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class EnumerableX {
	
	
	
    public static T Random<T>(this IEnumerable<T> source)
    {
		if(source.Count() == 0) return default(T);
		return source.ElementAt(source.RandomIndex());
    }
    
	public static int RandomIndex<T>(this IEnumerable<T> source)
	{
		return UnityEngine.Random.Range(0, source.Count());
	}
	
//    public static IEnumerable<T> Random<T>(this IEnumerable<T> source, int count)
//    {
//        return source.Shuffle().Take(count);
//    }

//    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
//    {
//        return source.OrderBy(x => Guid.NewGuid());
//    }
}