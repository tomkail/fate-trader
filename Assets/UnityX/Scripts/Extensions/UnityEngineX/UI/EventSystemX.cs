﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class EventSystemX {

	public static RaycastResult Raycast (Vector2 screenPos) {
		return RaycastAll (screenPos).FirstOrDefault();
	}
	
	public static RaycastResult Raycast (Vector2 screenPos, LayerMask layerMask) {
		return RaycastAll (screenPos, layerMask).FirstOrDefault();
	}
	
	public static bool Raycast (Vector2 screenPos, out RaycastResult raycastResult) {
		raycastResult = Raycast(screenPos);
		return raycastResult.isValid;
	}
	
	public static bool Raycast (Vector2 screenPos, LayerMask layerMask, out RaycastResult raycastResult) {
		raycastResult = Raycast (screenPos, layerMask);
		return raycastResult.isValid;
	}
	
	/// <summary>
	/// Performs a raycast through the entire scene including the UI. If no UI element is hit then elements
	/// in the 3D scene will be tested.
	/// </summary>
	/// <returns>A list of elements we hit. Closest hit first.</returns>
	/// <param name="screenPos">The screen position at which the cast should be performed.</param>
	public static IList<RaycastResult> RaycastAll (Vector2 screenPos) {
		if(EventSystem.current == null) {
			Debug.LogWarning("Tried to raycast into the event system, but no event system exists.");
			return null;
		}
		
		PointerEventData eventData = new PointerEventData (EventSystem.current);
		eventData.position = screenPos;
		List<RaycastResult> hits = new List<RaycastResult> ();
		EventSystem.current.RaycastAll (eventData, hits);
		return hits;
	}
	
	/// <summary>
	/// Performs a raycast through the entire scene including the UI. If no UI element is hit then elements
	/// in the 3D scene will be tested.
	/// </summary>
	/// <returns>A list of elements we hit. Closest hit first.</returns>
	/// <param name="screenPos">The screen position at which the cast should be performed.</param>
	public static IList<RaycastResult> RaycastAll (Vector2 screenPos, LayerMask layerMask) {
		if(EventSystem.current == null) {
			Debug.LogWarning("Tried to raycast into the event system, but no event system exists.");
			return null;
		}
		
		PointerEventData eventData = new PointerEventData (EventSystem.current);
		eventData.position = screenPos;
		IList<RaycastResult> hits = EventSystem.current.RaycastAll (eventData, layerMask);
		return hits;
	}
	
	/// <summary>
	/// Raycast into the scene using all configured BaseRaycasters, using a layer mask.
	/// </summary>
	/// <param name="eventSystem">Event system.</param>
	/// <param name="eventData">Event data.</param>
	/// <param name="layerMask">Layer mask.</param>
	/// <param name="raycastResults">Raycast results.</param>
	public static IList<RaycastResult> RaycastAll(this EventSystem eventSystem, PointerEventData eventData, LayerMask layerMask) {
		List<RaycastResult> hits = new List<RaycastResult> ();
		eventSystem.RaycastAll (eventData, hits);
		return hits.Where(hit => layerMask.Includes(hit.gameObject.layer)).ToList();
	}
}
