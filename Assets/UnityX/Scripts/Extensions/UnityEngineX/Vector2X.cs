﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Vector2X {

	public static float AngleDir(Vector2 A, Vector2 B) {
		return -A.x * B.y + A.y * B.x;
	}

	/// <summary>
	/// The number of components in the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static int NumComponents(this Vector2 v) {
		return 2;
	}
	
	/// <summary>
	/// Returns the half.
	/// </summary>
	/// <value>The half.</value>
	public static Vector2 half {
		get {
			return new Vector2(0.5f, 0.5f);
		}
	}
	
	public static string ToString(this Vector2 v, int numDecimalPlaces = 3){
		return ("("+v.x.RoundTo(numDecimalPlaces).ToString()+", "+v.y.RoundTo(numDecimalPlaces).ToString()+")");
	}
	
	/// <summary>
	/// Returns direction from a to b.
	/// </summary>
	/// <param name="a">The first component.</param>
	/// <param name="b">The second component.</param>
	public static Vector2 Direction(Vector2 a, Vector2 b){
		return b - a;
	}
	
	/// <summary>
	/// Returns normalized direction from a to b.
	/// </summary>
	/// <param name="a">The first component.</param>
	/// <param name="b">The second component.</param>
	public static Vector2 NormalizedDirection(Vector2 a, Vector2 b){
		return Direction(a, b).normalized;
	}

	/// <summary>
	/// Divide the specified left and right Vectors.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector2 Divide(float left, Vector2 right){
		return new Vector2(left / right.x, left / right.y);
	}
	
	/// <summary>
	/// Divide the specified left and right Vectors.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector2 Divide(Vector2 left, float right){
		return new Vector2(left.x / right, left.y / right);
	}
	
	/// <summary>
	/// Divide the specified left and right Vectors.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector2 Divide(Vector2 left, Vector2 right){
		return new Vector2(left.x / right.x, left.y / right.y);
	}
	
	/// <summary>
	/// Adds a float to a Vector.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector2 Add(this Vector2 left, float right){
		return new Vector2(left.x + right, left.y + right);
	}
	
	/// <summary>
	/// Subtracts a float from a Vector.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector2 Subtract(this Vector2 left, float right){
		return new Vector2(left.x - right, left.y - right);
	}
	
	/// <summary>
	/// Multiplies the components of the vectors.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector2 Scale(params Vector2[] vectors){
		Vector2 totalVector = Vector2.one;
		foreach(Vector2 vector in vectors) {
			totalVector = Vector2.Scale(vector, totalVector);
		}
		return totalVector;
	}
	
	/// <summary>
	/// Takes the reciprocal of each component in the vector.
	/// </summary>
	/// <param name="value">The vector to take the reciprocal of.</param>
	/// <returns>A vector that is the reciprocal of the input vector.</returns>
	public static Vector2 Reciprocal(this Vector2 v) {
		return Vector2X.Divide(1f, v);
	}
	
	/// <summary>
	/// Inverts the components of the Vector2.
	/// </summary>
	/// <param name="tmpRotation">Tmp rotation.</param>
	public static Vector2 Invert (Vector2 tmpRotation){
		return tmpRotation * -1;
	}
	
	/// <summary>
	/// Randomly inverts the components of the Vector3.
	/// </summary>
	/// <returns>The invert.</returns>
	/// <param name="tmpRotation">Tmp rotation.</param>
	public static Vector2 RandomInvert (Vector2 tmpRotation){
		for(int i = 0; i < 2; i++) {
			if(RandomX.boolean) tmpRotation[i] *= -1;
		}
		return tmpRotation;
	}
	
	/// <summary>
	/// Clamps the components of the Vector between 0 and 1.
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector2 Clamp01(Vector2 v){
		return Vector2X.Clamp(v, Vector2.zero, Vector2.one);
	}
	
	/// <summary>
	/// Clamps the components of the Vector between the matching components of min and max.
	/// </summary>
	/// <param name="v">V.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static Vector2 Clamp(Vector2 v, Vector2 min, Vector2 max){
		v.x = Mathf.Clamp(v.x, min.x, max.x);
		v.y = Mathf.Clamp(v.y, min.y, max.y);
		return v;
	}
	
	/// <summary>
	/// Projects a vector onto another vector.
	/// </summary>
	/// <param name="vector">vector.</param>
	/// <param name="direction">direction.</param>
	public static Vector2 Project(Vector2 vector, Vector2 direction) {
		Vector2 directionNormalized = direction.normalized;	
		float projectedDist = Vector2.Dot(vector, directionNormalized);
		return directionNormalized * projectedDist;
	}
	
	/// <summary>
	/// Gets a Vector2 with both components set to random numbers between 0 and 1. To get a normalized random direction, use RandomX.OnUnitCircle.
	/// </summary>
	/// <value>The random.</value>
	public static Vector2 random {
		get {
			return new Vector2(UnityEngine.Random.value, UnityEngine.Random.value);
		}
	}

	public static float SqrDistance (Vector2 a, Vector2 b) {
		return (a-b).sqrMagnitude;
	}

	public static Vector2 Rotate (Vector2 v, float angle) {
		angle *= Mathf.Deg2Rad;
		float sin = Mathf.Sin( angle );
		float cos = Mathf.Cos( angle );
		float tx = v.x;
		float ty = v.y;
		v.x = (cos * tx) - (sin * ty);
		v.y = (cos * ty) + (sin * tx);
		return v;
	}

	// Gets angle between a and b. Use Mathf.Rad2Deg to get degrees. 
	// 90 Degrees is added to ensure that up is 0 degrees
	public static float RadiansBetween(Vector2 a, Vector2 b) {
		return Mathf.Atan2(-(b.y - a.y), b.x - a.x) + (Mathf.Deg2Rad * 90);
	}

	// Gets angle between 0,0 and a. Use Mathf.Rad2Deg to get degrees
	public static float Radians(Vector2 a) {
		return RadiansBetween(Vector2.zero, a);
	}
	
	// Gets angle between a and b.
	public static float DegreesBetween(Vector2 a, Vector2 b) {
		return RadiansBetween(a,b) * Mathf.Rad2Deg;
	}
	
	// Gets angle between 0,0 and a.
	public static float Degrees(Vector2 a) {
		return DegreesBetween(Vector2.zero, a);
	}

		//Uses the X and Y coordinates to create a lerp
	public static float Lerp(this Vector2 v, float lerp) {
		return Mathf.Lerp(v.x, v.y, lerp);
	}

	/// <summary>
	/// Returns a random number between the components of the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static float Random( this Vector2 v ){
		return UnityEngine.Random.Range(v.x, v.y);
	}

	/// <summary>
	/// Returns the average of the components of the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static float Average( this Vector2 v ){
		return Mathf.Lerp(v.x,v.y,0.5f);
	}

	/// <summary>
	/// Returns the smallest component in the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static float Min( this Vector2 v ){
		return Mathf.Min(v.x,v.y);
	}

	/// <summary>
	/// Returns the smallest absolute component in the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static float MinAbs( this Vector2 v ){
		return Mathf.Min(Mathf.Abs(v.x), Mathf.Abs(v.y));
	}

	/// <summary>
	/// Returns the largest component in the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static float Max( this Vector2 v ){
		return Mathf.Max(v.x,v.y);
	}

	/// <summary>
	/// Returns the largest absolute component in the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static float MaxAbs( this Vector2 v ){
		return Mathf.Max(Mathf.Abs(v.x), Mathf.Abs(v.y));
	}

	/// <summary>
	/// Returns a new vector where the components of the input are absolute.
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector2 Abs( this Vector2 v ){
		return new Vector2(Mathf.Abs(v.x), Mathf.Abs(v.y));
	}


	///<summary>
	/// Returns a sign indicating the direction of forward relative to the target direction, rotating around axis up.
	/// </summary>
	/// <returns>The dir.</returns>
	/// <param name="A">A.</param>
	/// <param name="B">B.</param>
	public static float AngleDirection(Vector2 A, Vector2 B) {
		return -A.x * B.y + A.y * B.x;
	}

	/// <summary>
	/// Returns one of four cardinal directions from a vector: up (North), right (East), down (South), left (West)
	/// </summary>
	/// <param name="v">Vector to get direction from.</param>
	public static Vector2 CardinalDirection(this Vector2 v){
//		Vector2 newVector = new Vector2();
//		int index = v.AbsLargestIndex();
//		newVector[index] = v[index].Sign(false);
//		return newVector;
		return MathX.Degrees2Vector2(v.CardinalDirectionDegrees());
	}
	
	/// <summary>
	/// Returns one of four cardinal directions from a vector as an index: North(0), East (1), South (2), West (3).
	/// </summary>
	/// <param name="v">Vector to get direction from.</param>
	public static int CardinalDirectionIndex(this Vector2 v){
		return (int)(v.CardinalDirectionDegrees()/90);
	}
	
	/// <summary>
	/// Returns one of four cardinal directions from a vector as degrees.
	/// </summary>
	/// <param name="v">Vector to get direction from.</param>
	public static float CardinalDirectionDegrees(this Vector2 v){
		float angle = Vector2X.Degrees(v);
		return angle.RoundToNearestInt(90);
	}

	/// <summary>
	/// Returns one of four ordinal directions from a vector: northeast, southeast, southwest or northwest.
	/// </summary>
	/// <param name="v">Vector to get direction from.</param>
	public static Vector2 OrdinalDirection(this Vector2 v){
//		float angle = Vector2X.Degrees(v) + 45;
//		Vector2 cardinal = CardinalDirection(MathX.Degrees2Vector2(angle));
//		angle = Vector2X.Degrees(cardinal) - 45;
//		return MathX.Degrees2Vector2(angle);
		return MathX.Degrees2Vector2(v.OrdinalDirectionDegrees());
	}

	/// <summary>
	/// Returns one of four ordinal directions from a vector as an index: northeast(0), southeast (1), southwest (2), northwest (3).
	/// </summary>
	/// <param name="v">Vector to get direction from.</param>
	public static int OrdinalDirectionIndex(this Vector2 v){
		return (int)((v.OrdinalDirectionDegrees()-45)/90);
	}
	
	/// <summary>
	/// Returns one of four ordinal directions from a vector as degrees.
	/// </summary>
	/// <param name="v">Vector to get direction from.</param>
	public static float OrdinalDirectionDegrees(this Vector2 v){
		float angle = Vector2X.Degrees(v) - 45;
		return angle.RoundToNearestInt(90) + 45;
	}
	
	
	
	public static Vector2 ClampMagnitudeInDirection (Vector2 vector, Vector2 direction, float clampValue, bool outwardsOnly = false) {
		
		direction = direction.normalized;
		float actualComponentInDirection = Vector2.Dot (vector, direction);
		float desiredComponentInDirection = Mathf.Clamp (actualComponentInDirection, -clampValue, clampValue);
		float requiredOffsetInDirection = desiredComponentInDirection - actualComponentInDirection;
		
		return vector + requiredOffsetInDirection * direction;
		
		// Hey Ben! This was what I was using before. Seems to have the same result as above.
		//		Vector2 projectedVector = Vector2X.Project(vector, direction);
		//		Vector2 velocityDiff = projectedVector - vector;
		//		Vector2 clampedProjectedVelocity = Vector2.ClampMagnitude(projectedVector, clampValue);
		//		Vector2 deltaProjectedVector = clampedProjectedVelocity - projectedVector;
		//		vector += deltaProjectedVector;
		//		return vector;
	}

	
	
	
	
	
	/// <summary>
	/// Returns a normalized value between 0 and 1 for the distance on a line defined by a,b
	/// </summary>
	/// <param name="a">Start point of line.</param>
	/// <param name="b">End point of line.</param>
	/// <param name="value">Point on line to find.</param>
	public static float NormalizedDistance(Vector2 a, Vector2 b, Vector2 point) {
	   return (Vector2.Dot(point - a, b - a) / ((a-b).sqrMagnitude)).Abs();
	}

	/// <summary>
	/// Returns the index of the closest vector to v in the values list
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static int ClosestIndex(Vector2 v, IList<Vector2> values){
		int index = 0;
		float closest = Vector2X.SqrDistance(v, values[index]);
		for(int i = 1; i < values.Count; i++){
			float distance = Vector2X.SqrDistance(v, values[i]);
			if (distance < closest) {
				closest = distance;
				index = i;
			}
		}
		return index;
	}
	
	/// <summary>
	/// Returns the distance between the closest vector to v in the values list and v
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static float ClosestDistance(Vector2 v, IList<Vector2> values){
		return Vector2.Distance(v, Closest(v, values));
	}
	
	/// <summary>
	/// Returns the closest vector to v in the values list
	/// </summary>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static Vector2 Closest(Vector2 v, IList<Vector2> values){
		return values[Vector2X.ClosestIndex(v, values)];
	}
	
	/// <summary>
	/// Returns the index of the furthest vector to v in the values list
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static int FurthestIndex(IList<Vector3> valuesA, IList<Vector3> valuesB){
		int index = 0;
		float furthest = Vector3X.FurthestDistance(valuesA[index], valuesB);
		for(int i = 0; i < valuesA.Count; i++) {
			float distance = Vector3X.FurthestDistance(valuesA[i], valuesB);
			Debug.Log (distance +" "+furthest);
			if(distance > furthest) {
				furthest = distance;
				index = i;
			}
		}
		return index;
	}
	
	
	/// <summary>
	/// Returns the index of the furthest vector to v in the values list
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static int FurthestIndex(Vector2 v, IList<Vector2> values){
		if(values.Count == 0) {
			Debug.LogError("Values is empty!");
			return -1;
		}
		int index = 0;
		float furthest = Vector2X.SqrDistance(v, values[index]);
		for(int i = 1; i < values.Count; i++){
			float distance = Vector2X.SqrDistance(v, values[i]);
			if (distance > furthest) {
				furthest = distance;
				index = i;
			}
		}
		return index;
	}

	/// <summary>
	/// Returns the distance between the furthest vector to v in the values list and v
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static float FurthestDistance(Vector2 v, IList<Vector2> values){
		return Vector2.Distance(v, Furthest(v, values));
	}
	
	/// <summary>
	/// Returns the furthest vector to v in the values list
	/// </summary>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static Vector2 Furthest(Vector2 v, IList<Vector2> values){
		return values[Vector2X.FurthestIndex(v, values)];
	}
	
	
	/// <summary>
	/// Returns the index of the vector with the smallest magnitude.
	/// </summary>
	/// <returns>The magnitude index.</returns>
	/// <param name="values">Values.</param>
	public static int SmallestMagnitudeIndex(IList<Vector2> values){
		if(values.Count == 0) {
			Debug.LogError("Values is empty!");
			return -1;
		}
		int smallestIndex = 0;
		float smallestMagnitude = values[0].sqrMagnitude;
		for(int i = 1; i < values.Count; i++){
			float currentMagnitude = values[i].sqrMagnitude;
			if(currentMagnitude < smallestMagnitude) {
				smallestMagnitude = currentMagnitude;
				smallestIndex = i;
			}
		}
		return smallestIndex;
	}

	/// <summary>
	/// Returns the vector with the smallest magnitude.
	/// </summary>
	/// <returns>The magnitude index.</returns>
	/// <param name="values">Values.</param>
	public static Vector2 Smallest(this IList<Vector2> values){
		return values[Vector2X.SmallestMagnitudeIndex(values)];
	}

	/// <summary>
	/// Returns the magnitude of the vector with the smallest magnitude.
	/// </summary>
	/// <returns>The magnitude index.</returns>
	/// <param name="values">Values.</param>
	public static float SmallestMagnitude(this IList<Vector2> values){
		return values.Smallest().magnitude;
	}

	/// <summary>
	/// Returns the index of the vector with the largest magnitude.
	/// </summary>
	/// <returns>The magnitude index.</returns>
	/// <param name="values">Values.</param>
	public static int LargestIndex(IList<Vector2> values){
		if(values.Count == 0) {
			Debug.LogError("Values is empty!");
			return -1;
		}
		int largestIndex = 0;
		float largestMagnitude = values[0].sqrMagnitude;
		for(int i = 1; i < values.Count; i++){
			float currentMagnitude = values[i].sqrMagnitude;
			if(currentMagnitude > largestMagnitude) {
				largestMagnitude = currentMagnitude;
				largestIndex = i;
			}
		}
		return largestIndex;
	}

	/// <summary>
	/// Returns the vector with the largest magnitude.
	/// </summary>
	/// <returns>The magnitude index.</returns>
	/// <param name="values">Values.</param>
	public static Vector2 Largest(this IList<Vector2> values){
		return values[Vector2X.LargestIndex(values)];
	}

	/// <summary>
	/// Returns the magnitude of the vector with the largest magnitude.
	/// </summary>
	/// <returns>The magnitude index.</returns>
	/// <param name="values">Values.</param>
	public static float LargestMagnitude(this IList<Vector2> values){
		return values.Largest().magnitude;
	}



	/// <summary>
	/// Returns the index of the vector with the largest absolute component.
	/// </summary>
	/// <returns>The abs component index.</returns>
	/// <param name="values">Values.</param>
	public static int MaxAbsComponentIndex (Vector2[] values) {
		if(values.Length == 0) {
			Debug.LogError("Values is empty!");
			return -1;
		}

		int index = 0;
		float maxComponent = 0;
		float _tmpAbsComponent = 0;
		for(int i = 0; i < values.Length; i++){
			_tmpAbsComponent = MaxAbs(values[i]);
			if(_tmpAbsComponent > maxComponent) {
				maxComponent = _tmpAbsComponent;
				index = i;
			}
		}
		return index;
	}

	/// <summary>
	/// Returns the largest absolute component in the list.
	/// </summary>
	/// <returns>The abs component index.</returns>
	/// <param name="values">Values.</param>
	public static float MaxAbsComponent (Vector2[] vectors) {
		int index = MaxAbsComponentIndex(vectors);
		return vectors[index].MaxAbs();
	}


	/// <summary>
	/// Returns the average of the specified values.
	/// </summary>
	/// <param name="values">Values.</param>
	public static Vector2 Average(this IList<Vector2> values){
		Vector2 total = Vector2.zero;
		for(int i = 0; i < values.Count; i++){
			total += values[i];
		}
		return total/values.Count;
	}


	public static float[] ToMagnitudeArray(this IList<Vector2> values){
		float[] magnitudes = new float[values.Count];
		for(int i = 0; i < values.Count; i++) {
			magnitudes[i] = values[i].magnitude;
		}
		return magnitudes;
	}
	
	public static float[] ToMagnitudeArray(this IList<Vector2> values, float multiplier){
		float[] magnitudes = new float[values.Count];
		for(int i = 0; i < values.Count; i++) {
			magnitudes[i] = values[i].magnitude * multiplier;
		}
		return magnitudes;
	}
	
	public static float[] ToSqrMagnitudeArray(this IList<Vector2> values){
		float[] sqrMagnitudes = new float[values.Count];
		for(int i = 0; i < values.Count; i++) {
			sqrMagnitudes[i] = values[i].sqrMagnitude;
		}
		return sqrMagnitudes;
	}
	
	
	
	
	
	/// <summary>
	/// Creates a new vector where the x and y components are swapped.
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector2 YX(Vector2 v) {
		return new UnityEngine.Vector2(v.y, v.x);
	}
	
	/// <summary>
	/// Returns a new Vector only containing the X component of the input
	/// </summary>
	public static Vector2 X (this Vector2 v) {
		return new UnityEngine.Vector2(v.x, 0);
	}
	
	/// <summary>
	/// Returns a new Vector only containing the Y component of the input
	/// </summary>
	public static Vector2 Y (this Vector2 v) {
		return new UnityEngine.Vector2(0, v.y);
	}
	
	/// <summary>
	/// Sets the value of the X component.
	/// </summary> 
	public static Vector2 SetX (this Vector2 v, float newX) {
		return new Vector2(newX, v.y);
	}
	
	/// <summary>
	/// Sets the value of the Y component
	/// </summary> 
	public static Vector2 SetY (this Vector2 v, float newY) {
		return new Vector2(v.x, newY);
	}
	
	/// <summary>
	/// Adds a value to the X component
	/// </summary> 
	public static Vector2 AddX (this Vector2 v, float addX) {
		return new Vector2(v.x + addX, v.y);
	}
	
	/// <summary>
	/// Adds a value to the Y component
	/// </summary> 
	public static Vector2 AddY (this Vector2 v, float addY) {
		return new Vector2(v.x, v.y + addY);
	}

	public static Vector3 ToVector3XZY (this Vector2 v) {
		return new Vector3(v.x, 0, v.y);
	}


	//Convert to color array, automatically calculating the saturation and lightness, where hue is the angle. Relatively expensive.
	public static Color[] ToColorArray(this IList<Vector2> values){
		float hue, saturation, lightness;
		float maxMagnitude = values.LargestMagnitude();
		Color[] colors = new Color[values.Count];
		for(int i = 0; i < values.Count; i++) {
			if(values[i] == Vector2.zero) {
				colors[i] = Color.black;
			} else {
				float magnitude = values[i].magnitude;
				hue = Vector2X.DegreesBetween(Vector2.zero, values[i]);
				saturation = magnitude / maxMagnitude;
				lightness = saturation * 0.5f;
				Color color = new HSLColor(hue, saturation, lightness);
				colors[i] = color;
			}
		}
		return colors;
	}

	public static Color[] ToColorArray(this IList<Vector2> values, float saturation, float lightness){
		float maxMagnitude = values.LargestMagnitude();
		Color[] colors = new Color[values.Count];
		for(int i = 0; i < values.Count; i++) {
			if(values[i] == Vector2.zero) {
				colors[i] = Color.black;
			} else {
				float hue = Vector2X.DegreesBetween(Vector2.zero, values[i]);
				saturation = (Vector2.zero - values[i]).magnitude / maxMagnitude;
				lightness = ((Vector2.zero - values[i]).magnitude / maxMagnitude) * 0.5f;
				Color color = new HSLColor(hue, saturation, lightness);
				colors[i] = color;
			}
		}
		return colors;
	}
}