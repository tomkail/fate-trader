﻿using UnityEngine;
using System;
using System.Collections;
using System.Reflection;

#if UNITY_WINRT
using UnityEngine.Windows;
#endif

public static class TypeX {
	
	/// <summary>
	/// Determines if the type of an object is the specified type. Includes interfaces.
	/// </summary>
	/// <returns><c>true</c> if is type of the specified obj; otherwise, <c>false</c>.</returns>
	/// <param name="obj">Object.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool IsTypeOf<T>(this System.Object obj) {
		return obj.GetType().IsTypeOf<T>();
	}
	
	#if !UNITY_WINRT
	/// <summary>
	/// Determines if the type of an object inherits from the specified type. Includes interfaces.
	/// </summary>
	/// <returns><c>true</c> if is type of the specified obj; otherwise, <c>false</c>.</returns>
	/// <param name="obj">Object.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool InheritsFrom<T>(this System.Object obj) {
		return typeof(T).IsAssignableFrom(obj.GetType());
	}
	
	/// <summary>
	/// Determines if type is of the specified type. Includes interfaces.
	/// </summary>
	/// <returns><c>true</c> if is type of the specified type; otherwise, <c>false</c>.</returns>
	/// <param name="type">Type.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool IsTypeOf<T>(this Type type) {
		return typeof (T).IsAssignableFrom(type);
		//return GetTypeInfo().IsAssignableFrom(type.GetTypeInfo());
	}
	#endif

	#if !UNITY_WINRT
	public static bool IsAssignableToGenericType(this Type givenType, Type genericType) {
		var interfaceTypes = givenType.GetInterfaces();

		foreach (var it in interfaceTypes)
		{
			if (it.IsGenericType && it.GetGenericTypeDefinition() == genericType)
				return true;
		}
		
		if (givenType.IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
			return true;
		
		Type baseType = givenType.BaseType;
		if (baseType == null) return false;
		
		return IsAssignableToGenericType(baseType, genericType);
	}
	#endif
}