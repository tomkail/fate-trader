﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class BoundsX {
	
	/// <summary>
	/// Creates new bounds that encapsulates a list of vectors.
	/// </summary>
	/// <param name="vectors">Vectors.</param>
	public static Bounds CreateEncapsulating (IList<Vector3> vectors) {
		Bounds bounds = new Bounds(vectors[0], Vector3.zero);
		for(int i = 1; i < vectors.Count; i++)
			bounds.Encapsulate(vectors[i]);
		return bounds;
	}
	
	public static Bounds Lerp (Bounds bounds1, Bounds bounds2, float lerp) {
		return new Bounds(Vector3.Lerp(bounds1.center, bounds2.center, lerp), Vector3.Lerp(bounds1.size, bounds2.size, lerp));
	}
	
	public static Bounds Add(this Bounds left, Bounds right){
		return new Bounds(left.center + right.center, left.size + right.size);
	}

	public static Bounds Subtract(this Bounds left, Bounds right){
		return new Bounds(left.center - right.center, left.size - right.size);
	}

	public static Bounds GetCombinedBounds(IList<Bounds> bounds) {
		Bounds totalBounds = new Bounds();
		if(bounds.Count == 0) return totalBounds;
		Vector3 min = bounds[0].min;
		Vector3 max = bounds[0].max;
		for(int i = 0; i < bounds.Count; i++) {
			min = Vector3.Min(min, bounds[i].min);
			max = Vector3.Max(max, bounds[i].max);
		}
		totalBounds.SetMinMax(min, max);
		return totalBounds;
	}

	public static Bounds Combine(this IList<Bounds> bounds) {
		return BoundsX.GetCombinedBounds(bounds);
	}
	
	/// <summary>
	/// Clamps a point inside the rect.
	/// </summary>
	/// <param name="r">The red component.</param>
	/// <param name="point">Point.</param>
	public static Vector3 Clamp(this Bounds b, Vector3 point) {
		point.x = Mathf.Clamp(point.x, b.min.x, b.max.x);
		point.y = Mathf.Clamp(point.y, b.min.y, b.max.y);
		point.z = Mathf.Clamp(point.z, b.min.z, b.max.z);
		return point;
	}

	public static bool ContainsAny(this Bounds bounds, IList<Vector3> points) {
		foreach(Vector3 point in points) {
			if(bounds.Contains(point)) return true;
		}
		return false;
	}
	
	public static Vector3[] GetVertices(this Bounds bounds) {
		Vector3[] vertices = new Vector3[8];
		vertices[0] = bounds.min;
		vertices[1] = bounds.max;
		vertices[2] = new Vector3(vertices[0].x, vertices[0].y, vertices[1].z);
		vertices[3] = new Vector3(vertices[0].x, vertices[1].y, vertices[0].z);
		vertices[4] = new Vector3(vertices[1].x, vertices[0].y, vertices[0].z);
		vertices[5] = new Vector3(vertices[0].x, vertices[1].y, vertices[1].z);
		vertices[6] = new Vector3(vertices[1].x, vertices[0].y, vertices[1].z);
		vertices[7] = new Vector3(vertices[1].x, vertices[1].y, vertices[0].z);
		return vertices;
	}
}
