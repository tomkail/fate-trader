using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GizmosX {
	
	public static void DrawLine (Vector3 start, Vector3 end, float thickness, Vector3 upwards) {
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.LookRotation(Vector3X.Direction(start, end), upwards), Vector3.one);
		Gizmos.DrawWireCube(Vector3.Lerp(start, end, 0.5f).YZX (), new Vector3(thickness, 0, Vector3.Distance(start, end)));
		Gizmos.matrix = temp;
	}
	
	public static void DrawSpheres (IList<Vector3> centers, float radius) {
		for(int i = 0; i < centers.Count; i++) {
			Gizmos.DrawSphere(centers[i], radius);
		}
	}
	
	public static void DrawWirePolygon (Vector3 position, Quaternion rotation, IList<Vector2> points) {
		for(int i = 0; i < points.Count; i++) {
			Gizmos.DrawLine(position + rotation * points.GetRepeating(i), position + rotation * points.GetRepeating(i+1));
		}
	}
	
	public static void DrawWirePolygon (IList<Vector2> points) {
		for(int i = 0; i < points.Count; i++) {
			Gizmos.DrawLine(points.GetRepeating(i), points.GetRepeating(i+1));
		}
	}
	
	public static void DrawWirePolygon (IList<Vector3> points) {
		for(int i = 0; i < points.Count; i++) {
			Gizmos.DrawLine(points.GetRepeating(i), points.GetRepeating(i+1));
		}
	}
	
	public static void DrawWireCircle (Vector3 position, Quaternion rotation, float radius, int numLines = 32) {
		for(int i = 0; i < numLines; i++) {
			Vector3 offsetVectorA = MathX.Degrees2Vector2(MathX.Degrees(i, numLines)) * radius;
			Vector3 offsetVectorB = MathX.Degrees2Vector2(MathX.Degrees(i+1, numLines)) * radius;
			Vector3 a = position + rotation * offsetVectorA;
			Vector3 b = position + rotation * offsetVectorB;
			Gizmos.DrawLine(a, b);
		}
	}
	
	public static void DrawWireCube (Bounds _bounds) {
		Gizmos.DrawWireCube(_bounds.center, _bounds.size);
	}
	
	public static void DrawCube (Bounds _bounds) {
		Gizmos.DrawCube(_bounds.center, _bounds.size);
	}
	
	public static void DrawWireCube (Transform transform) {
		DrawWireCube(transform.position, transform.rotation, transform.lossyScale);
	}

	public static void DrawCube (Transform transform) {
		DrawCube(transform.position, transform.rotation, transform.lossyScale);
	}

	public static void DrawWireCube (Vector3 position, Quaternion rotation, Vector3 scale) {
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(position, rotation, scale);
		Gizmos.DrawWireCube(new Vector3(0,0,0), Vector3.one);
		Gizmos.matrix = temp;
	}

	public static void DrawCube (Vector3 position, Quaternion rotation, Vector3 scale) {
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(position, rotation, scale);
		Gizmos.DrawCube(new Vector3(0,0,0), Vector3.one);
		Gizmos.matrix = temp;
	}
	
	public static void DrawWireCube (Rect _rect, float _distance) {
		Gizmos.DrawWireCube(new Vector3(_rect.x + _rect.width * 0.5f, _rect.y + _rect.height * 0.5f, _distance), new Vector3(_rect.width, _rect.height, 0));
	}
	
	public static void DrawCube (Rect _rect, float _distance) {
		Gizmos.DrawCube(new Vector3(_rect.x + _rect.width * 0.5f, _rect.y + _rect.height * 0.5f, _distance), new Vector3(_rect.width, _rect.height, 0));
	}
	
	public static void DrawWireRect (Rect _rect) {
		DrawWireRect(_rect.TopLeft(), _rect.TopRight(), _rect.BottomLeft(), _rect.BottomRight());
	}
	
	public static void DrawWireRect (Vector3 origin, Quaternion rotation, Vector2 scale) {
		scale *= 0.5f;
		Vector3 topLeft = origin + rotation * new Vector3(-scale.x, scale.y, 0);
		Vector3 topRight = origin + rotation * new Vector3(scale.x, scale.y, 0);
		Vector3 bottomLeft = origin + rotation * new Vector3(-scale.x, -scale.y, 0);
		Vector3 bottomRight = origin + rotation * new Vector3(scale.x, -scale.y, 0);
		DrawWireRect(topLeft, topRight, bottomLeft, bottomRight);
	}
	
	public static void DrawWireRect (Vector3 topLeft, Vector3 topRight, Vector3 bottomLeft, Vector3 bottomRight) {
		Gizmos.DrawLine(topLeft, topRight);
		Gizmos.DrawLine(topRight, bottomRight);
		Gizmos.DrawLine(bottomRight, bottomLeft);
		Gizmos.DrawLine(bottomLeft, topLeft);
	}
	
	public static void DrawWireTube (Vector3 start, Vector3 end, float radius, int numLines = 12) {
		Gizmos.DrawLine(start, end);
		Quaternion rot = Quaternion.LookRotation(start - end);
		for(int i = 0; i < numLines; i++) {
			Vector3 offsetVector = MathX.Degrees2Vector2(MathX.Degrees(i, numLines)) * radius;
			Gizmos.DrawLine(start + (rot * offsetVector), end + (rot * offsetVector));
		}
	}
	
	public static void DrawArrow (Vector3 position, Quaternion rotation, float arrowSize) {
		Vector3 start = position + (rotation * Vector3.back * arrowSize);
		Vector3 end = position + (rotation * Vector3.forward * arrowSize);
		
		GizmosX.DrawWireTube(start+(rotation * Vector3.left * arrowSize), end, arrowSize * 0.1f, 6);
		GizmosX.DrawWireTube(start+(rotation * Vector3.right * arrowSize), end, arrowSize * 0.1f, 6);
	}
	
	public static void DrawFrustum (Vector3 position, Quaternion rotation, float fieldOfView, float aspectRatio, float nearClipPlane, float farClipPlane) {
		if(!QuaternionX.IsValid(rotation)) return;
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(position, rotation, Vector3.one);
		Gizmos.DrawFrustum(Vector3.zero, fieldOfView, farClipPlane, nearClipPlane, aspectRatio);
		Gizmos.matrix = temp;
	}
	
	public static void DrawOrthographicFrustum (Vector3 position, Quaternion rotation, float orthographicSize, float aspectRatio, float nearClipPlane, float farClipPlane) {
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(position, rotation, Vector3.one);
		float spread = farClipPlane - nearClipPlane;
		float center = (farClipPlane + nearClipPlane)*0.5f;
		Gizmos.DrawWireCube(new Vector3(0,0,center), new Vector3(orthographicSize*2*aspectRatio, orthographicSize*2, spread));
		Gizmos.matrix = temp;
	}
//	
//	public static void DrawFrustum (Camera cam) {
//		Vector3[] nearCorners = new Vector3[4]; //Approx'd nearplane corners
//		Vector3[] farCorners = new Vector3[4]; //Approx'd farplane corners
//		Plane[] camPlanes = GeometryUtility.CalculateFrustumPlanes( cam ); //get planes from matrix
//		Plane temp = camPlanes[1]; camPlanes[1] = camPlanes[2]; camPlanes[2] = temp; //swap [1] and [2] so the order is better for the loop
//		
//		for ( int i = 0; i < 4; i++ ) {
//			nearCorners[i] = Plane3Intersect( camPlanes[4], camPlanes[i], camPlanes[( i + 1 ) % 4] ); //near corners on the created projection matrix
//			farCorners[i] = Plane3Intersect( camPlanes[5], camPlanes[i], camPlanes[( i + 1 ) % 4] ); //far corners on the created projection matrix
//		}
//		
//		for ( int i = 0; i < 4; i++ ) {
//			Debug.DrawLine( nearCorners[i], nearCorners[( i + 1 ) % 4], Color.red, Time.deltaTime, true ); //near corners on the created projection matrix
//			Debug.DrawLine( farCorners[i], farCorners[( i + 1 ) % 4], Color.blue, Time.deltaTime, true ); //far corners on the created projection matrix
//			Debug.DrawLine( nearCorners[i], farCorners[i], Color.green, Time.deltaTime, true ); //sides of the created projection matrix
//		}
//	}
//	
//	private Vector3 Plane3Intersect ( Plane p1, Plane p2, Plane p3 ) { //get the intersection point of 3 planes
//		return ( ( -p1.distance * Vector3.Cross( p2.normal, p3.normal ) ) +
//		        ( -p2.distance * Vector3.Cross( p3.normal, p1.normal ) ) +
//		        ( -p3.distance * Vector3.Cross( p1.normal, p2.normal ) ) ) /
//			( Vector3.Dot( p1.normal, Vector3.Cross( p2.normal, p3.normal ) ) );
//	}
}
