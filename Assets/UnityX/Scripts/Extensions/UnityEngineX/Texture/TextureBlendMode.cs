﻿public enum TextureBlendMode {
	Normal,
	Additive,
	Multiply,
	Screen,
	Overlay,
	Darken,
	Lighten,
	Difference,
	Hue,
	Saturation,
	Color,
	Luminosity,
	Overwrite,
	Ignore
}