﻿using UnityEngine;
using System.Collections;

public static class Vector4X {

	/// <summary>
	/// The number of components in the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static int NumComponents(this Vector4 v) {
		return 4;
	}
	
	public static Quaternion ToQuaternion(this Vector4 vec) {
		return new Quaternion(vec.x, vec.y, vec.z, vec.w);
	}
}
