﻿using UnityEngine;
using System.Collections;

public class AudioSourceX : MonoBehaviour {
	
	//Credit to http://answers.unity3d.com/questions/157940/getoutputdata-and-getspectrumdata-they-represent-t.html
	public static float GetRMS (AudioSource audioSource, int qSamples = 1024) {
		// increase qSamples to get better average, but will decrease performance. Best to leave it
		float[] samples = new float[qSamples];
		audioSource.GetOutputData(samples, 0); // fill array with samples
		float sum = 0;
		for (int i=0; i < qSamples; i++){
			sum += samples[i]*samples[i]; // sum squared samples
		}
		return Mathf.Sqrt(sum/qSamples); // rms = square root of average
	}
	
	public static float GetDB (AudioSource audioSource, int qSamples = 1024, float rmsRefValue = 0.1f) {
		float dbValue = 20 * Mathf.Log10(GetRMS(audioSource, qSamples)/rmsRefValue); // calculate dB
		if (dbValue < -160) dbValue = -160; // clamp it to -160dB min
		return dbValue;
	}
	
	public static float GetPitch (AudioSource audioSource) {
		int qSamples = 1024;
		float[] spectrum = new float[qSamples];
		
		float fSample = AudioSettings.outputSampleRate; // Sample rate
		float threshold = 0.02f;      // minimum amplitude to extract pitch
		
		audioSource.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
		float maxV = 0;
		float maxN = 0;
		for (int i=0; i < qSamples; i++){ // find max 
			if (spectrum[i] > maxV && spectrum[i] > threshold){
				maxV = spectrum[i];
				maxN = i; // maxN is the index of max
			}
		}
		float freqN = maxN; // pass the index to a float variable
		if (maxN > 0 && maxN < qSamples-1){ // interpolate index using neighbours
			float dL = spectrum[Mathf.RoundToInt(maxN)-1]/spectrum[Mathf.RoundToInt(maxN)];
			float dR = spectrum[Mathf.RoundToInt(maxN)+1]/spectrum[Mathf.RoundToInt(maxN)];
			freqN += 0.5f*(dR*dR - dL*dL);
		}
		return freqN*(fSample/2)/qSamples; // convert index to frequency
	}
}
