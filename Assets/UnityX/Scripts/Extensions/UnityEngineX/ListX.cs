using UnityEngine;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
 
public static class ListX {

	public static List<T> ToList<T>(this IList<T> IList) {
		return new List<T>(IList);
	}

	/// <summary>
	/// Get an item from the specified list by turning a float of range 0-1 into an index, based on the length of the List.
	/// Where 0 is the start of the List and 1 is the end of the List
	/// </summary>
	/// <param name="list">List.</param>
	/// <param name="f">F.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Get<T>(this IList<T> list, float f) {
		return list[Mathf.Clamp(Mathf.CeilToInt(((float)list.Count) * f) - 1, 0, list.Count-1)];
	}
	
	/// <summary>
	/// First object in the specified list.
	/// </summary>
	/// <param name="list">List.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T First<T>(this IList<T> list) {
		return list[0];
	}
	
	/// <summary>
	/// Last object in the specified list.
	/// </summary>
	/// <param name="list">List.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Last<T>(this IList<T> list) {
		return list[list.Count - 1];
	}
	
	/// <summary>
	/// 
	/// </summary>
	/// <param name="list"></param>
	/// <typeparam name="T"></typeparam>
	public static bool IsEmpty<T>(this IEnumerable<T> list) {
		return list.Count() == 0;
	}
	
	/// <summary>
	/// Determines if the specified list is null or empty.
	/// </summary>
	/// <returns><c>true</c> if the specified list is null or empty; otherwise, <c>false</c>.</returns>
	/// <param name="list">List.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool IsNullOrEmpty<T>(this IEnumerable<T> list) {
		return (list == null || list.IsEmpty());
	}
	
	/// <summary>
	/// Shortcut to RandomX.Weighted
	/// </summary>
	/// <returns>The random.</returns>
	/// <param name="values">Values.</param>
	/// <param name="weights">Weights.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T RandomWeighted<T>(this IList<T> values, IList<float> weights){
		return RandomX.Weighted(values, weights);
	}
	
	/// <summary>
	/// Returns true of a list contains a type
	/// </summary>
	/// <param name="list">List.</param>
	/// <param name="type">Type.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool Contains<T>(this IList<T> list, System.Type type) {
		for(int i = list.Count - 1; i >= 0; i--) {
			if(list[i].GetType() == type) return true;
		}
		return false;
	}
	
	/// <summary>
	/// Gets the list element looping with an index that repeats around the length of the array.
	/// </summary>
	/// <returns>The looping.</returns>
	/// <param name="list">List.</param>
	/// <param name="index">Index.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
    public static T GetRepeating<T>(this IList<T> list, int index) {
		return list[list.GetLoopingIndex(index)];
    }
    
	/// <summary>
	/// Gets the list element clamp with an index that repeats around the length of the array.
	/// </summary>
	/// <returns>The looping.</returns>
	/// <param name="list">List.</param>
	/// <param name="index">Index.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T GetClamped<T>(this IList<T> list, int index) {
		return list[list.GetClampedIndex(index)];
	}
	
	/// <summary>
	/// Loops an index around the length of the array if it exceeds the length of the array.
	/// </summary>
	/// <returns>The looping.</returns>
	/// <param name="list">List.</param>
	/// <param name="index">Index.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static int GetLoopingIndex<T>(this IList<T> list, int index) {
		return Mathf.Repeat(index, list.Count).RoundToInt();
	}
	
	/// <summary>
	/// Clamps an index around the length of the array if it exceeds the length of the array.
	/// </summary>
	/// <returns>The looping.</returns>
	/// <param name="list">List.</param>
	/// <param name="index">Index.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static int GetClampedIndex<T>(this IList<T> list, int index) {
		return Mathf.Clamp(index, 0, list.Count-1);
	}
	
    /// <summary>
	/// Determines if the specified array index is valid.
    /// </summary>
	/// <returns><c>true</c> if the specified array index is valid; otherwise, <c>false</c>.</returns>
    /// <param name="array">Array.</param>
    /// <param name="index">Index.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool ContainsIndex<T>(this IEnumerable<T> list, int index) {
		if(list.IsNullOrEmpty()) return false;
		return MathX.InRange(index, 0, list.Count()-1);
    }
	
	/// <summary>
	/// Checks to see if a list contains a particular item.
	/// </summary>
	/// <param name="list">List.</param>
	/// <param name="item">Item.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
    public static bool Contains<T>(this IList<T> list, T item) {
		if(list.IsNullOrEmpty()) return false;
       for(int i = list.Count - 1; i >= 0; i--) {
            if(list[i].Equals(item)) return true;
        }
        return false;
    }
    
    /// <summary>
    /// Finds the index of a particular item. Returns -1 if the item could not be found.
    /// </summary>
    /// <returns>The index.</returns>
    /// <param name="list">List.</param>
    /// <param name="item">Item.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static int IndexOf<T>(this IList<T> list, T item) {
        for(int i = list.Count - 1; i >= 0; i--) {
			if(list[i] == null) continue;
            if(list[i].Equals(item)) return i;
        }
        return -1;
    }

	/// <summary>
	/// Finds the index between list, item, start and end.
	/// </summary>
	/// <returns>The <see cref="System.Int32"/>.</returns>
	/// <param name="list">List.</param>
	/// <param name="item">Item.</param>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
    public static int IndexOfBetween<T>(this IList<T> list, T item, int start, int end) {
        for(int i = end; i >= start; i--) {
            if(list[i].Equals(item)) return i;
        }
        return -1;
    }
    
    /// <summary>
    /// Returns the specified item in the list, or returns default(T).
    /// </summary>
    /// <param name="list">List.</param>
    /// <param name="item">Item.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static T Find<T>(this IList<T> list, T item) {
        int index = list.IndexOf(item);
        if(index < 0) return default(T);
        else return list[index];
    }
    
	/// <summary>
	/// G
	/// </summary>
	/// <returns>The all of type.</returns>
	/// <param name="list">List.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	/// <typeparam name="Q">The 2nd type parameter.</typeparam>
	public static List<T> GetAllOfType<T>(this List<T> list, System.Type removeType) {
		List<T> newList = new List<T>();
		for (int i = list.Count-1; i >= 0; i--) {
			if(list[i].GetType() == removeType) newList.Add(list[i]);
		}
		return newList;
	}

	
    /// <summary>
    /// G
    /// </summary>
    /// <returns>The all of type.</returns>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    /// <typeparam name="Q">The 2nd type parameter.</typeparam>
	#if !UNITY_WINRT
	public static List<T> GetAllInheritingType<T>(this IList<T> list, System.Type baseType) {
//		return list.Where(t => baseType.IsAssignableFrom(t.GetType())) as List<T>;
		List<T> typeList = new List<T>();
		for (int i = list.Count-1; i >= 0; i--) {
			if(baseType.IsAssignableFrom(list[i].GetType())) typeList.Add(list[i]);
		}
		return typeList;
	}

	public static T GetFirstInheritingType<T>(this List<T> list, System.Type assignableType) {
		for (int i = 0; i < list.Count; i++) {
			if(assignableType.IsAssignableFrom(list[i].GetType())) return list[i];
		}
		return default(T);
	}
	
	public static T GetLastInheritingType<T>(this List<T> list, System.Type assignableType) {
		for (int i = list.Count-1; i >= 0; i--) {
			if(assignableType.IsAssignableFrom(list[i].GetType())) return list[i];
		}
		return default(T);
	}
	#endif
	
	
//	public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable {
//		return listToClone.Select(item => (T)item.Clone()).ToList();
//	}

	//Not tested	
	public static List<T> RemoveDuplicates<T>(this List<T> list) {
        return list.Distinct().ToList();
    }
    

	public static List<T> RemoveNull<T>(this List<T> list) {
        for (int i = list.Count-1; i >= 0; i--) {
			// Equals check is a workaround for weird Unity Object behaviour. http://forum.unity3d.com/threads/fun-with-null.148090/
			if(list[i] == null || list[i].Equals(null)) {
            	list.RemoveAt(i);
            }
        }
		return list;
    }
    
    /// <summary>
    /// Gets the index of the first empty slot in the array. Returns -1 if none exist.
    /// Takes Unity null object behaviour into account.
    /// </summary>
    /// <returns>The first empty index.</returns>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
	public static int GetFirstEmptyIndex<T>(this List<T> list) {
		for (int i = 0; i < list.Count; i++)
			if(list[i] == null || list[i].Equals(null))
				return i;
		return -1;
	}
    
   
	public static List<T> NonDistinct<T> (this List<T> list) {
		List<T> nonDistinct = new List<T>();
		var distinct = new Dictionary<T, int>();
		foreach (T item in list) {
			if (!distinct.ContainsKey(item))
				distinct.Add(item, 1);
			else {
				nonDistinct.Add (item);
			}                    
		}
		return nonDistinct;
	}

	/// <summary>
	/// Swaps the elements at the two given indices within the list
	/// </summary>
	/// <param name="list">The list we want to swap the elements within.</param>
	/// <param name="indexA">The index of the first element we wish to swap.</param>
	/// <param name="indexB">The index of the second element we wish to swap.</param>
	/// <typeparam name="T">The type of the elements within the list.</typeparam>
	public static void Swap<T>(this IList<T> list, int indexA, int indexB) {
		T tmp = list[indexA];
		list[indexA] = list[indexB];
		list[indexB] = tmp;
	}
	
	/// <summary>
	/// RemovesAt, returning the removed value.
	/// </summary>
	/// <returns>The value.</returns>
	/// <param name="theList">The list.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T RemoveAtReturn<T>(this List<T> theList, int i) {
		T local = theList[i];
		theList.RemoveAt(i);
		return local;
	}
	
	public static T RemoveFirstOfType<T>(this List<T> list, System.Type removeType) {
		for (int i = list.Count-1; i >= 0; i--) {
			if(list[i].GetType() == removeType) {
				T removed = list[i];
				list.RemoveAt(i);
				return removed;
			}
		}
		return default(T);
	}
	
    //Not tested
	public static void RemoveAllOfType<T>(this List<T> list, System.Type removeType) {
        for (int i = list.Count-1; i >= 0; i--) {
			if(list[i].GetType() == removeType) list.RemoveAt(i);
        }
    }

    //Use like...
	//var list = new List<string> { "x", "y" };
	//list.AddMany("a", "z");
    public static void AddMany<T>(this List<T> list, params T[] elements) {
        for (int i = 0; i < elements.Length; i++) {
            list.Add(elements[i]);
        }
    }
    
    /// <summary>
    /// Shuffles the specified list.
    /// </summary>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static void Shuffle<T>(this IList<T> list) {  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = UnityEngine.Random.Range (0, n + 1);  
			T value = list [k];  
			list [k] = list [n];  
			list [n] = value;  
		}  
	}
	
	/// <summary>
	/// Returns a reversed version of the list
	/// </summary>
	/// <param name="list">List.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static List<T> Reversed<T>(this IList<T> list) {  
		return list.AsEnumerable().Reverse().ToList();
	}
	
	public static IList<T> GetOffset<T>(this IList<T> list, int offset) {
		IList<T> newList = new T[list.Count];
		for(int i = 0; i < list.Count; i++) {
			newList[i] = list.GetRepeating(i + offset);
		}
		return newList as IList<T>;
	}
	
	public static bool IsEquivalent<T>(this IList<T> list1, IList<T> list2) {  
		return list1.SequenceEqual(list2);
	}

	public static List<string> ToStringList<T> (this IList<T> list) {
		return list.Select(i => i.ToString()).ToList();
	}

	public static string[] ToStringArray<T> (this IList<T> list) {
		return list.Select(i => i.ToString()).ToArray();
	}
}