﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ForceEventArgs {
	public Rigidbody2D body;
	public Vector2 force;
	public Vector2? position;
	public ForceMode2D forceMode;
	
	public ForceEventArgs (Rigidbody2D _body, Vector2 _force, Vector2? _position = null, ForceMode2D _forceMode = ForceMode2D.Force) {
		body = _body;
		force = _force;
		position = _position;
		forceMode = _forceMode;
	}
	
	public override string ToString () {
		return string.Format ("[ForceEventArgs] Body: "+body+", Force: "+force+", Position: "+position+", ForceMode: "+forceMode);
	}
}

public static class Rigidbody2DX {
	
	/// <summary>
	/// Applies a force to the rigidbody that simulates explosion effects. The explosion force will fall off linearly with distance to the rigidbody.
	///	
	/// TODO - If explosionRadius is 0, the full force will be applied no matter how far away position is from the rigidbody
	/// </summary>
	/// <param name="explosionForce">Explosion force.</param>
	/// <param name="explosionPosition">Explosion position.</param>
	/// <param name="explosionRadius">Explosion radius.</param>
	/// <param name="mode">Mode.</param>
	public static void AddExplosionForce (float explosionForce, Vector2 explosionPosition, float explosionRadius, ForceMode2D mode = ForceMode2D.Force) {
		Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPosition, explosionRadius);
		foreach (Collider2D hit in colliders) {
			Vector2 hitPos = hit.transform.position.XY();
			Vector2 forceDirection = Vector2.zero;
			if(hitPos == explosionPosition) 
				forceDirection = RandomX.onUnitCircle;
			else
				forceDirection = (hitPos - explosionPosition).normalized;

			if (hit && hit.GetComponent<Rigidbody2D>()) {
				float forceDistance = Vector2.Distance(explosionPosition, hitPos);
				float distPenalty = Mathf.Pow((explosionRadius - forceDistance) / explosionRadius, 2);
				Vector2 force = forceDirection * explosionForce * distPenalty;
				
				//	hit.rigidbody2D.AddForce(force, mode);
				hit.GetComponent<Rigidbody2D>().AddForceAtPosition(force, hitPos, mode);
//				hit.gameObject.SendMessage("OnAddForce", new AddForceEventArgs(force, interceptHit.point, mode));
				//	hit.rigidbody2D.AddForceAtPosition(explosionPos, forceDirection * power, ForceMode2D.Impulse);
			}
		}
	}
	
	/// <summary>
	/// Applies a force to the rigidbody that simulates explosion effects. The explosion force will fall off linearly with distance to the rigidbody.
	/// If the line between the explosion position and the rigidbody is obstructed, a force will not be applied.
	/// </summary>
	/// <param name="explosionForce">Explosion force.</param>
	/// <param name="explosionPosition">Explosion position.</param>
	/// <param name="explosionRadius">Explosion radius.</param>
	/// <param name="mode">Mode.</param>
	public static void AddObstructableExplosionForce (float explosionForce, Vector2 explosionPosition, float explosionRadius, ForceMode2D mode = ForceMode2D.Force, LayerMask obstructMask = new LayerMask()) {
		Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPosition, explosionRadius);
		foreach (Collider2D hit in colliders) {
			Vector2 hitPos = hit.transform.position.XY();
			Vector2 forceDirection = Vector2.zero;
			if(hitPos == explosionPosition) 
				forceDirection = RandomX.onUnitCircle;
			else
				forceDirection = (hitPos - explosionPosition).normalized;
			
			if (hit && hit.GetComponent<Rigidbody2D>()) {
				RaycastHit2D interceptHit = Physics2D.Raycast(explosionPosition, forceDirection, explosionRadius/*, obstructMask*/); 
				if(!interceptHit) continue;
				if (hit.gameObject != interceptHit.transform.gameObject) continue;
				
				float forceDistance = Vector2.Distance(explosionPosition, interceptHit.point);
				float distPenalty = Mathf.Pow((explosionRadius - forceDistance) / explosionRadius, 2);
				Vector2 force = forceDirection * explosionForce * distPenalty;
				
				//	hit.rigidbody2D.AddForce(force, mode);
				hit.GetComponent<Rigidbody2D>().AddForceAtPosition(force, interceptHit.point, mode);
				//				hit.gameObject.SendMessage("OnAddForce", new AddForceEventArgs(force, interceptHit.point, mode));
				//	hit.rigidbody2D.AddForceAtPosition(explosionPos, forceDirection * power, ForceMode2D.Impulse);
			}
		}
	}
	
	
	public static List<ForceEventArgs> GetExplosionForces (float explosionForce, Vector2 explosionPosition, float explosionRadius, ForceMode2D mode = ForceMode2D.Force) {
		List<ForceEventArgs> args = new List<ForceEventArgs>();
		Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPosition, explosionRadius);
		foreach (Collider2D hit in colliders) {
			Vector2 hitPos = hit.transform.position.XY();
			Vector2 forceDirection = Vector2.zero;
			if(hitPos == explosionPosition) 
				forceDirection = RandomX.onUnitCircle;
			else
				forceDirection = (hitPos - explosionPosition).normalized;
			
			if (hit && hit.GetComponent<Rigidbody2D>()) {
				float forceDistance = Vector2.Distance(explosionPosition, hitPos);
				float distPenalty = Mathf.Pow((explosionRadius - forceDistance) / explosionRadius, 2);
				Vector2 force = forceDirection * explosionForce * distPenalty;
				args.Add(new ForceEventArgs(hit.GetComponent<Rigidbody2D>(), force, null, ForceMode2D.Impulse));
			}
		}
		return args;
	}
	
	public static List<ForceEventArgs> GetObstructableExplosionForces (float explosionForce, Vector2 explosionPosition, float explosionRadius, ForceMode2D mode = ForceMode2D.Force, LayerMask obstructMask = new LayerMask()) {
		List<ForceEventArgs> args = new List<ForceEventArgs>();
		Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPosition, explosionRadius);
		foreach (Collider2D hit in colliders) {
			Vector2 hitPos = hit.transform.position.XY();
			Vector2 forceDirection = Vector2.zero;
			if(hitPos == explosionPosition) 
				forceDirection = RandomX.onUnitCircle;
			else
				forceDirection = (hitPos - explosionPosition).normalized;
			
			if (hit && hit.GetComponent<Rigidbody2D>()) {
				RaycastHit2D[] interceptHits = Physics2D.RaycastAll(explosionPosition, forceDirection, explosionRadius); 
				if(interceptHits.IsNullOrEmpty()) continue;
				
				RaycastHit2D interceptHit = GetObstructedHit(interceptHits ,hit.transform.gameObject, obstructMask);
				
				if (hit.gameObject != interceptHit.transform.gameObject) continue;
				
				float forceDistance = Vector2.Distance(explosionPosition, interceptHit.point);
				float distPenalty = Mathf.Pow((explosionRadius - forceDistance) / explosionRadius, 2);
				Vector2 force = forceDirection * explosionForce * distPenalty;
				args.Add(new ForceEventArgs(hit.GetComponent<Rigidbody2D>(), force, null, ForceMode2D.Impulse));
			}
		}
		return args;
	}
	
	private static RaycastHit2D GetObstructedHit (RaycastHit2D[] interceptHits, GameObject target, LayerMask obstructMask = new LayerMask()) {
		foreach (RaycastHit2D tmpInterceptHit in interceptHits) {
			if(obstructMask.Includes(tmpInterceptHit.transform.gameObject.layer) || tmpInterceptHit.transform.gameObject == target) {
				return tmpInterceptHit;
			}
		}
		return default(RaycastHit2D);
	}
	
}
