﻿using UnityEngine;
using System.Collections;

public static class OnGUIX {

	public const float windowHeaderHeight = 14;
	public const float scrollBarSize = 15;

	public static Vector2 ScreenToGUIScreen (Vector2 vector) {
		return new Vector2(vector.x, Screen.height - vector.y);
	}
	
	public static Rect ScreenToGUIScreen (Rect rect) {
		return new Rect(rect.x, Screen.height - rect.y - rect.height, rect.width, rect.height);
	}
	
	public static Rect ViewportToGUIScreen (Rect rect) {
		return (new Rect(rect.x * Screen.width, rect.y * Screen.height, rect.width * Screen.width, rect.height * Screen.height));
	}




	public static Texture2D lineTex;
 
    public static void DrawLine(Rect rect) { DrawLine(rect, GUI.contentColor, 1.0f); }
    public static void DrawLine(Rect rect, Color color) { DrawLine(rect, color, 1.0f); }
    public static void DrawLine(Rect rect, float width) { DrawLine(rect, GUI.contentColor, width); }
    public static void DrawLine(Rect rect, Color color, float width) { DrawLine(new Vector2(rect.x, rect.y), new Vector2(rect.x + rect.width, rect.y + rect.height), color, width); }
    public static void DrawLine(Vector2 pointA, Vector2 pointB) { DrawLine(pointA, pointB, GUI.contentColor, 1.0f); }
    public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color) { DrawLine(pointA, pointB, color, 1.0f); }
    public static void DrawLine(Vector2 pointA, Vector2 pointB, float width) { DrawLine(pointA, pointB, GUI.contentColor, width); }
    public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color, float width)
    {
        // Save the current GUI matrix, since we're going to make changes to it.
        Matrix4x4 matrix = GUI.matrix;
 
        // Generate a single pixel texture if it doesn't exist
        if (!lineTex) { lineTex = new Texture2D(1, 1); }
 
        // Store current GUI color, so we can switch it back later,
        // and set the GUI color to the color parameter
        Color savedColor = GUI.color;
        GUI.color = color;

        /*
        // Determine the angle of the line.
        float angle = Vector3.Angle(pointB - pointA, Vector2.right);
 
        // Vector3.Angle always returns a positive number.
        // If pointB is above pointA, then angle needs to be negative.
        if (pointA.y > pointB.y) { angle = -angle; }
 
        // Use ScaleAroundPivot to adjust the size of the line.
        // We could do this when we draw the texture, but by scaling it here we can use
        //  non-integer values for the width and length (such as sub 1 pixel widths).
        // Note that the pivot point is at +.5 from pointA.y, this is so that the width of the line
        //  is centered on the origin at pointA.
        GUIUtility.ScaleAroundPivot(new Vector2((pointB - pointA).magnitude, width), new Vector2(pointA.x, pointA.y + 0.5f));
 
        // Set the rotation for the line.
        //  The angle was calculated with pointA as the origin.
        GUIUtility.RotateAroundPivot(angle, pointA);
        */
		Vector2 offset = new Vector2(0, -20);
	    offset += new Vector2(0, 0.5f); // Compensate for line width
		Quaternion guiRot = Quaternion.FromToRotation(Vector2.right, pointB-pointA);
		Matrix4x4 guiRotMat = Matrix4x4.TRS(pointA, guiRot, new Vector3((pointB-pointA).magnitude, width, 1));
		Matrix4x4 guiTransMat = Matrix4x4.TRS(offset, Quaternion.identity, Vector3.one);
		Matrix4x4 guiTransMatInv = Matrix4x4.TRS(-offset, Quaternion.identity, Vector3.one);
	 
	    GUI.matrix = guiTransMatInv * guiRotMat * guiTransMat;

        // Finally, draw the actual line.
        // We're really only drawing a 1x1 texture from pointA.
        // The matrix operations done with ScaleAroundPivot and RotateAroundPivot will make this
        //  render with the proper width, length, and angle.
        GUI.DrawTexture(new Rect(0, 0, 1, 1), lineTex);   // We're done.  Restore the GUI matrix and GUI color to whatever they were before.
        GUI.matrix = matrix;
        GUI.color = savedColor;
    }

	public static void DrawProgressBar (float value, float maxValue) {
		Rect rect = GUILayoutUtility.GetLastRect();
		rect.y += rect.height + 4;
		DrawProgressBar(rect, value, maxValue);
		GUILayout.Box("");
	}

	public static void DrawProgressBar (float value, float hoverValue, float maxValue) {
		Rect rect = GUILayoutUtility.GetLastRect();
		rect.y += rect.height + 4;
		DrawProgressBar(rect, value, hoverValue, maxValue);
		GUILayout.Box("");
	}

	public static void DrawProgressBar (Rect rect, float value, float maxValue) {
		value = Mathf.Clamp(value, 0, maxValue);

		GUI.Box(rect, "");
         
		float normalizedValue = value/maxValue;
		GUI.Box(new Rect(rect.x, rect.y, rect.width * normalizedValue, rect.height), "");
	}

	public static void DrawProgressBar (Rect rect, float value, float hoverValue, float maxValue) {
		value = Mathf.Clamp(value, 0, maxValue);
		hoverValue = Mathf.Clamp(hoverValue, 0, maxValue);

		GUI.Box(rect, "");
         
		float normalizedValue = value/maxValue;
		GUI.Box(new Rect(rect.x, rect.y, rect.width * normalizedValue, rect.height), "");

		float normalizedHoverValue = hoverValue/maxValue;
		GUI.Box(new Rect(rect.x, rect.y, rect.width * normalizedHoverValue, rect.height), "");
	}

	/*
	public delegate void Callback();
    public static void Window(Rect rect, string title, Callback windowFunction) {
        GUILayout.Window(GUIUtility.GetControlID(FocusType.Passive), rect, (int id) => {
            GUI.depth = 0;

            // first get a control id. every subsequent call to GUI control function will get a larger id
            int min = GUIUtility.GetControlID(FocusType.Native);

            // we can use the id to check if current control is inside our window
            if (GUIUtility.hotControl < min)
				SetHotControl(0);

            // draw the window
            windowFunction();

            int max = GUIUtility.GetControlID(FocusType.Native);

            if (GUIUtility.hotControl < min || (GUIUtility.hotControl > max && max != -1))
				SetHotControl(-1);

            GUI.FocusWindow(id);
            GUI.BringWindowToFront(id);

        }, title);
    }
 
    private static void SetHotControl(int id) {
        if (new Rect(0, 0, Screen.width, Screen.height).Contains(GUIUtility.GUIToScreenPoint(Event.current.mousePosition)))
            GUIUtility.hotControl = id;
    }
    */

	/*
	public static void DrawProgressBar (Rect rect, float value, float maxValue, Texture2D emptyTexture, Texture2D fullTexture) {
		float normalizedValue = value/maxValue;
		GUILayout.BeginArea(rect);
		GUILayout.Box(new Rect(0, 0, rect.width, rect.height), emptyTexture);
         
		//draw the filled-in part:
		GUILayout.BeginArea(new Rect(rect.x, rect.y, rect.width * normalizedValue, rect.height));
		GUILayout.Box(new Rect(0, 0, rect.width, rect.height), fullTexture);
		GUILayout.EndArea();
		GUILayout.EndArea();
	}
	*/
	/*
	// The texture used by DrawLine(Color)
	private static Texture2D _coloredLineTexture;
	
	// The color used by DrawLine(Color)
	private static Color _coloredLineColor;
	
	/// <summary>
	/// Draw a line between two points with the specified color and a thickness of 1
	/// </summary>
	/// <param name="lineStart">The start of the line</param>
	/// <param name="lineEnd">The end of the line</param>
	/// <param name="color">The color of the line</param>
	public static void DrawLine(Vector2 lineStart, Vector2 lineEnd, Color color)
	{
		DrawLine(lineStart, lineEnd, color, 1);
	}
	
	/// <summary>
	/// Draw a line between two points with the specified color and thickness
	/// Inspired by code posted by Sylvan
	/// http://forum.unity3d.com/threads/17066-How-to-draw-a-GUI-2D-quot-line-quot?p=407005&viewfull=1#post407005
	/// </summary>
	/// <param name="lineStart">The start of the line</param>
	/// <param name="lineEnd">The end of the line</param>
	/// <param name="color">The color of the line</param>
	/// <param name="thickness">The thickness of the line</param>
	public static void DrawLine(Vector2 lineStart, Vector2 lineEnd, Color color, int thickness)
	{
		if (_coloredLineTexture == null || _coloredLineColor != color)
		{
			_coloredLineColor = color;
			_coloredLineTexture = new Texture2D(1, 1);
			_coloredLineTexture.SetPixel(0, 0, _coloredLineColor);
			_coloredLineTexture.wrapMode = TextureWrapMode.Repeat;
			_coloredLineTexture.Apply();
		}
		DrawLineStretched(lineStart, lineEnd, _coloredLineTexture, thickness);
	}
	
	/// <summary>
	/// Draw a line between two points with the specified texture and thickness.
	/// The texture will be stretched to fill the drawing rectangle.
	/// Inspired by code posted by Sylvan
	/// http://forum.unity3d.com/threads/17066-How-to-draw-a-GUI-2D-quot-line-quot?p=407005&viewfull=1#post407005
	/// </summary>
	/// <param name="lineStart">The start of the line</param>
	/// <param name="lineEnd">The end of the line</param>
	/// <param name="texture">The texture of the line</param>
	/// <param name="thickness">The thickness of the line</param>
	public static void DrawLineStretched(Vector2 lineStart, Vector2 lineEnd, Texture2D texture, int thickness)
	{
		Vector2 lineVector = lineEnd - lineStart;
		float angle = Mathf.Rad2Deg * Mathf.Atan(lineVector.y / lineVector.x);
		if (lineVector.x < 0)
		{
			angle += 180;
		}
		
		if (thickness < 1)
		{
			thickness = 1;
		}
		
		// The center of the line will always be at the center
		// regardless of the thickness.
		int thicknessOffset = (int)Mathf.Ceil(thickness / 2);
		
		GUIUtility.RotateAroundPivot(angle,
		                             lineStart);
		GUI.DrawTexture(new Rect(lineStart.x,
		                         lineStart.y - thicknessOffset,
		                         lineVector.magnitude,
		                         thickness),
		                texture);
		GUIUtility.RotateAroundPivot(-angle, lineStart);
	}
	
	/// <summary>
	/// Draw a line between two points with the specified texture and a thickness of 1
	/// The texture will be repeated to fill the drawing rectangle.
	/// </summary>
	/// <param name="lineStart">The start of the line</param>
	/// <param name="lineEnd">The end of the line</param>
	/// <param name="texture">The texture of the line</param>
	public static void DrawLine(Vector2 lineStart, Vector2 lineEnd, Texture2D texture)
	{
		DrawLine(lineStart, lineEnd, texture, 1);
	}
	
	/// <summary>
	/// Draw a line between two points with the specified texture and thickness.
	/// The texture will be repeated to fill the drawing rectangle.
	/// Inspired by code posted by Sylvan and ArenMook
	/// http://forum.unity3d.com/threads/17066-How-to-draw-a-GUI-2D-quot-line-quot?p=407005&viewfull=1#post407005
	/// http://forum.unity3d.com/threads/28247-Tile-texture-on-a-GUI?p=416986&viewfull=1#post416986
	/// </summary>
	/// <param name="lineStart">The start of the line</param>
	/// <param name="lineEnd">The end of the line</param>
	/// <param name="texture">The texture of the line</param>
	/// <param name="thickness">The thickness of the line</param>
	public static void DrawLine(Vector2 lineStart, Vector2 lineEnd, Texture2D texture, int thickness)
	{
		Vector2 lineVector = lineEnd - lineStart;
		float angle = Mathf.Rad2Deg * Mathf.Atan(lineVector.y / lineVector.x);
		if (lineVector.x < 0)
		{
			angle += 180;
		}
		
		if (thickness < 1)
		{
			thickness = 1;
		}
		
		// The center of the line will always be at the center
		// regardless of the thickness.
		int thicknessOffset = (int)Mathf.Ceil(thickness / 2);
		
		Rect drawingRect = new Rect(lineStart.x,
		                            lineStart.y - thicknessOffset,
		                            Vector2.Distance(lineStart, lineEnd),
		                            (float) thickness);
		GUIUtility.RotateAroundPivot(angle,
		                             lineStart);
		GUI.BeginGroup(drawingRect);
		{
			int drawingRectWidth = Mathf.RoundToInt(drawingRect.width);
			int drawingRectHeight = Mathf.RoundToInt(drawingRect.height);
			
			for (int y = 0; y < drawingRectHeight; y += texture.height)
			{
				for (int x = 0; x < drawingRectWidth; x += texture.width)
				{
					GUI.DrawTexture(new Rect(x,
					                         y,
					                         texture.width,
					                         texture.height),
					                texture);
				}
			}
		}
		GUI.EndGroup();
		GUIUtility.RotateAroundPivot(-angle, lineStart);
	}
	*/
}
