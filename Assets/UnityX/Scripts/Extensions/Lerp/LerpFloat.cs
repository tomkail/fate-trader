﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LerpFloat : TypeLerp<float> {
	
	public LerpFloat (float origin) : base (origin) {
		
	}

	public override void Update(float deltaTime){
		if(current != target){
			current = Mathf.Lerp(current, target, lerpSpeed*deltaTime);

			if(Mathf.Abs(current - target) < 0.001f){
				current = target;
			}
		}
	}
}