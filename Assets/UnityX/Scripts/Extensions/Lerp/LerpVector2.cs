using UnityEngine;
using System.Collections;

[System.Serializable]
public class LerpVector2 : TypeLerp<Vector2> {

	public LerpVector2 (Vector2 origin) : base (origin){
		
	}

	public LerpVector2 (float x, float y) {
		current = target = new Vector2(x,y);
	}

	public override void Update (float deltaTime){
		if(current != target){
			current.x = Mathf.Lerp(current.x, target.x, lerpSpeed*deltaTime);
			current.y = Mathf.Lerp(current.y, target.y, lerpSpeed*deltaTime);

			if(Mathf.Abs(current.x - target.x) < 0.001f){
				current.x = target.x;
			}

			if(Mathf.Abs(current.y - target.y) < 0.001f){
				current.y = target.y;
			}
		}
		
	}
}