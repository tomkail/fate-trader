using UnityEngine;
using System.Collections;

[System.Serializable]
public class LerpColor : TypeLerp<Color> {

	public LerpColor(Color origin) : base (origin){}
	
	public override void Update(float deltaTime){
		current = Color.Lerp(current, target, lerpSpeed*deltaTime);
	}
}