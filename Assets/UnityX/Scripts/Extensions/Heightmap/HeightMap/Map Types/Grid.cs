﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityX.Geometry;

/// <summary>
/// Grid class.
/// Normalized space is in the range (0, 1) on the X, Y and Z axis.
/// Grid space is in the range (0, size), on the X and Y axis.
/// </summary>
[System.Serializable]
public class Grid {
	
	/// <summary>
	/// The size of the grid.
	/// </summary>
	public Point size;
	
	/// <summary>
	/// Gets the number of cells in the grid.
	/// </summary>
	/// <value>The length.</value>
	public int cellCount {
		get {
			return size.area;
		}
	}
	
	/// <summary>
	/// The size of the grid, minus one.
	/// Many functions calculate starting from zero rather than one, where this value is used instead of size.
	/// </summary>
	public Point sizeMinusOne {
		get {
			return new Point(size.x-1, size.y-1);
		}
	}
	
	/// <summary>
	/// The reciprocal of the size of the grid. 
	/// An optimization, used as a multiplier instead of division by the grid size by various functions.
	/// </summary>
	public Vector2 sizeReciprocal {
		get {
			return new Vector2(1f/size.x, 1f/size.y);
		}
	}
	
	/// <summary>
	/// The reciprocal of the size of the grid minus one.
	/// An optimization, used as a multiplier instead of division by the grid size minus one by various functions.
	/// </summary>
	public Vector2 sizeMinusOneReciprocal {
		get {
			return new Vector2(1f/sizeMinusOne.x, 1f/sizeMinusOne.y);
		}
	}

	public delegate void OnResizeEvent(Vector2 lastSize, Vector2 newSize);
	public event OnResizeEvent OnResize;

	
	public Grid (Point _size) {
		SetSize(_size);
	}
	
	public virtual void SetSize(Point _size) {
		size = _size;
	}
	
	public int GridPointToArrayIndex (int x, int y){
		return Grid.GridPointToArrayIndex(x, y, size.x);
	}
	
	public int GridPointToArrayIndex (Point gridPoint){
		return GridPointToArrayIndex(gridPoint.x, gridPoint.y);
	}
	
	public int PositionToArrayIndex (Vector2 position){
		return GridPointToArrayIndex(NormalizedPositionToGridPoint(position));
	}
	
	public int PositionToArrayIndex (Vector3 position){
		return GridPointToArrayIndex(NormalizedPositionToGridPoint(position));
	}
	
	public Point ArrayIndexToGridPoint (int arrayIndex){
		return new Point(arrayIndex%size.x, MathX.FloorToInt((float)arrayIndex * sizeReciprocal.x));
	}

	public Point ArrayIndexToNormalizedPosition (int arrayIndex){
		Point gridPoint = ArrayIndexToGridPoint(arrayIndex);
		return GridPositionToNormalizedPosition(gridPoint);
	}
	
	public bool IsOnGrid(Point gridPoint){
		return IsOnGrid(gridPoint.x, gridPoint.y);
	}
	
	public bool IsOnGrid(int x, int y){
		return (x >= 0 && x < size.x && y >= 0 && y < size.y);
	}

	public bool IsOnGrid(int index){
		return (index >= 0 && index < cellCount);
	}
	
	
	// RANDOM LOCATION
	
	public Point RandomGridPoint () {
		return Grid.RandomGridPoint(size);
	}
	
	public Vector2 RandomGridPosition () {
		return Grid.RandomGridPosition(size);
	}
	
	public Point GetRandomEdgeGridPoint () {
		Point gridPoint = new Point(0,0);
		
		int r = UnityEngine.Random.Range(0,4);
		
		if(r == 0) {
			gridPoint.x = UnityEngine.Random.Range(0, size.x);
		} else if(r == 1) {
			gridPoint.x = UnityEngine.Random.Range(0, size.x);
			gridPoint.y = size.y - 1;
		} else if(r == 2) {
			gridPoint.y = UnityEngine.Random.Range(0, size.y);
		} else if(r == 3) {
			gridPoint.y = UnityEngine.Random.Range(0, size.y);
			gridPoint.x = size.x - 1;
		}
		
		return gridPoint;
	}	
	
	//Conversion Functions
	
	public Vector2 GridPositionToNormalizedPosition (Vector2 gridPosition){
		return Grid.GridPositionToNormalizedPosition(gridPosition, size);
	}
	
	public Vector2 NormalizedPositionToGridPosition (Vector3 normalizedPosition){
		return Grid.NormalizedPositionToGridPosition(normalizedPosition, size);
	}
	
	public Vector2 NormalizedPositionToGridPosition (Vector2 normalizedPosition){
		return Grid.NormalizedPositionToGridPosition(normalizedPosition, size);
	}
	
	public Point NormalizedPositionToGridPoint (Vector3 normalizedPosition) {
		return Grid.NormalizedPositionToGridPoint(normalizedPosition, size);
	}
	
	public Point NormalizedPositionToGridPoint(Vector2 normalizedPosition) {
		return Grid.NormalizedPositionToGridPoint(normalizedPosition, size);
	}
	
	
	//Clamping Functions

	public Vector3 ClampNormalizedPosition3D(Vector3 normalizedPosition){
		return ClampNormalizedPosition3D(normalizedPosition, 0, 1);
	}
	
	public Vector3 ClampNormalizedPosition3D(Vector3 normalizedPosition, float min, float max){
		float x = Mathf.Clamp(normalizedPosition.x, min, max);
		float y = Mathf.Clamp(normalizedPosition.y, min, max);
		float z = Mathf.Clamp(normalizedPosition.z, min, max);
		if(normalizedPosition.x == x && normalizedPosition.y == y && normalizedPosition.z == z) return normalizedPosition;
		return new Vector3(x, y, z);
	}
	
	public Vector3 ClampNormalizedPosition(Vector2 normalizedPosition){
		return ClampNormalizedPosition(normalizedPosition, 0, 1);
	}
	
	public Vector2 ClampNormalizedPosition(Vector2 normalizedPosition, float min, float max){
		float x = Mathf.Clamp(normalizedPosition.x, min, max);
		float y = Mathf.Clamp(normalizedPosition.y, min, max);
		if(normalizedPosition.x == x && normalizedPosition.y == y) return normalizedPosition;
		return new Vector2(x, y);
	}
	
	public Vector2 ClampGridPosition(Vector2 gridPosition){
		return ClampGridPosition(gridPosition, 0, sizeMinusOne.x);
	}
	
	public Vector2 ClampGridPosition(Vector2 gridPosition, float min, float max){
		float x = Mathf.Clamp(gridPosition.x, min, max);
		float y = Mathf.Clamp(gridPosition.y, min, max);
		if(gridPosition.x == x && gridPosition.y == y) return gridPosition;
		return new Vector2(x, y);
	}
	
	public void ClampGridPoint(ref int x, ref int y){
		ClampGridPoint(ref x, ref y, 0, sizeMinusOne.x, 0, sizeMinusOne.y);
	}
	
	public void ClampGridPoint(ref int x, ref int y, int minX, int maxX, int minY, int maxY){
		x = Mathf.Clamp(x, minX, maxX);
		y = Mathf.Clamp(y, minY, maxY);
	}
	
	public Point ClampGridPoint(Point gridPoint){
		return ClampGridPoint(gridPoint, 0, sizeMinusOne.x, 0, sizeMinusOne.y);
	}
	
	public Point ClampGridPoint(Point gridPoint, int minX, int maxX, int minY, int maxY){
		int x = Mathf.Clamp(gridPoint.x, minX, maxX);
		int y = Mathf.Clamp(gridPoint.y, minY, maxY);
		if(gridPoint.x == x && gridPoint.y == y) return gridPoint;
		return new Point(x, y);
	}
	
	
	public Vector2 RepeatNormalizedPosition(Vector2 normalizedPosition){
		return RepeatNormalizedPosition(normalizedPosition, 0, 1, 0, 1);
	}
	
	public Vector2 RepeatNormalizedPosition(Vector2 normalizedPosition, float minX, float maxX, float minY, float maxY){
		float x = MathX.Repeat(normalizedPosition.x, minX, maxX);
		float y = MathX.Repeat(normalizedPosition.y, minY, maxY);
		if(normalizedPosition.x == x && normalizedPosition.y == y) return normalizedPosition;
		return new Vector2(x, y);
	}
	
	public Vector2 RepeatGridPosition(Vector2 gridPosition){
		return RepeatGridPosition(gridPosition, 0, sizeMinusOne.x, 0, sizeMinusOne.y);
	}
	
	public Vector2 RepeatGridPosition(Vector2 gridPosition, float minX, float maxX, float minY, float maxY){
		float x = MathX.Repeat(gridPosition.x, minX, maxX);
		float y = MathX.Repeat(gridPosition.y, minY, maxY);
		if(gridPosition.x == x && gridPosition.y == y) return gridPosition;
		return new Vector2(x, y);
	}
	
	public Point RepeatGridPoint(Point gridPoint){
		return RepeatGridPoint(gridPoint, 0, sizeMinusOne.x, 0, sizeMinusOne.y);
	}
	
	public Point RepeatGridPoint(Point gridPoint, int minX, int maxX, int minY, int maxY){
		int x = MathX.Repeat(gridPoint.x, minX, maxX);
		int y = MathX.Repeat(gridPoint.y, minY, maxY);
		if(gridPoint.x == x && gridPoint.y == y) return gridPoint;
		return new Point(x, y);
	}
	



	public Point[] ValidCardinalDirections(Point gridPoint){
		return Grid.Filter(Grid.Filter(Grid.CardinalDirections(gridPoint).ToList(), IsOnGrid)).ToArray();
	}

	public Point[] ValidOrdinalDirections(Point gridPoint){
		return Grid.Filter(Grid.Filter(Grid.OrdinalDirections(gridPoint).ToList(), IsOnGrid)).ToArray();
	}

	public Point[] ValidCompassDirections(Point gridPoint){
		return Grid.Filter(Grid.Filter(Grid.CompassDirections(gridPoint).ToList(), IsOnGrid)).ToArray();
	}

	public Point[] ValidPointsOnRing(Point gridPoint, int ringDistance = 1){
		return Grid.Filter(Grid.Filter(Grid.PointsOnRing(gridPoint, ringDistance).ToList(), IsOnGrid)).ToArray();
	}
	
	
	
	
	// MAP FUNCTIONS
	public IList<Point> GetAllGridPoints () {
		Point[] gridPoints = new Point[size.area];
		for(int y = 0; y < size.y; y++)
			for(int x = 0; x < size.x; x++)
				gridPoints[GridPointToArrayIndex(x,y)] = new Point(x,y);
		return gridPoints;
	}
	
	/// <summary>
	/// Determines whether the point is on the edge of the grid;
	/// </summary>
	/// <returns><c>true</c> if this point is on the edge of the grid; otherwise, <c>false</c>.</returns>
	/// <param name="_point">_point.</param>
	public bool IsEdge (Point _point) {
		if(_point.x == 0 || _point.x == size.x-1 || _point.y == 0 || _point.y == size.y-1) return true;
		return false;
	}
	
	/// <summary>
	/// Determines whether the point is on a corner of the grid.
	/// </summary>
	/// <returns><c>true</c> if this point is on a corner of the grid; otherwise, <c>false</c>.</returns>
	/// <param name="_point">_point.</param>
	public bool IsCorner (Point _point) {
		if(_point.x == 0 && _point.y == 0) return true;
		if(_point.x == size.x-1 && _point.y == 0) return true;
		if(_point.x == 0 && _point.y == size.y-1) return true;
		if(_point.x == size.x-1 && _point.y == size.y-1) return true;
		return false;
	}
	
	/// <summary>
	/// Converts a grid point to an index of the array.
	/// </summary>
	/// <returns>The point to array index.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="width">Width.</param>
	public static int GridPointToArrayIndex (int x, int y, int width){
		return y * width + x;
	}
	
	/// <summary>
	/// Converts a grid point to an index of the array.
	/// </summary>
	/// <returns>The point to array index.</returns>
	/// <param name="gridPoint">Grid point.</param>
	/// <param name="width">Width.</param>
	public static int GridPointToArrayIndex (Point gridPoint, int width){
		return GridPointToArrayIndex(gridPoint.x, gridPoint.y, width);
	}
	
	public static Point ArrayIndexToGridPoint (int arrayIndex, int width){
		return new Point(arrayIndex%width, MathX.FloorToInt((float)arrayIndex/width));
	}

	public static Vector2 GridPositionToNormalizedPosition (Vector2 gridPosition, Point gridSize){
		return new Vector2(gridPosition.x / (gridSize.x - 1), gridPosition.y / (gridSize.y - 1));
	}
	
	public static Vector2 NormalizedPositionToGridPosition (Vector3 normalizedPosition, Point gridSize){
		return new Vector2(normalizedPosition.x * (gridSize.x-1), normalizedPosition.z * (gridSize.y-1));
	}
	
	public static Vector2 NormalizedPositionToGridPosition (Vector2 normalizedPosition, Point gridSize){
		return new Vector2(normalizedPosition.x * (gridSize.x-1), normalizedPosition.y * (gridSize.y-1));
	}
	
	public static Point NormalizedPositionToGridPoint (Vector3 normalizedPosition, Point gridSize){
		return new Point(MathX.RoundToInt(normalizedPosition.x * (gridSize.x - 1)) , MathX.RoundToInt(normalizedPosition.z * (gridSize.y - 1)) );
	}
	
	public static Point NormalizedPositionToGridPoint(Vector2 normalizedPosition, Point gridSize){
		return new Point(MathX.RoundToInt(normalizedPosition.x * (gridSize.x - 1)) , MathX.RoundToInt(normalizedPosition.y * (gridSize.y - 1)) );
	}
	
	public static Point RandomGridPoint (Point gridSize) {
		return new Point (UnityEngine.Random.Range(0, gridSize.x), UnityEngine.Random.Range(0, gridSize.y));
	}
	
	public static Vector2 RandomGridPosition (Point gridSize) {
		return new Vector2 (UnityEngine.Random.Range(0f, gridSize.x), UnityEngine.Random.Range(0f, gridSize.y));
	}
	
	public static Vector2 RandomNormalizedPosition () {
		return new Vector2 (UnityEngine.Random.Range(0, 1), UnityEngine.Random.Range(0, 1));
	}
	
	
	public static Point[] CardinalDirections(){
		Point[] points = new Point[4];
		points[0] = new Point(0, 1);
		points[1] = new Point(1, 0);
		points[2] = new Point(0, -1);
		points[3] = new Point(-1, 0);
		return points;
	}
	
	
	public static Point[] CardinalDirections(Point gridPoint){
		Point[] points = CardinalDirections();
		for(int i = 0; i < points.Length; i++) points[i] += gridPoint;
		return points;
	}
	
	public static Point[] OrdinalDirections(){
		Point[] points = new Point[4];
		points[0] = new Point(1, 1);
		points[1] = new Point(1, -1);
		points[2] = new Point(-1, -1);
		points[3] = new Point(-1, 1);
		return points;
	}
	
	public static Point[] OrdinalDirections(Point gridPoint){
		Point[] points = OrdinalDirections();
		for(int i = 0; i < points.Length; i++) points[i] += gridPoint;
		return points;
	}

	public static Point[] CompassDirections(Point gridPoint){
		Point[] points = new Point[8];
		CardinalDirections(gridPoint).CopyTo(points, 0);
		OrdinalDirections(gridPoint).CopyTo(points, 4);
		return points;
	}

	public static Point[] PointsOnRing(Point gridPoint, int ringDistance = 1){
		//Point[] points = new Point[ringDistance + 1 + ringDistance == 0?0 : ringDistance - 1];
		List<Point> points = new List<Point>();
		for (int i = 0; i < ringDistance + 1; i++) {
			points.Add(new Point(gridPoint.x - ringDistance + i, gridPoint.y - i));
			points.Add(new Point(gridPoint.x + ringDistance - i, gridPoint.y + i));
		}

		for (int i = 1; i < ringDistance; i++) {
			points.Add(new Point(gridPoint.x - i, gridPoint.y + ringDistance - i));
			points.Add(new Point(gridPoint.x + ringDistance - i, gridPoint.y - i));
		}

		return points.ToArray();
	}

	/// <summary>
	/// Removes the invalid points in the list as defined by function parameters.
	/// Example usage: List<Point> validAdjacent = Grid.Filter(GetAdjacentPoints(new Point(0,3), IsOnGrid);
	/// </summary>
	/// <returns>The invalid.</returns>
	/// <param name="allPoints">All points.</param>
	public static List<Point> Filter(IList<Point> allPoints, params Func<Point, bool>[] filters){
		List<Point> validPoints = new List<Point>();
		foreach(Point gridPoint in allPoints) {
			bool valid = true;
			foreach(Func<Point, bool> filterFunction in filters) {
				if(!filterFunction(gridPoint)) {
					valid = false;
					break;
				}
			}
			if(valid) validPoints.Add(gridPoint);
		}
		return validPoints;
	}


	public virtual void Resize (Point size) {
		Point lastSize = this.size;
		this.size = size;
		RaiseResizeEvent(lastSize, size);
	}

	protected virtual void RaiseResizeEvent (Point lastSize, Point size) {
		if(OnResize != null)
			OnResize(lastSize, size);
	}
}