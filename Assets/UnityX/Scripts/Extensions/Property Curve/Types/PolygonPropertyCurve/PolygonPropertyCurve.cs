﻿using UnityEngine;
using System.Collections;
using UnityX.Geometry;
using UnityX.Geometry.Polygon;

[System.Serializable]
public class PolygonPropertyCurve : PropertyCurve<Polygon> {
	
	public PolygonPropertyCurve(params PropertyCurveKeyframe<Polygon>[] keys) : base (keys) {}
	
	protected override Polygon GetSmoothedValue(Polygon key1, Polygon key2, float time) {
		return Polygon.Lerp(key1, key2, time);
	}
}
