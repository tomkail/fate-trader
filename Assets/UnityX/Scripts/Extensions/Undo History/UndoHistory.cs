﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UndoHistory<T> where T : class {
	
	private int _undoHistoryIndex;
	public int undoHistoryIndex {
		get {
			return _undoHistoryIndex;
		} set {
			_undoHistoryIndex = Mathf.Clamp(value, 0, undoHistory.Count-1);
			if(OnChangeHistoryIndex != null) OnChangeHistoryIndex(undoHistory[undoHistoryIndex]);
		}
	}

	public List<T> undoHistory;
	public int maxHistoryItems = 100;

	public bool canUndo {
		get {
			return !undoHistory.IsEmpty() && undoHistoryIndex > 0;
		}
	}
	
	public bool canRedo {
		get {
			return !undoHistory.IsEmpty() && undoHistoryIndex < undoHistory.Count - 1;
		}
	}

	public delegate void OnUndoEvent(T historyItem);
	public event OnUndoEvent OnUndo;

	public delegate void OnRedoEvent(T historyItem);
	public event OnRedoEvent OnRedo;

	public delegate void OnChangeHistoryIndexEvent(T historyItem);
	public event OnChangeHistoryIndexEvent OnChangeHistoryIndex;

	public delegate void OnChangeUndoHistoryEvent();
	public event OnChangeUndoHistoryEvent OnChangeUndoHistory;

	public UndoHistory () {
		undoHistory = new List<T>();
		_undoHistoryIndex = -1;
	}

	public UndoHistory (int maxHistoryItems) : this () {
		this.maxHistoryItems = Mathf.Clamp(maxHistoryItems, 1, int.MaxValue);
	}
	
	public virtual void AddToUndoHistory (T state) {
		if(undoHistory.Count > 0 && undoHistory.Count - (undoHistoryIndex + 1) > 0) {
			undoHistory.RemoveRange(undoHistoryIndex + 1, undoHistory.Count - (undoHistoryIndex + 1));
		}
		
		if(undoHistory.Count >= maxHistoryItems) {
			undoHistory.RemoveAt (0);
			_undoHistoryIndex--;
		}

		undoHistory.Add (state);
		_undoHistoryIndex++;

		if(OnChangeUndoHistory != null) OnChangeUndoHistory();
	}

	public virtual void Clear () {
		undoHistory.Clear();
		_undoHistoryIndex = -1;
		if(OnChangeUndoHistory != null) OnChangeUndoHistory();
	}
	
	public virtual T Undo () {
		if(!canUndo) {
			if(undoHistory.IsEmpty())
				return default(T);
		} else {
			undoHistoryIndex--;
			if(OnUndo != null) OnUndo(undoHistory[undoHistoryIndex]);
		}
		return undoHistory[undoHistoryIndex];
	}
	
	public virtual T Redo () {
		if(!canRedo) {
			if(undoHistory.IsEmpty())
				return default(T);
		} else {
			undoHistoryIndex++;
			if(OnRedo != null) OnRedo(undoHistory[undoHistoryIndex]);
		}
		return undoHistory[undoHistoryIndex];
	}

	protected virtual void ApplyHistoryItem (T historyItem) {}
}