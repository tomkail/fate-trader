﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Utility class that let you see normals and tangent vectors for a mesh.
/// This is really useful when debugging mesh appearance
/// </summary>
[RequireComponent (typeof (MeshFilter))]
public class MeshDisplay : MonoBehaviour {
	public bool showNormals = true;
	public bool showTangents = true;
	public float displayLengthScale = 1.0f;

	public Color normalColor = Color.red;
	public Color tangentColor = Color.blue;
	
	void Start () {}
	
	void OnDrawGizmosSelected() {
		MeshFilter meshFilter = GetComponent<MeshFilter>();
		if (meshFilter==null){
			Debug.LogWarning("Cannot find MeshFilter");
			return;
		}
		Mesh mesh = meshFilter.sharedMesh;
		if (mesh == null){
			return;
		}
		
		int[] tris = mesh.triangles;
		Vector3[] vertices = mesh.vertices;
		Vector3[] normals = mesh.normals;
		Vector4[] tangents = mesh.tangents;
		
		bool doShowNormals = showNormals && normals.Length == vertices.Length;
		bool doShowTangents = showTangents && tangents.Length == vertices.Length;
		
		foreach (int idx in tris){
			Vector3 vertex = transform.TransformPoint(vertices[idx]);

			if (doShowNormals){
				Vector3 normal = transform.TransformDirection(normals[idx]);
				Gizmos.color = normalColor;
				Gizmos.DrawLine(vertex, vertex+normal*displayLengthScale);
			}
			if (doShowTangents){
				Vector3 tangent = transform.TransformDirection(tangents[idx]);
				Gizmos.color = tangentColor;
				Gizmos.DrawLine(vertex, vertex+tangent*displayLengthScale);
			}
		}
    }
}