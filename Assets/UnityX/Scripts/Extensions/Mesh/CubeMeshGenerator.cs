﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (MeshFilter))]
[RequireComponent (typeof (MeshRenderer))]
[RequireComponent (typeof (MeshCollider))]

[System.Serializable]
public class CubeMeshGenerator : MonoBehaviour {
	Mesh mesh;
	List<Vector3> verts;
	List<int> tris;
	List<Vector2> uvs;

	public bool invertTop = false;
	public bool invertBottom = false;
	public bool invertFront = false;
	public bool invertBack = false;
	public bool invertLeft = false;
	public bool invertRight = false;

	public virtual Mesh CreateCubeMesh(float radius = 0.5f) {
		return CreateCubeMesh(new Vector3(-radius, radius, radius), new Vector3(radius, radius, radius), new Vector3(-radius, -radius, radius), new Vector3(radius, -radius, radius), new Vector3(-radius, radius, -radius), new Vector3(radius, radius, -radius), new Vector3(-radius, -radius, -radius), new Vector3(radius, -radius, -radius));
	}

	// public virtual Mesh CreateCubeMesh() {
	// 	return CreateCubeMesh(new Vector3(-0.5f, 0.5f, 0.5f),new Vector3(0.5f, 0.5f, 0.5f), new Vector3(-0.5f, -0.5f, 0.5f), new Vector3(0.5f, -0.5f, 0.5f), new Vector3(-0.5f, 0.5f, -0.5f), new Vector3(0.5f, 0.5f, -0.5f), new Vector3(-0.5f, -0.5f, -0.5f), new Vector3(0.5f, -0.5f, -0.5f));
	// }

	[ContextMenu ("Create Cube")]
	public virtual Mesh CreateCubeMesh(Vector3 leftTopFront, Vector3 rightTopFront, Vector3 leftTopBack, Vector3 rightTopBack, Vector3 leftBottomFront, Vector3 rightBottomFront, Vector3 leftBottomBack, Vector3 rightBottomBack){
		mesh = new Mesh();
		mesh.name = "Custom Cube Mesh";

		verts = new List<Vector3>();
		tris = new List<int>();
		uvs = new List<Vector2>();

		/*
		public Vector3[] verts = new Vector3[24];
		public int[] tris = new int[24];
		public Vector2[] uvs = new Vector2[24];
		*/

		Vector2 uv0 = new Vector2(0,0);
		Vector2 uv1 = new Vector2(1,0);
		Vector2 uv2 = new Vector2(0,1);
		Vector2 uv3 = new Vector2(1,1);

		int vertexIndex = 0;
		
		vertexIndex = verts.Count;
		verts.Add(leftTopBack);
		verts.Add(leftTopFront);
		verts.Add(rightTopFront);
		verts.Add(rightTopBack);
		if(invertTop){
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex);
		} else {
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex);
		}
		uvs.AddRange(new List<Vector2>(){
			uv0,uv1,uv2,uv3
		});

		
		vertexIndex = verts.Count;
		verts.Add(leftBottomFront);
		verts.Add(rightBottomFront);
		verts.Add(rightTopFront);
		verts.Add(leftTopFront);
		if(invertFront){
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex);
		} else {
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex);
		}

		uvs.AddRange(new List<Vector2>(){
			uv0,uv1,uv2,uv3
		});



		vertexIndex = verts.Count;
		verts.Add(rightBottomBack);
		verts.Add(rightTopBack);
		verts.Add(rightTopFront);
		verts.Add(rightBottomFront);
		if(invertRight){
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex);
		} else {
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex);
		}

		uvs.AddRange(new List<Vector2>(){
			uv0,uv1,uv2,uv3
		});



		vertexIndex = verts.Count;
		verts.Add(leftBottomBack);
		verts.Add(leftTopBack);
		verts.Add(rightTopBack);
		verts.Add(rightBottomBack);
		if(invertBack){
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex);
		} else {
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex);
		}

		uvs.AddRange(new List<Vector2>(){
			uv0,uv1,uv2,uv3
		});



		vertexIndex = verts.Count;
		verts.Add(leftBottomFront);
		verts.Add(leftTopFront);
		verts.Add(leftTopBack);
		verts.Add(leftBottomBack);
		if(invertLeft){
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex);
		} else {
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex);
		}

		uvs.AddRange(new List<Vector2>(){
			uv0,uv1,uv2,uv3
		});



		vertexIndex = verts.Count;
		verts.Add(leftBottomBack);
		verts.Add(rightBottomBack);
		verts.Add(rightBottomFront);
		verts.Add(leftBottomFront);
		if(invertBottom){
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex);
		} else {
			tris.Add(vertexIndex);
			tris.Add(vertexIndex+1);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+2);
			tris.Add(vertexIndex+3);
			tris.Add(vertexIndex);
		}

		uvs.AddRange(new List<Vector2>(){
			uv0,uv1,uv2,uv3
		});


		mesh.vertices = verts.ToArray();
		mesh.SetTriangles(tris.ToArray(),0);
		mesh.uv = uvs.ToArray();
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		mesh.Optimize();
		
		return mesh;
	}
}
