﻿// http://catlikecoding.com/unity/tutorials/octahedron-sphere/
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class OctahedronSphereTester : MonoBehaviour {

	public int subdivisions = 4;
	
	public float radius = 0.5f;
	
	[ContextMenu("Create")]
	private void Awake () {
		GetComponent<MeshFilter>().mesh = OctahedronSphereCreator.Create(subdivisions, radius);
	}
}