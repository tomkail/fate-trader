﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
public class ReverseNormals : MonoBehaviour {
    public bool reverseOnStart = true;
    void Start () {
        if(reverseOnStart)
        Reverse();
    }

    [ContextMenu("ReverseNormals")]
    void Reverse () {
        MeshFilter filter = GetComponent(typeof (MeshFilter)) as MeshFilter;
        if (filter != null) {
            Mesh mesh = filter.mesh;
            MeshX.ReverseNormal(mesh);
        }

        MeshCollider collider = GetComponent<MeshCollider>();
        if(collider != null)
        collider.sharedMesh = filter.mesh;
    }
}