//CURRENTLY MAKES A QUAD - FIX!
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class CreateTriangle : Editor {
    [MenuItem ("GameObject/Create Other/Triangle")]
    static void Create(){
        GameObject gameObject = new GameObject("Triangle");
        SimpleTriangle plane = gameObject.AddComponent<SimpleTriangle>();
        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
        meshFilter.mesh = new Mesh();
        plane.Rebuild();

        DestroyImmediate(gameObject.GetComponent<SimpleTriangle>());

        Selection.activeObject = plane;
        //gameObject.Destroy(gameObject.GetComponent<"SimpleTriangle">());
    }
}

[RequireComponent (typeof (MeshCollider))]
[RequireComponent (typeof (MeshFilter))]
[RequireComponent (typeof (MeshRenderer))]
public class SimpleTriangle : MonoBehaviour {
    public bool sharedVertices = false;
    public void Rebuild(){
        MeshFilter meshFilter = GetComponent<MeshFilter>();

        //Check if mesh already created and saved
        string planeAssetName = "Plane.asset";
        Mesh mesh = (Mesh)AssetDatabase.LoadAssetAtPath("Assets/Editor/" + planeAssetName,typeof(Mesh));
        //meshFilter.sharedMesh = mesh;
        if(mesh == null){
            mesh = new Mesh();
            mesh.name = planeAssetName;

            mesh.Clear();

            Vector3[] vertices = new Vector3[4];

            vertices[0] = new Vector3(-0.5f, -0.5f, 0);
            vertices[1] = new Vector3(0.5f, -0.5f, 0);
            vertices[2] = new Vector3(-0.5f, 0.5f, 0);
            vertices[3] = new Vector3(0.5f, 0.5f, 0);

            mesh.vertices = vertices;

            int[] tri = new int[6];

            tri[0] = 0;
            tri[1] = 2;
            tri[2] = 1;

            tri[3] = 2;
            tri[4] = 3;
            tri[5] = 1;

            mesh.triangles = tri;

            Vector3[] normals = new Vector3[4];

            normals[0] = -Vector3.forward;
            normals[1] = -Vector3.forward;
            normals[2] = -Vector3.forward;
            normals[3] = -Vector3.forward;

            mesh.normals = normals;

            Vector2[] uv = new Vector2[4];

            uv[0] = new Vector2(0, 0);
            uv[1] = new Vector2(1, 0);
            uv[2] = new Vector2(0, 1);
            uv[3] = new Vector2(1, 1);

            mesh.uv = uv;

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            mesh.Optimize();

            //Save mesh
            AssetDatabase.CreateAsset(mesh, "Assets/Editor/"+planeAssetName);
            AssetDatabase.SaveAssets();
        }

        meshFilter.mesh = mesh;
        meshFilter.sharedMesh = mesh;

        //Apply default material
        GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Plane);
        primitive.SetActive(false);
        Material diffuse = primitive.GetComponent<MeshRenderer>().sharedMaterial;
        DestroyImmediate(primitive);
        GetComponent<Renderer>().sharedMaterial = diffuse;

        
    }
}