#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

public class MeshExporter : MonoBehaviour {
	public enum MeshExportFormat {
		UnityMesh,
		OBJ,
	} 

	public MeshExportFormat format;

	public string folderName;
	public string fileName = "Exported Mesh";

	[ContextMenu("Export Unity Mesh")]
	[MenuItem ("Custom/ExportUnityMesh/Export Unity Mesh")]
	public void Export () {
		if(format == MeshExportFormat.UnityMesh) {
			ExportPrefab();
		} else if(format == MeshExportFormat.OBJ) {
			ExportOBJ();
		}
	}

	[ContextMenu("Export Unity Mesh")]
	public Mesh ExportPrefab () {
		string parentFolder = "Assets/"+folderName;
		return FileX.AttemptCreateAsset(parentFolder, fileName, GetComponent<MeshFilter>().mesh) as Mesh;
	}

	[ContextMenu("Export OBJ")]
	public void ExportOBJ () {
		string parentFolder = "Assets/"+folderName;
		ObjExporter.MeshToFile(GetComponent<MeshFilter>(), parentFolder+"/"+fileName+".obj");
	}
}
#endif