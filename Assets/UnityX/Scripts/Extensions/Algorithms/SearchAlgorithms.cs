﻿using UnityEngine;
using System;
using System.Collections.Generic;

public static class SearchAlgorithms {
	//	Used like this:
	//	SearchAlgorithms.BinarySearch(target, entities.Count, (int index) => {
	// 		return entities[index].distance;
	//	});
	public static int BinarySearch(float target, int listLength, Func<int, float> GetComparisonValueAtIndex) {
		int low = 0;
		int high = listLength;
		
		if (high < 0)
			Debug.LogError("The list length cannot be 0");
		while (low <= high) {
			int mid = (int)((uint)(low + high) >> 1);
			
			float midVal = GetComparisonValueAtIndex(mid);
			
			if (midVal < target)
				low = mid + 1;
			else if (midVal > target)
				high = mid - 1;
			else
				return mid; // key found
		}
		if(high < 0) {
			high = 0;
		}
		return high;
	}
	
	// Compare returns a signed int.
	public static int BinarySearch<T>(T target, int listLength, Func<int, T> GetComparisonValueAtIndex, Func<T, T, int> Compare) {
		int low = 0;
		int high = listLength;
		
		if (high < 0)
			Debug.LogError("The list length cannot be 0");
		while (low <= high) {
			int mid = (int)((uint)(low + high) >> 1);
			
			T midVal = GetComparisonValueAtIndex(mid);
			int compareResult = Compare(midVal, target);
			if (compareResult < 0)
				low = mid + 1;
			else if (compareResult > 0)
				high = mid - 1;
			else
				return mid; // key found
		}
		if(high < 0) {
			high = 0;
		}
		return high;
	}
}
