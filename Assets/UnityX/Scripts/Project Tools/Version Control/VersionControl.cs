﻿using UnityEngine;
using System.Collections;

public class VersionControl : MonoBehaviour {
	private static string VersionPlayerPrefKey = "My Game Version Key";
	private static string IncrementBuildNumberAutomaticallyPlayerPrefKey = "My Increment Build Number Automatically Key";
	

//	private static System.Version version = new Version(0,0,0,0);
	public static System.Version Version {
		get {
			if(!PlayerPrefs.HasKey(VersionControl.VersionPlayerPrefKey)) {
				Version = new System.Version(0,0,0,0);
			} 
			string versionString = PlayerPrefs.GetString(VersionControl.VersionPlayerPrefKey);
			string[] tokens = versionString.Split('.');
			int[] convertedItems = new int[tokens.Length];
			for(int i = 0; i < tokens.Length; i++) {
				convertedItems[i] = int.Parse(tokens[i]);
			}
			System.Version v = new System.Version(convertedItems[0],convertedItems[1],convertedItems[2],convertedItems[3]);
			return v;
		} set {
			PlayerPrefs.SetString(VersionControl.VersionPlayerPrefKey, value.ToString());
			if(VersionControl.OnVersionChange != null) VersionControl.OnVersionChange(value);
		}
	}
	
	public static bool IncrementAutomatically {
		get {
			if(!PlayerPrefs.HasKey(VersionControl.IncrementBuildNumberAutomaticallyPlayerPrefKey)) {
				VersionControl.IncrementAutomatically = true;
			}
			return PlayerPrefs.GetInt(VersionControl.IncrementBuildNumberAutomaticallyPlayerPrefKey).ToBool();
		}
		set {
			PlayerPrefs.SetInt(VersionControl.IncrementBuildNumberAutomaticallyPlayerPrefKey, value.ToInt());
		}
	}
	
	public static System.Version IncrementMajorNumber () {
		return VersionControl.Version = GetIncrementedMajorNumber();
	}
	
	public static System.Version GetIncrementedMajorNumber () {
		return new System.Version(VersionControl.Version.Major+1, 0, 0, 0);
	}
	
	public static System.Version IncrementMinorNumber () {
		return VersionControl.Version = VersionControl.GetIncrementedMinorNumber();
	}
	
	public static System.Version GetIncrementedMinorNumber () {
		return new System.Version(VersionControl.Version.Major, VersionControl.Version.Minor+1, 0, 0);
	}
	
	public static System.Version IncrementBuildNumber () {
		return VersionControl.Version = VersionControl.GetIncrementedBuildNumber();
	}
	
	public static System.Version GetIncrementedBuildNumber () {
		return new System.Version(VersionControl.Version.Major, VersionControl.Version.Minor, VersionControl.Version.Build+1, 0);
	}
	
	public static System.Version IncrementRevisionNumber () {
		return VersionControl.Version = VersionControl.GetIncrementedRevisionNumber();
	}
	
	public static System.Version GetIncrementedRevisionNumber () {
		return new System.Version(VersionControl.Version.Major, VersionControl.Version.Minor, VersionControl.Version.Build, VersionControl.Version.Revision+1);
	}
	
	public delegate void OnVersionChangeDelegate(System.Version version);
	public static event OnVersionChangeDelegate OnVersionChange;
}
