using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(VersionControl))]
public class VersionControlEditor : BaseEditor<VersionControl> {
	
	public bool editingVersionNumber = false;
	public int tmpMajor;
	public int tmpMinor;
	public int tmpBuild;
	public int tmpRevision;
	
	public override void OnInspectorGUI () {
		EditorGUILayout.LabelField("Version: "+VersionControl.Version);
		
		
		VersionControl.IncrementAutomatically = EditorGUILayout.Toggle(new GUIContent("Increment Automatically?", "Whether Unity increments the build number when a build is created."), VersionControl.IncrementAutomatically);
		
		if(GUILayout.Button("Increment major") && EditorUtility.DisplayDialog("Increment major?","Are you sure you want to increment the major number to "+VersionControl.GetIncrementedMajorNumber()+"?", "Increment", "Cancel")) {
			VersionControl.IncrementMajorNumber();
		}
		
		if(GUILayout.Button("Increment minor") && EditorUtility.DisplayDialog("Increment minor?","Are you sure you want to increment the minor number to "+VersionControl.GetIncrementedMinorNumber()+"?", "Increment", "Cancel")) {
			VersionControl.IncrementMinorNumber();
		}
		
		if(GUILayout.Button("Change Version")) {
			editingVersionNumber = !editingVersionNumber;
			tmpMajor = VersionControl.Version.Major;
			tmpMinor = VersionControl.Version.Minor;
			tmpBuild = VersionControl.Version.Build;
			tmpRevision = VersionControl.Version.Revision;
		}
		
		if (EditorGUILayout.BeginFadeGroup(editingVersionNumber.ToInt())) {
			EditorGUI.indentLevel++;
			tmpMajor = EditorGUILayout.IntField("Major", tmpMajor);
			tmpMinor = EditorGUILayout.IntField("Minor", tmpMinor);
			tmpBuild = EditorGUILayout.IntField("Build", tmpBuild);
			tmpRevision = EditorGUILayout.IntField("Revision", tmpRevision);
			if(GUILayout.Button("Commit")) {
				System.Version newVersion = new System.Version(tmpMajor, tmpMinor, tmpBuild, tmpRevision);
				if(EditorUtility.DisplayDialog("Commit version number change?","Are you sure you want to change the version number to "+newVersion+"?", "Change", "Cancel")) {
					VersionControl.Version = newVersion;
					editingVersionNumber = false;
				}
			}
			EditorGUI.indentLevel--;
		}
		EditorGUILayout.EndFadeGroup();
	}
}
