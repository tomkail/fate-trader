using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

[InitializeOnLoad]
public class VersionControlAutoIncrementer {
	
	static VersionControlAutoIncrementer() {
		UnityEditorPlayModeController.OnPlayModeChanged += VersionControlAutoIncrementer.ChangePlayMode;
	}
	
	public static bool IsDevelopmentBuild {
		get {
			return Application.isEditor || Debug.isDebugBuild;
		}
	}
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
		if(VersionControl.IncrementAutomatically && IsDevelopmentBuild)
		VersionControl.IncrementBuildNumber();
	}
	
	public static void ChangePlayMode(UnityEditorPlayModeState lastState, UnityEditorPlayModeState newState) {
		if(VersionControl.IncrementAutomatically && VersionControlAutoIncrementer.IsDevelopmentBuild && (lastState == UnityEditorPlayModeState.Stopped && newState == UnityEditorPlayModeState.Playing))
		VersionControl.IncrementRevisionNumber();
	}
}