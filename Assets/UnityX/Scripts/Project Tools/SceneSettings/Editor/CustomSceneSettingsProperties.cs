using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

namespace UnityX.SceneManagement {
	public class CustomSceneSettingsProperties : ScriptableObject {
		[SceneName(SceneNameAttribute.SceneFindMethod.AllInProject)]
		public string[] sceneNames;
		public bool is2D = false;

		public virtual void OnSetAsActive () {
			#if UNITY_EDITOR
			if(SceneView.lastActiveSceneView == null) return;
			SceneView.lastActiveSceneView.in2DMode = is2D;
			#endif
		}
		
		public virtual void OnUnsetAsActive () {
			
		}

		public virtual void OnLevelLoad () {
			
		}
		
		public virtual void OnLevelUpdate () {
			
		}
		
		public virtual void OnLevelUnload () {
			
		}
	}
}