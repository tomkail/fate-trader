﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Localization. Sets the default language. Anomaly X is currently only translated to English.
/// </summary>
public class Localization : MonoBehaviour {
	/// <summary>
	/// The default language
	/// </summary>
	public static Language defaultLanguage = Language.English;
	
	/// <summary>
	/// The current language
	/// </summary>
	public static Language language = defaultLanguage;
	public static Language Language {
		get {
			return Localization.language;
		} set {
			Language lastLanguage = Localization.language;
			Localization.language = value;
			if(OnLanguageChange != null) OnLanguageChange(lastLanguage, Localization.language);
		}
	}
	
	/// <summary>
	/// Event called when the language changes
	/// </summary>
	public delegate void OnLanguageChangeEvent(Language lastLanguage, Language newLanguage);
	public static event OnLanguageChangeEvent OnLanguageChange;
	
	/// <summary>
	/// This function is called when the instance is used the first time
	/// Put all the initializations you need here, as you would do in Awake
	/// </summary>
	public void Init () {
		Language = Localization.LanguageFromSystemLanguage();
	}

	/// <summary>
	/// Returns a supported language from automatically detected system language
	/// </summary>
	private static Language LanguageFromSystemLanguage () {
		switch (Application.systemLanguage) {
		    case SystemLanguage.Arabic:
			return Language.English;
		    case SystemLanguage.Basque:
			return Language.English;
			case SystemLanguage.Belarusian:
			return Language.English;
			case SystemLanguage.Bulgarian:
			return Language.English;
			case SystemLanguage.Catalan:
			return Language.English;
			case SystemLanguage.Chinese:
			return Language.Chinese;
			case SystemLanguage.Czech:
			return Language.English;
			case SystemLanguage.Danish:
			return Language.English;
			case SystemLanguage.Dutch:
			return Language.English;
			case SystemLanguage.English:
			return Language.English;
			case SystemLanguage.Estonian:
			return Language.English;
			case SystemLanguage.Faroese:
			return Language.English;
			case SystemLanguage.Finnish:
			return Language.English;
			case SystemLanguage.French:
			return Language.French;
			case SystemLanguage.German:
			return Language.German;
			case SystemLanguage.Greek:
			return Language.English;
			case SystemLanguage.Hebrew:
			return Language.English;
			case SystemLanguage.Icelandic:
			return Language.English;
			case SystemLanguage.Indonesian:
			return Language.English;
			case SystemLanguage.Italian:
			return Language.English;
			case SystemLanguage.Japanese:
			return Language.English;
			case SystemLanguage.Korean:
			return Language.English;
			case SystemLanguage.Latvian:
			return Language.English;
			case SystemLanguage.Lithuanian:
			return Language.English;
			case SystemLanguage.Norwegian:
			return Language.English;
			case SystemLanguage.Polish:
			return Language.English;
			case SystemLanguage.Portuguese:
			return Language.English;
			case SystemLanguage.Romanian:
			return Language.English;
			case SystemLanguage.Russian:
			return Language.English;
			case SystemLanguage.SerboCroatian:
			return Language.English;
			case SystemLanguage.Slovak:
			return Language.English;
			case SystemLanguage.Slovenian:
			return Language.English;
			case SystemLanguage.Spanish:
			return Language.English;
			case SystemLanguage.Swedish:
			return Language.English;
			case SystemLanguage.Thai:
			return Language.English;
			case SystemLanguage.Turkish:
			return Language.English;
			case SystemLanguage.Ukrainian:
			return Language.English;
			case SystemLanguage.Vietnamese:
			return Language.English;
			case SystemLanguage.Unknown:
			return Language.English;
			case SystemLanguage.Hungarian:
			return Language.English;
			default:
			return Language.English;
		}
	}
}