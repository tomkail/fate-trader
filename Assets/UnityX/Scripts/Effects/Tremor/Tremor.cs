﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Base Tremor class.
/// </summary>
[System.Serializable]
public abstract class Tremor {
	public bool alive = true;
	
	/// <summary>
	/// The tremor position.
	/// </summary>
	public Vector2 tremorPosition = Vector3.zero;
	
	/// <summary>
	/// The life of the tremor.
	/// </summary>
	public float _life = 1f;
	public float life {
		get {
			return _life;
		} set {
			_life = value;
			normalizedLife = life/lifetime;
		}
	}
	
	/// <summary>
	/// The lifetime of the tremor.
	/// </summary>
	public float lifetime = 1f;
	
	/// <summary>
	/// The normalized life of the tremor.
	/// </summary>
	public float normalizedLife;
	
	/// <summary>
	/// The magnitude of the tremor.
	/// </summary>
	public float magnitude = 20;
	
	/// <summary>
	/// The time for one tremor.
	/// </summary>
	public float tremorTime;
	public float tremorSpeed;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="Tremor"/> class.
	/// </summary>
	/// <param name="_lifetime">_lifetime.</param>
	/// <param name="_magnitude">_magnitude.</param>
	public Tremor(float _lifetime, float _tremorTime, float _magnitude) {
		life = lifetime = _lifetime;
		tremorTime = _tremorTime;
		magnitude = _magnitude;
		tremorSpeed = 1f/tremorTime;
		Init();
	}
	
	/// <summary>
	/// Init this instance of the tremor.
	/// </summary>
	public virtual void Init() {
		UpdateTremor();
	}
	
	public virtual void Loop(){
		Loop(Time.deltaTime);
	}
	
	public virtual void Loop(float _deltaTime){
		life -= _deltaTime;
		if(life <= 0) {
			alive = false;
		}
		UpdateTremor();
	}
	
	public abstract void UpdateTremor();
}
