﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RandomTremor : Tremor {

	
	protected float seed;
//	
//	public RandomTremor(float myLifetime, float myMagnitude, float myShakeTime) : base (myLifetime) {
//		magnitude = Vector2.one * myMagnitude;
//	}
	
	public RandomTremor(float _lifetime, float _shakeTime, float _magnitude) : base (_lifetime, _shakeTime, _magnitude) {
//		magnitude = myMagnitude;
	}
	
	public override void Init(){
		RandomizePerlinReference();
		base.Init ();
	}
	
	public virtual void RandomizePerlinReference(){
		seed = Random.value;
	}
	
	public override void UpdateTremor(){
//		tremorPosition = Vector2.Scale(MathX.Degrees2Vector2(Mathf.PerlinNoise(life * 100, seed) * 10000) * normalizedLife, magnitude);
//		float shakesPerSecond = 1f/tremorTime;
		float shake = MathX.OscellateTriangleRepeating((lifetime - life) * tremorSpeed);
		float x = Mathf.FloorToInt((lifetime - life) * tremorSpeed) * seed;
		float y = seed;
//		Debug.Log (x+" "+y+" "+Mathf.PerlinNoise(x,y));
		Vector2 direction = MathX.Degrees2Vector2(10000 * Mathf.PerlinNoise(x,y));
		tremorPosition = Vector2.Scale(Vector2.one * shake, direction * magnitude * (normalizedLife * normalizedLife));
	}
}
