using System;
using UnityEngine;

namespace UnityX.ImageEffects {
    [ExecuteInEditMode]
    [RequireComponent (typeof (Camera))]
    [AddComponentMenu ("UnityX/Image Effects/Edge Detection")]
    public class EdgeDetection : MonoBehaviour
    {
		protected bool  supportHDRTextures = true;
		protected bool  supportDX11 = false;
		protected bool  isSupported = true;
		
		
        public enum EdgeDetectMode
        {
            TriangleDepthNormals = 0,
            RobertsCrossDepthNormals = 1,
            SobelDepth = 2,
            SobelDepthThin = 3,
            TriangleLuminance = 4,
        }


        public EdgeDetectMode mode = EdgeDetectMode.SobelDepthThin;
        public float sensitivityDepth = 1.0f;
        public float sensitivityNormals = 1.0f;
        public float lumThreshold = 0.2f;
        public float edgeExp = 1.0f;
        public float sampleDist = 1.0f;
		public Color edgeColor = Color.white;
        public float edgesOnly = 0.0f;
        public Color edgesOnlyBgColor = Color.white;

        public Shader edgeDetectShader;
        private Material edgeDetectMaterial = null;
        private EdgeDetectMode oldMode = EdgeDetectMode.SobelDepthThin;

        void Start ()
		{
            oldMode	= mode;
        }

        void SetCameraFlag ()
		{
            if (mode == EdgeDetectMode.SobelDepth || mode == EdgeDetectMode.SobelDepthThin)
                GetComponent<Camera>().depthTextureMode |= DepthTextureMode.Depth;
            else if (mode == EdgeDetectMode.TriangleDepthNormals || mode == EdgeDetectMode.RobertsCrossDepthNormals)
                GetComponent<Camera>().depthTextureMode |= DepthTextureMode.DepthNormals;
        }

        void OnEnable ()
		{
            SetCameraFlag();
        }

        //[ImageEffectOpaque]
        void OnRenderImage (RenderTexture source, RenderTexture destination)
		{
            if (CheckResources () == false)
			{
                Graphics.Blit (source, destination);
                return;
            }

            Vector2 sensitivity = new Vector2 (sensitivityDepth, sensitivityNormals);
            edgeDetectMaterial.SetVector ("_Sensitivity", new Vector4 (sensitivity.x, sensitivity.y, 1.0f, sensitivity.y));
            edgeDetectMaterial.SetFloat ("_BgFade", edgesOnly);
            edgeDetectMaterial.SetFloat ("_SampleDistance", sampleDist);
            edgeDetectMaterial.SetVector ("_BgColor", edgesOnlyBgColor);
            edgeDetectMaterial.SetFloat ("_Exponent", edgeExp);
            edgeDetectMaterial.SetFloat ("_Threshold", lumThreshold);
			edgeDetectMaterial.SetColor ("_Color", edgeColor);
            Graphics.Blit (source, destination, edgeDetectMaterial, (int) mode);
        }
		
		public bool CheckResources ()
		{
			CheckSupport (true);
			
			edgeDetectMaterial = CheckShaderAndCreateMaterial (edgeDetectShader,edgeDetectMaterial);
			if (mode != oldMode)
				SetCameraFlag ();
			
			oldMode = mode;
			
			if (!isSupported)
				ReportAutoDisable ();
			return isSupported;
		}
        
		protected Material CheckShaderAndCreateMaterial ( Shader s, Material m2Create)
		{
			if (!s)
			{
				Debug.Log("Missing shader in " + ToString ());
				enabled = false;
				return null;
			}
			
			if (s.isSupported && m2Create && m2Create.shader == s)
				return m2Create;
			
			if (!s.isSupported)
			{
				NotSupported ();
				Debug.Log("The shader " + s.ToString() + " on effect "+ToString()+" is not supported on this platform!");
				return null;
			}
			else
			{
				m2Create = new Material (s);
				m2Create.hideFlags = HideFlags.DontSave;
				if (m2Create)
					return m2Create;
				else return null;
			}
		}
		
		protected bool CheckSupport (bool needDepth)
		{
			isSupported = true;
			supportHDRTextures = SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf);
			supportDX11 = SystemInfo.graphicsShaderLevel >= 50 && SystemInfo.supportsComputeShaders;
			
			if (!SystemInfo.supportsImageEffects || !SystemInfo.supportsRenderTextures)
			{
				NotSupported ();
				return false;
			}
			
			if (needDepth && !SystemInfo.SupportsRenderTextureFormat (RenderTextureFormat.Depth))
			{
				NotSupported ();
				return false;
			}
			
			if (needDepth)
				GetComponent<Camera>().depthTextureMode |= DepthTextureMode.Depth;
			
			return true;
		}
		
		protected void NotSupported ()
		{
			enabled = false;
			isSupported = false;
			return;
		}
		
		protected void ReportAutoDisable ()
		{
			Debug.LogWarning ("The image effect " + ToString() + " has been disabled as it's not supported on the current platform.");
		}
    }
}
