﻿Shader "Custom/CRTShader" 
{
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Opacity("Opacity", Float) = 0
        _VertsColor("Verts fill color", Float) = 0 
		_Contrast("Contrast", Float) = 0
        _Br("Brightness", Float) = 0
		_Size("Size", Range(1,5)) = 1
    }

    SubShader {
        Pass {
            ZTest Always Cull Off ZWrite Off Fog { Mode off }

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest
            #include "UnityCG.cginc"
            #pragma target 3.0

            struct v2f 
            {
                float4 pos      : POSITION;
                float2 uv       : TEXCOORD0;
                float4 scr_pos : TEXCOORD1;
            };

            uniform sampler2D _MainTex;
            uniform float _Opacity;
            uniform float _VertsColor;
			uniform float _Contrast;
            uniform float _Br;
			uniform float _Size;

            v2f vert(appdata_img v)
            {
                v2f o;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord);
                o.scr_pos = ComputeScreenPos(o.pos);
                return o;
            }

            half4 frag(v2f i): COLOR
            {
                half4 color = tex2D(_MainTex, i.uv);
                half4 finalColor = color;
                
                finalColor += (_Br / 255);
				finalColor = finalColor - _Contrast * (finalColor - 1.0) * finalColor *(finalColor - 0.5);

            	float2 ps = (i.scr_pos.xy *_ScreenParams.xy / i.scr_pos.w) / (int)_Size;

            	int pp = ((int)ps.x) % 3;
				float4 outcolor = float4(0, 0, 0, 1);
				float4 muls = float4(0.5, 0.5, 0.5, 0);

				if (pp == 1) { muls.r = 1; muls.g = _VertsColor; }
    			else if (pp == 2) { muls.g = 1; muls.b = _VertsColor; }
				else { muls.b = 1; muls.r = _VertsColor; } 
        		color = lerp(color, finalColor * muls, _Opacity);

				return color;

                //return color;
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}