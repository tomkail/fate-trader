using UnityEngine;
namespace UnityX.ImageEffects {
 
[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
	public class TVShader : MonoBehaviour
	{
	    public Shader shader;
	 	
	    [Range(0, 1)] public float opacity = 0.0f;
	 	[Range(0, 1)] public float verts_force = 0.0f;
		[Range(-3, 20)] public float contrast = 0.0f;
	    [Range(-200, 200)] public float brightness = 0.0f;
		[Range(1, 5)] public int size = 1;
	
	    private Material _material;
	    protected Material material
	    {
	        get
	        {
	            if (_material == null)
	            {
	                _material = new Material(shader);
	                _material.hideFlags = HideFlags.HideAndDontSave;
	            }
	            return _material;
	        }
	    }
	 
	    private void OnRenderImage(RenderTexture source, RenderTexture destination)
	    {
	        if (shader == null) return;
	        Material mat = material;
	        mat.SetFloat("_Opacity", opacity);
	        mat.SetFloat("_VertsColor", 1-verts_force);
			mat.SetFloat("_Contrast", contrast);
	        mat.SetFloat("_Br", brightness);
			mat.SetFloat("_Size", size);
	        Graphics.Blit(source, destination, mat);
	    }
	 
	    void OnDisable()
	    {
	        if (_material)
	        {
	            DestroyImmediate(_material);
	        }
	    }
	}
}