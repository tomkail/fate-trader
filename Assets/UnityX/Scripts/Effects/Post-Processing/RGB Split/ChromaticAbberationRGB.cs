using UnityEngine;
 
namespace UnityX.ImageEffects {
	
	[ExecuteInEditMode]
	[RequireComponent(typeof(Camera))]
	public class ChromaticAbberationRGB : MonoBehaviour
	{
	    public Shader shader;
	 	
	 	public Vector2 red;
		public Vector2 green;
	    public Vector2 blue;
	
	    private Material _material;
	    protected Material material
	    {
	        get
	        {
	            if (_material == null)
	            {
	                _material = new Material(shader);
	                _material.hideFlags = HideFlags.HideAndDontSave;
	            }
	            return _material;
	        }
	    }
	 
	    private void OnRenderImage(RenderTexture source, RenderTexture destination)
	    {
	        if (shader == null) return;
	        Material mat = material;
	        mat.SetFloat("_RedOffsetX", red.x);
	        mat.SetFloat("_RedOffsetY", red.y);
			mat.SetFloat("_GreenOffsetX", green.x);
	        mat.SetFloat("_GreenOffsetY", green.y);
	        mat.SetFloat("_BlueOffsetX", blue.x);
			mat.SetFloat("_BlueOffsetY", blue.y);
	        Graphics.Blit(source, destination, mat);
	    }
	 
	    void OnDisable()
	    {
	        if (_material)
	        {
	            DestroyImmediate(_material);
	        }
	    }
	}
}