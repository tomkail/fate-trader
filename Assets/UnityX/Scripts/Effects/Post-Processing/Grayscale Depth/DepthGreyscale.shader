﻿Shader "Custom/DepthGrayscale" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_FadeColor ("Fade Color", Color) = (0.5, 0.5, 0.5, 0)
	}

	SubShader {
		Tags { "RenderType"="Opaque" }

		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _CameraDepthTexture;

			float4 _Color;
			float4 _FadeColor;

			struct v2f {
				float4 pos : SV_POSITION;
				float4 scrPos:TEXCOORD1;
			};

			//Vertex Shader
			v2f vert (appdata_base v){
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.scrPos=ComputeScreenPos(o.pos);
				return o;
			}

			//Fragment Shader
			half4 frag (v2f i) : COLOR{
				float depthValue = Linear01Depth (tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);
				half4 depth;

				//float lerpValue = clamp((IN.pos.y - _FogMaxHeight) / (_FogMinHeight - _FogMaxHeight), 0, 1);
				depth.rgb = lerp (_Color.rgb, _FadeColor.rgb, depthValue * _FadeColor.a);

//				depth.r = depthValue;
				//depth.g = depthValue;
				//depth.b = depthValue;

				//depth.a = 1;
				return depth;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}