﻿using UnityEngine;
using UnityEngine.UI;
using System;
 
public class ScreenFade : MonoSingleton<ScreenFade> {
	public bool fading = false;
	public ColorTween colorTween;
	public Color currentColor;
	
	public Image image;
	
	public delegate void OnCompleteEvent();
	public static event OnCompleteEvent OnComplete;

	// Initialize the texture, background-style and Initial color:
	public override void Init () {
		image = GetComponentInChildren<Image>();
        colorTween = new ColorTween(Color.clear);
		colorTween.OnComplete += FinishFade;
		base.Init ();
	}
 
	// Draw the texture and perform the fade:
	public void Loop() {
		if(colorTween.tweening) {
			currentColor = colorTween.Loop(Time.deltaTime);
		}

		// Only draw the texture when the alpha value is greater than 0
//		if(currentColor.a != 0)
		ScreenFade.RenderColor();
    }

    public static void FadeToClear () {
    	ScreenFade.Fade(Color.clear, 1f);
    }

	public static void FadeToBlack () {
    	ScreenFade.Fade(Color.black, 1f);
    }

	public static void RenderColor() {
		ScreenFade.Instance.image.color = ScreenFade.Instance.currentColor;
	}

	public static void SetColor(Color myTargetColor) {
		ScreenFade.Instance.colorTween.Stop();
		ScreenFade.Instance.colorTween.currentValue = myTargetColor;
		ScreenFade.Instance.currentColor = myTargetColor;
		RenderColor();
	}

	public static void Fade(Color myTargetColor, float myFadeDuration) {
		if(ScreenFade.Instance.fading) ScreenFade.Instance.FinishFade();
		ScreenFade.Instance.colorTween.Tween(ScreenFade.Instance.colorTween.currentValue, myTargetColor, myFadeDuration);
		//ScreenFade.Instance.colorTween.OnComplete += ScreenFade.Instance.FinishFade;
		ScreenFade.Instance.fading = true;
	}

	public static void Fade(Color myTargetColor, float myFadeDuration, AnimationCurve myFadeCurve) {
		if(ScreenFade.Instance.fading) ScreenFade.Instance.FinishFade();
		ScreenFade.Instance.colorTween.Tween(ScreenFade.Instance.colorTween.currentValue, myTargetColor, myFadeDuration, myFadeCurve);
		//ScreenFade.Instance.colorTween.OnComplete += ScreenFade.Instance.FinishFade;
		ScreenFade.Instance.fading = true;
	}

	public static void Fade(Color myTargetColor, float myFadeDuration, AnimationCurve myFadeCurve, ColorBlendMode myColorBlendMode) {
		if(ScreenFade.Instance.fading) ScreenFade.Instance.FinishFade();
		ScreenFade.Instance.colorTween.Tween(ScreenFade.Instance.colorTween.currentValue, myTargetColor, myFadeDuration, myFadeCurve, myColorBlendMode);
		//ScreenFade.Instance.colorTween.OnComplete += ScreenFade.Instance.FinishFade;
		ScreenFade.Instance.fading = true;
	}
	
	/// <summary>
	/// Ends a fade
	/// </summary>
	private void FinishFade () {
		currentColor = colorTween.targetValue;
		fading = false;
		if(OnComplete != null) OnComplete();
		RenderColor();
	}
}