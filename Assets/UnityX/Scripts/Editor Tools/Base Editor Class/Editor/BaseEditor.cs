﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Reflection;

[CanEditMultipleObjects]
public class BaseEditor<T> : Editor where T : UnityEngine.Object {
	
	#pragma warning disable
	protected T data;
	
	public virtual void OnEnable() {
		SetData();
	}
	
	public override void OnInspectorGUI() {
		SetData();
		
		GUIContent label = new GUIContent();
		label.text = "Properties"; //
 
		DrawDefaultInspector();
		//DrawDefaultInspectors(label, data);
 
		if(GUI.changed && target != null) {         
			EditorUtility.SetDirty(target);
		}
	}
	
	protected void SetData () {
		DebugX.Assert(target != null, "Target is null "+typeof(T));
		DebugX.Assert(target as T != null, "Cannot cast "+target + " to "+typeof(T));
		data = (T) target;
	}
}

// Usage Example:

// using UnityEditor;
// using UnityEngine;
// using System.Collections;

// [CanEditMultipleObjects]
// [CustomEditor(typeof(Level))]
// public class LevelEditor : BaseEditor<Level>
// {}
