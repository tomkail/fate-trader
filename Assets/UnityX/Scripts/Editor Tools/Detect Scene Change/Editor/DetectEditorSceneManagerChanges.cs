﻿using UnityEditor;

namespace UnityX.SceneManagement {

	/// <summary>
	/// Provides events for when scenes change.
	/// </summary>
	[InitializeOnLoad]
	public static class DetectEditorSceneManagerChanges {

		private static SceneChangeDetector sceneChangeDetector;

		// Event proxies.
		public static event SceneChangeDetector.OnChangeActiveSceneEvent OnChangeActiveScene;
		public static event SceneChangeDetector.OnAddSceneEvent OnAddScene;
		public static event SceneChangeDetector.OnRemoveSceneEvent OnRemoveScene;
		
		static DetectEditorSceneManagerChanges () {
			sceneChangeDetector = new SceneChangeDetector();
			sceneChangeDetector.OnAddScene += (loadedScene) => {
				if(!EditorApplication.isPlaying) 
					OnAddScene(loadedScene);
			};
			sceneChangeDetector.OnRemoveScene += (unloadedSceneName) => {
				if(!EditorApplication.isPlaying) 
					OnRemoveScene(unloadedSceneName);
			};
			sceneChangeDetector.OnChangeActiveScene += (lastActiveSceneName, currentActiveScene) => {
				if(!EditorApplication.isPlaying) 
					OnChangeActiveScene(lastActiveSceneName, currentActiveScene);
			};

			EditorApplication.update += Update;
		}
		
		private static void Update () {
			sceneChangeDetector.Update();
		}
	}
}