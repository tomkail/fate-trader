using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

namespace UnityX.SceneManagement {

	/// <summary>
	/// Detects changes to the SceneManager.
	/// Update this within a static editor script or a gameobject that persists across scenes.
	/// </summary>
	public class SceneChangeDetector {

		public delegate void OnChangeActiveSceneEvent(string lastActiveSceneName, Scene currentActiveScene);
		public event OnChangeActiveSceneEvent OnChangeActiveScene;

		public delegate void OnAddSceneEvent(Scene loadedScene);
		public event OnAddSceneEvent OnAddScene;

		public delegate void OnRemoveSceneEvent(string unloadedSceneName);
		public event OnRemoveSceneEvent OnRemoveScene;

		private Scene cachedActiveScene;
		private string cachedActiveSceneName;
		private Scene[] cachedScenes;
		private string[] cachedSceneNames;
		
		public SceneChangeDetector () {
			cachedActiveScene = SceneManager.GetActiveScene();
			cachedActiveSceneName = cachedActiveScene.name;
			cachedScenes = GetAllScenes();
			cachedSceneNames = GetSceneNames(cachedScenes);
		}
		
		public void Update () {
			Scene activeScene = SceneManager.GetActiveScene();
			Scene[] currentScenes = GetAllScenes();
			string[] currentSceneNames = GetSceneNames(currentScenes);

			string[] unloadedSceneNames = cachedSceneNames.Except(currentSceneNames).ToArray();
			Scene[] loadedScenes = currentScenes.Except(cachedScenes).ToArray();
			// If we've changed the active scene at the same time as the number of scenes changed, then assume that we've opened a scene file.
			// If we to switch to a scene that was previously loaded, it won't have been added to the unloaded scene names list, so won't call unload.
			// To fix this, we simply unload all the scenes that were active.
			if((loadedScenes.Length > 0 || unloadedSceneNames.Length > 0) && activeScene != cachedActiveScene) {
				unloadedSceneNames = cachedSceneNames;
			}

			foreach(string unloadedSceneName in unloadedSceneNames) {
				if(OnRemoveScene != null) OnRemoveScene(unloadedSceneName);
			}

			foreach(Scene loadedScene in loadedScenes) {
				if(OnAddScene != null) OnAddScene(loadedScene);
			}

			if(activeScene != cachedActiveScene) {
				if(OnChangeActiveScene != null) OnChangeActiveScene(cachedActiveSceneName, activeScene);
				cachedActiveScene = activeScene;
				cachedActiveSceneName = cachedActiveScene.name;
			}

			if(unloadedSceneNames.Length > 0 || loadedScenes.Length > 0)
			cachedScenes = currentScenes;
			cachedSceneNames = currentSceneNames;
		}

		private string[] GetSceneNames (Scene[] scenes) {
			string[] names = new string[scenes.Length];
			for(int i = 0; i < scenes.Length; i++) {
				names[i] = scenes[i].name;
			}
			return names;
		}

		private Scene[] GetAllScenes () {
			Scene[] scenes = new Scene[SceneManager.sceneCount];
			for(int i = 0; i < scenes.Length; i++) {
				scenes[i] = SceneManager.GetSceneAt(i);
			}
			return scenes;
		}
	}
}