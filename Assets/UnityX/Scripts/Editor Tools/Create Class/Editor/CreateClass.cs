﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Reflection;
using System.Linq;

public class CreateClass : EditorWindow {
	public string objectName = "MyNewClass";
	[MenuItem("Assets/Create/Custom C# Class")]
	static void Init () {
		EditorWindow.GetWindow(typeof(CreateClass));
	}
	
	void OnGUI () {
		objectName = EditorGUILayout.TextField ("Object Name ", objectName);
		
		if(GUILayout.Button("Create Scriptable Object")) {
			ClassUtility.Create(objectName);
//			ClassUtility.CreateRawCSharp(newClassName, output);
		}
	}
}

#endif