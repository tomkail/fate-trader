﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

[InitializeOnLoad]
/// <summary>
/// Manages the editor's play mode state.
/// </summary>
public class UnityEditorPlayModeController {
	
	private static UnityEditorPlayModeState currentState = UnityEditorPlayModeState.Stopped;
	
	static UnityEditorPlayModeController() {
		EditorApplication.playmodeStateChanged += OnUnityPlayModeChanged;
	}
	
	public delegate void OnPlayModeChangedEvent(UnityEditorPlayModeState lastState, UnityEditorPlayModeState currentState);
	public static event OnPlayModeChangedEvent OnPlayModeChanged;
	
	public static void Play() {
		EditorApplication.isPlaying = true;
	}
	
	public static void Pause() {
		EditorApplication.isPaused = true;
	}
	
	public static void Stop() {
		EditorApplication.isPlaying = false;
	}
	
	private static void OnUnityPlayModeChanged() {
		//Unity calls this event twice when the button is pressed; once when the button is pressed, and once when the game starts. This return statement allows our event to run correctly.
		if (!EditorApplication.isPlaying) return;
		
		UnityEditorPlayModeState lastState = currentState;
		currentState = GetNewState(currentState);
		if (OnPlayModeChanged != null) OnPlayModeChanged(lastState, currentState);
//		Debug.Log ("Changed from "+lastState + " to " + currentState);
		
		//Pressing the advance frame button doesn't trigger Unity's PlayModeChange event twice like it should. It instead triggers once, and sets isPlaying and isPaused to true. 
		//GetNewState will therefore enter the wrong state, so we run the function again to restore it to the paused state, which also forces our PlayModeChange event to fire correctly.
		if(currentState == UnityEditorPlayModeState.Playing && EditorApplication.isPaused) {
			OnUnityPlayModeChanged();
		}
	}
	
	private static UnityEditorPlayModeState GetNewState (UnityEditorPlayModeState _currentState) {
		UnityEditorPlayModeState changedState = UnityEditorPlayModeState.Stopped;
		switch (_currentState) {
		case UnityEditorPlayModeState.Stopped:
			if (EditorApplication.isPlayingOrWillChangePlaymode) {
				changedState = UnityEditorPlayModeState.Playing;
			}
			break;
		case UnityEditorPlayModeState.Playing:
			if (EditorApplication.isPaused) {
				changedState = UnityEditorPlayModeState.Paused;
			} else {
				changedState = UnityEditorPlayModeState.Stopped;
			}
			break;
		case UnityEditorPlayModeState.Paused:
			if (EditorApplication.isPlayingOrWillChangePlaymode) {
				changedState = UnityEditorPlayModeState.Playing;
			} else {
				changedState = UnityEditorPlayModeState.Stopped;
			}
			break;
		default:
			throw new ArgumentOutOfRangeException();
		}
		return changedState;
	}
}
#endif