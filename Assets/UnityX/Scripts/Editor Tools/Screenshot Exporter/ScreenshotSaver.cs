﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Screenshot saver. Allows developers or players to create screenshots.
/// </summary>
public class ScreenshotSaver {
	private static bool _capturingScreenshot;
	public static bool capturingScreenshot { 
		get { 
			return _capturingScreenshot;
		} private set {
			_capturingScreenshot = value;
		}
	}
	
	public delegate void OnCompleteScreenshotCaptureEvent (Texture2D screenshot);
	public static event OnCompleteScreenshotCaptureEvent OnCompleteScreenshotCapture;
	
	public static void CaptureScreenshot(int width, int height) {
		CaptureScreenshot(width, height, Camera.allCameras);
	}
	
	public static void CaptureScreenshot(int width, int height, IList<Camera> cameras, ScreenshotSaverTextureFormat textureFormat = ScreenshotSaverTextureFormat.RGB24) {
		CoroutineHelper.Execute(CaptureScreenshotCoroutine(width, height, cameras, textureFormat));
	}
	
	public static IEnumerator CaptureScreenshotCoroutine(int width, int height, IList<Camera> cameras, ScreenshotSaverTextureFormat textureFormat = ScreenshotSaverTextureFormat.RGB24){
		List<Camera> _cameras = cameras.ToList().RemoveNull();
		if(capturingScreenshot || width <= 0 || height <= 0 || _cameras.IsNullOrEmpty()) {
			if(capturingScreenshot) Debug.LogError("Could not capture screenshot because a screenshot is currently being captured by the same ScreenshotSaver.");
			else if(width <= 0) Debug.LogError("Could not capture screenshot because width is "+width+".");
			else if(height <= 0) Debug.LogError("Could not capture screenshot because height is "+height+".");
			else if(_cameras.IsNullOrEmpty()) Debug.LogError("Could not capture screenshot because camera array contains no cameras.");
			yield break;
		}
		yield return new WaitForEndOfFrame();
		
		// Its possible that the value for depth should depend on the export format. Not checked.
		RenderTexture rt = new RenderTexture(width, height, 24);
		// Must render and wait for frame twice to ensure that UI Text has refreshed.
		foreach(Camera cam in _cameras) {
			if(!cam.isActiveAndEnabled) continue;
			cam.targetTexture = rt;
			cam.Render();
		}

		yield return new WaitForEndOfFrame();
		rt = new RenderTexture(width, height, 24);
		yield return new WaitForEndOfFrame();

		foreach(Camera cam in _cameras) {
			if(!cam.isActiveAndEnabled) continue;
			cam.targetTexture = rt;
//			cam.SetTargetBuffers()
			cam.Render();
			cam.targetTexture = null;
		}

		RenderTexture savedActiveRenderTexture = RenderTexture.active;
		RenderTexture.active = rt;
		
		Texture2D screenshot = new Texture2D(width, height, ScreenshotSaverTextureFormatUtils.ToTextureFormat(textureFormat), false);
		screenshot.ReadPixels(new Rect(0, 0, width, height), 0, 0);

		RenderTexture.active = savedActiveRenderTexture;
		// Not sure if this is needed. Use in case of memory leaks.
//		rt.Release();
		MonoBehaviour.Destroy(rt);
		
		if(OnCompleteScreenshotCapture != null) {
			OnCompleteScreenshotCapture(screenshot);
		}
	}
	
	private static void HandleOnCompleteScreenshotCapture (Texture2D screenshot) {			
		capturingScreenshot = false;
		if(OnCompleteScreenshotCapture != null) {
			OnCompleteScreenshotCapture(screenshot);
		}
	}
}