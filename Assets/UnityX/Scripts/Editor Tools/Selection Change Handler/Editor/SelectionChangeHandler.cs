﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Triggers events when key members of UnityEngine.Selection change.
/// Handy for editor scripting.
/// </summary>
[InitializeOnLoad]
class SelectionChangeHandler {
	
	private static Object activeObject;
	private static Object[] objects;
	
	public delegate void OnSelectionActiveObjectChangedEvent(Object oldObjects, Object newObjects);
	public static event OnSelectionActiveObjectChangedEvent OnSelectionActiveObjectChanged;
	
	public delegate void OnSelectionObjectsChangedEvent(Object[] oldObjects, Object[] newObjects);
	public static event OnSelectionObjectsChangedEvent OnSelectionObjectsChanged;
	
	static SelectionChangeHandler () {
		Selection.selectionChanged += OnSelectionChanged;
	}
	
	static void OnSelectionChanged () {
		if(activeObject == Selection.activeObject) {
			Object lastObject = activeObject;
			activeObject = lastObject;
			if(OnSelectionActiveObjectChanged != null) {
				OnSelectionActiveObjectChanged(activeObject, Selection.activeObject);
			}
		}
		
		if(!CollectionX.Equal(objects, Selection.objects)) {
			Object[] lastObjects = objects;
			CopySelectionIntoArray();
			if(OnSelectionObjectsChanged != null) {
				OnSelectionObjectsChanged(lastObjects, objects);
			}
		}
	}
	
	private static void CopySelectionIntoArray () {
		if(Selection.objects != null) {
			objects = new Object[Selection.objects.Length];
			System.Array.Copy(Selection.objects, objects, Selection.objects.Length);
		}
		else
			objects = null;
	}
}