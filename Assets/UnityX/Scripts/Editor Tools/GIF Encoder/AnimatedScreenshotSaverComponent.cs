﻿using GifEncoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class AnimatedScreenshotSaverComponent : MonoBehaviour
{
//	private string fileExtension = "gif";
	
	public KeyCode[] keycodes = new KeyCode[] {KeyCode.F5};
	public Camera[] cameras;
	public ScreenshotResolution resolution = new ScreenshotResolution("GIF", 320, 240);
	
	public bool capturing;
	public int framesPerSecond = 18;
	public int numFrames = 20;
	public int currentNumFrames = 0;
	private RenderTexture renderTexture;
	private AnimatedGifEncoder gifEncoder;
	
	private Timer frameTimer = new Timer();
	
	private void Awake () {
		frameTimer.OnComplete += HandleOnCompleteFrameTimer;
	}

	void HandleOnCompleteFrameTimer () {
		ScreenshotSaver.OnCompleteScreenshotCapture += OnCaptureScreenshot;
		ScreenshotSaver.CaptureScreenshot(resolution.width, resolution.height, cameras);
	}
	private void Update () {
		if(capturing) {
			frameTimer.Loop();
		} else {
			GetInput();
		}
		
	}
	
	private void GetInput () {
		if(keycodes.IsNullOrEmpty()) return;
		foreach(KeyCode keycode in keycodes) {
			if(Input.GetKeyDown(keycode)) {
				Capture ();
			}
		}
	}
	
	public void Capture() {
		capturing = true;
		currentNumFrames = 0;
		// Create a GIF encoder:
		gifEncoder = new AnimatedGifEncoder(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "output1.gif"));
		gifEncoder.SetDelay(1000 / framesPerSecond);
		frameTimer.Set (1f/framesPerSecond);
		ResetFrameTimer();
	}
	
	private void ResetFrameTimer () {
		frameTimer.Reset();
		frameTimer.Start();
	}
	
	private void OnCaptureScreenshot (Texture2D screenshot) {
		// Add the current frame to the GIF:
		gifEncoder.AddFrame(screenshot);
		
		// Destroy the temporary texture:
		UnityEngine.Object.Destroy(screenshot);
		
		currentNumFrames++;
		if (currentNumFrames >= numFrames) {
			gifEncoder.Finish();
			capturing = false;
		} else {
			ResetFrameTimer();
		}
		ScreenshotSaver.OnCompleteScreenshotCapture -= OnCaptureScreenshot;
	}
}
