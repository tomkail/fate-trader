﻿#if UNITY_EDITOR
//	http://wiki.unity3d.com/index.php/CreateScriptableObjectAsset2
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Reflection;
using System.Collections.Generic;

public static class ScriptableObjectUtility {

	/// <summary>
	//	Create new asset from <see cref="ScriptableObject"/> type using the OS save prompt.
	/// </summary>
	public static T CreateAssetWithSavePrompt<T> (string name = "", string path = "Assets") where T : ScriptableObject {
		T asset = ScriptableObject.CreateInstance<T> ();
		if(name.IsNullOrWhiteSpace()) 
			name = "New "+typeof(T).Name;
 
		path = EditorUtility.SaveFilePanelInProject("Save ScriptableObject", name+".asset", "asset", "Enter a file name for the ScriptableObject.", path);
		if (path != "") {
			AssetDatabase.CreateAsset (asset, path);
			AssetDatabase.SaveAssets ();
			AssetDatabase.Refresh();
			EditorUtility.FocusProjectWindow ();
			Selection.activeObject = asset;
			return asset;
		} else {
			MonoBehaviour.DestroyImmediate(asset);
			return null;
		}
	}

	/// Create new asset from <see cref="ScriptableObject"/> type with unique name at
	/// selected folder in project window. Asset creation can be cancelled by pressing
	/// escape key when asset is initially being named.
	/// </summary>
	/// <typeparam name="T">Type of scriptable object.</typeparam>
	/// <param name="_objectName">_object name.</param>
	public static T CreateAsset<T>(string _objectName = "") where T : ScriptableObject {
		var asset = ScriptableObject.CreateInstance<T>();
		if(_objectName.IsNullOrWhiteSpace()) _objectName = "New " + typeof(T).Name;
		ProjectWindowUtil.CreateAsset(asset, _objectName + ".asset");
		return asset;
	}
	
	/// Create new asset from <see cref="ScriptableObject"/> type with unique name at
	/// selected folder in project window. Asset creation can be cancelled by pressing
	/// escape key when asset is initially being named. This method uses reflection to find 
	/// </summary>
	/// <param name="_type">Type of scriptable object.</param>
	/// <param name="_objectName">_object name.</param>
	public static void CreateAssetUsingReflection(Type _type, string _objectName) {
		Type ex = typeof(ScriptableObjectUtility);
		MethodInfo mi = ex.GetMethod("CreateAsset");
		MethodInfo miConstructed = mi.MakeGenericMethod(_type);
		
		object[] args = {_objectName};
		miConstructed.Invoke(null, args);
	}
		
	[MenuItem("Assets/Create Asset From Script", false, 10000)]
	public static void CreateAssetMenuItem ()
	{
		ScriptableObject asset = ScriptableObject.CreateInstance (Selection.activeObject.name);
		AssetDatabase.CreateAsset (asset, String.Format ("Assets/{0}.asset", Selection.activeObject.name));
		EditorUtility.FocusProjectWindow ();
		Selection.activeObject = asset;
	}
	
	[MenuItem("Assets/Create Asset From Script", true, 10000)]
	public static bool ValidateCreateAssetMenuItem ()
	{
		if (Selection.activeObject == null || Selection.activeObject.GetType () != typeof(MonoScript))
			return false;
		MonoScript script = (MonoScript)Selection.activeObject;
		var scriptClass = script.GetClass ();
		if (scriptClass == null)
			return false;
//		return typeof(Manager).IsAssignableFrom (scriptClass.BaseType);
		return true;
	}
}

/*
using UnityEngine;

public class YourScriptableObject : ScriptableObject {
	
}
*/
#endif