﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class AnimationClipReverserWindow : EditorWindow  {
	
	Vector2 scrollPosition = new Vector2(0,0);
	
	private AnimationClip sourceClip;
	private string newClipName;
	
	[MenuItem("UnityX/Animation Clip/Reverse")]
	public static void Init() {
		AnimationClipReverserWindow window = EditorWindow.GetWindow<AnimationClipReverserWindow>("Reverse Animation Clip");
		window.Show();
		window.Focus();
	}
	
	void OnEnable () {
		if(Selection.activeObject != null && Selection.activeObject.IsTypeOf<AnimationClip>()) {
			sourceClip = (AnimationClip)Selection.activeObject;
			newClipName = sourceClip.name;
		}
	}
	
	void OnGUI() {
		scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
		
		EditorGUI.BeginChangeCheck();
		sourceClip = EditorGUILayout.ObjectField ("Source", sourceClip, typeof(AnimationClip), false) as AnimationClip;
		if(EditorGUI.EndChangeCheck()) {
			newClipName = sourceClip.name;
		}
		
		
		if(sourceClip == null) {
			EditorGUILayout.HelpBox("Assign an animation clip to reverse.", MessageType.None);
			GUI.enabled = false;
		}
		
		if (GUILayout.Button("Invert")) {
			AnimationClipX.ReverseAnimationClip(sourceClip);
			Selection.activeObject = sourceClip;
			EditorGUIUtility.PingObject(Selection.activeObject);
		}
		
		newClipName = EditorGUILayout.TextField("Cloned Clip Name", newClipName);
		if (GUILayout.Button("Clone and Invert")) {
			AnimationClip newClip = AnimationClipX.DuplicateAnimationClip(sourceClip, newClipName);
			AnimationClipX.ReverseAnimationClip(newClip);
			Selection.activeObject = newClip;
			EditorGUIUtility.PingObject(Selection.activeObject);
		}
		GUI.enabled = true;
		
		EditorGUILayout.EndScrollView();
	}
}