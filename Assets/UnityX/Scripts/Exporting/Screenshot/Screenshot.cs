﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Screenshot {
	public Texture2D screenshot;
}
