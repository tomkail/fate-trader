﻿using UnityEngine;
//using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public enum ScreenshotCaptureType {
	Single,
	Burst,
	Continuous
}

public class ScreenshotGenerator : MonoBehaviour {
	public static ScreenshotGenerator inst;
	
	public int defaultWidth = 1920;
	public int defaultHeight = 1080;

	public ScreenshotSet automaticSet;

	//private int numScreenshots = 0;
	private int numSets = 0;
	private int numBurstSets = 0;
	private int numContinuousSets = 0;
	public List<ScreenshotSet> screenshotSets;
	public ScreenshotSet currentScreenshotSet;
	
	public bool export = true;

	public static bool folderCreated = false;
	public static string folderRoot = "Screenshots";

	//Prevents out of memory crashes
	//private bool hitScreenshotCap = false;
	//private int maxScreenshots = 600;

	private static string startupTime;

	void Awake () {
		inst = this;
		startupTime = ((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000).ToString();
	}

	void Start () {
		screenshotSets = new List<ScreenshotSet>();
	}

	void LateUpdate () {
		//DebuggingControls();

		for(int i = 0; i < screenshotSets.Count; i++){
			screenshotSets[i].Loop();
		}
	}

	void OnApplicationQuit () {
		ExportImages();
	}

	public static string GetFolderRoot () {
		#if !UNITY_WINRT
		if(!folderCreated) {
			//Create root folder if it doesn't exist
			if (!Directory.Exists(Directory.GetCurrentDirectory()+"/"+ScreenshotGenerator.folderRoot)) {
				Directory.CreateDirectory(Directory.GetCurrentDirectory()+"/"+ScreenshotGenerator.folderRoot);
			}
			//Create this play folder if it doesn't exist
			if (!Directory.Exists(Directory.GetCurrentDirectory()+"/"+ScreenshotGenerator.folderRoot+"/"+startupTime)) {
				Directory.CreateDirectory(Directory.GetCurrentDirectory()+"/"+ScreenshotGenerator.folderRoot+"/"+startupTime);
			}

			folderCreated = true;
		}

		return Directory.GetCurrentDirectory()+"/"+ScreenshotGenerator.folderRoot+"/"+startupTime;
		#else
		return "ERROR. SCREENSHOTS NOT SUPPORTED ON WINDOWS RT";
		#endif
	}

	//[ContextMenu ("Single Photo")]
	public SingleScreenshotSet CreateSingleScreenshot () {
		SingleScreenshotSet newSet = gameObject.AddComponent<SingleScreenshotSet>() as SingleScreenshotSet;
		newSet.Init();
		newSet.Begin();
		screenshotSets.Add(newSet);
		SetCurrentScreenshotSet(newSet);
		numSets++;
		return newSet;

		/*
		CreateScreenshot();
		string imageName = "Screenshot "+(numScreenshots+1);
		byte[] bytes = screenshots[i].EncodeToPNG ();
		File.WriteAllBytes(ScreenshotGenerator.GetFolderRoot()+"/"+folderName+"/"+imageName+".png", bytes);
		numScreenshots++;
		*/
	}

	public BurstScreenshotSet CreateBurstScreenshotSet (float captureInterval, int burstQuantity, int width = 1920, int height = 1080) {
		BurstScreenshotSet newSet = gameObject.AddComponent<BurstScreenshotSet>() as BurstScreenshotSet;
		newSet.folderName = "Burst "+(numBurstSets+1);
		newSet.width = width;
		newSet.height = height;
		newSet.captureInterval = captureInterval;
		newSet.burstQuantity = burstQuantity;
		AddNewScreenshotSet(newSet);
		SetCurrentScreenshotSet(newSet);
		numBurstSets++;
		return newSet;
	}

	public ContinuousScreenshotSet CreateContinuousScreenshotSet (int frameRate, float captureTime, int width = 1920, int height = 1080) {
		ContinuousScreenshotSet newSet = gameObject.AddComponent<ContinuousScreenshotSet>() as ContinuousScreenshotSet;
		newSet.folderName = "Continuous "+(numContinuousSets+1);
		newSet.frameRate = frameRate;
		newSet.captureTime = captureTime;
		AddNewScreenshotSet(newSet);
		SetCurrentScreenshotSet(newSet);
		numContinuousSets++;
		return newSet;
	}

	public ContinuousScreenshotSet CreateContinuousScreenshotSet (int frameRate, int width = 1920, int height = 1080) {
		ContinuousScreenshotSet newSet = gameObject.AddComponent<ContinuousScreenshotSet>() as ContinuousScreenshotSet;
		newSet.folderName = "Continuous "+(numContinuousSets+1);
		newSet.frameRate = frameRate;
		newSet.captureForever = true;
		AddNewScreenshotSet(newSet);
		SetCurrentScreenshotSet(newSet);
		numContinuousSets++;
		return newSet;
	}

	public void AddNewScreenshotSet (ScreenshotSet newSet) {
		newSet.Init();
		newSet.Begin();
		screenshotSets.Add(newSet);
		numSets++;
	}

	private void SetCurrentScreenshotSet (ScreenshotSet newScreenshotSet) {
		if(currentScreenshotSet != null) {
			currentScreenshotSet.OnCapture -= FinishCurrentScreenshotSet;
		}
		currentScreenshotSet = newScreenshotSet;
		currentScreenshotSet.OnCapture += FinishCurrentScreenshotSet;
	}

	private void FinishCurrentScreenshotSet () {

		
		currentScreenshotSet = null;
	}

	[ContextMenu ("Export Now")]
	private void ExportImages(){
		if(export){
			for(int i = 0; i < screenshotSets.Count; i++){
				screenshotSets[i].ExportScreenshotSet();
				screenshotSets.RemoveAt(i);
				i--;
			}
		}
	}
}