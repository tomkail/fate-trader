﻿using UnityEngine;
using System.Collections;

public class BurstScreenshotSet : ScreenshotSet {
	//public bool capturing = false;
	public int burstQuantity = 6;
	public float captureInterval = 0.2f;

	//private float burstTime = 0;
	private float timer = 0;

	public override void Begin(){
		//burstTime = captureInterval * 6;
		CreateScreenshot();
		base.Begin();
	}

	public override void Loop(){
		if(!capturing)return;
		

		StartCoroutine(UpdateCR());

		base.Loop();
	}

	public IEnumerator UpdateCR(){
		timer += Time.deltaTime;
		int currentShot = MathX.FloorToInt(timer/captureInterval);
		if(currentShot > numScreenshots) {
			yield return StartCoroutine(CreateScreenshotCR());
			if(numScreenshots >= burstQuantity){
				EndScreenshotSet(exportWhenDone);
			}
		}
		yield return null;
	}

	public override void EndScreenshotSet (bool export) {
		base.EndScreenshotSet(export);
	}
}
