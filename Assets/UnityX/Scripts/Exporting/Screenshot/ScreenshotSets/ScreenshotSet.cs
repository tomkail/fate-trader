using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class ScreenshotSet : MonoBehaviour {
	public int width = 1920;
	public int height = 1080;
	
	public int numScreenshots = 0;
	public List<Texture2D> screenshots;
	private bool takingScreenshot = false;
	
	public string folderName = "";
	public bool capturing = false;
	public bool export = true;
	public bool exportWhenDone = true;
	public bool exported = false;
	public bool clampNumScreenshots = false;

	public delegate void OnFinishSetDelegate();
	public event OnFinishSetDelegate OnFinishSet;

	public delegate void OnPrepareCaptureDelegate();
	public event OnPrepareCaptureDelegate OnPrepareCapture;

	public delegate void OnCaptureDelegate();
	public event OnCaptureDelegate OnCapture;

	public delegate void OnExportDelegate();
	public event OnExportDelegate OnExport;

	public virtual void Init(){
		screenshots = new List<Texture2D>();
		capturing = true;
	}

	public virtual void Begin(){

	}

	public virtual void Loop(){
		
	}

	[ContextMenu("Export now")]
	public virtual bool ExportScreenshotSet(bool force = false){
		#if UNITY_EDITOR||UNITY_STANDALONE_WIN||UNITY_STANDALONE_OSX
		if(!export || screenshots.Count == 0 || (exported && !force)) return false;

		for(int i = 0; i < screenshots.Count; i++){
			string imageName = "screenshot "+(i+1);
			byte[] bytes = screenshots[i].EncodeToPNG ();

			if (!Directory.Exists(ScreenshotGenerator.GetFolderRoot()+"/"+folderName)) {
				Directory.CreateDirectory(ScreenshotGenerator.GetFolderRoot()+"/"+folderName);
			}

			File.WriteAllBytes(ScreenshotGenerator.GetFolderRoot()+"/"+folderName+"/"+imageName+".png", bytes);
		}

		exported = true;
		Debug.Log(screenshots.Count + " screenshots saved to "+(ScreenshotGenerator.GetFolderRoot()));
		screenshots = new List<Texture2D>();

		#endif
		if(OnExport != null) {
			OnExport();
		}
		return true;
	}

	public virtual void PauseScreenshotSet () {
		capturing = false;
	}

	public virtual void EndScreenshotSet (bool export) {
		capturing = false;
		if(export) {
			ExportScreenshotSet();
		}
		FinishSet();
	}

	public void DestroyScreenshotSet () {
		ClearScreenshots();
		export = false;
	}

	public virtual void ClearScreenshots(){
		screenshots = new List<Texture2D>();
	}

	public void CreateScreenshot(){
		StartCoroutine(CreateScreenshotCR());
	}

	

	public IEnumerator CreateScreenshotCR(){
		if(takingScreenshot) yield break;
		SetupScreenshotCapture();
		yield return new WaitForEndOfFrame();
		CaptureScreenshot();
		FinishScreenshotCapture();		
	}

	protected virtual void SetupScreenshotCapture () {
		takingScreenshot = true;
		if(OnPrepareCapture != null) {
			OnPrepareCapture();
		}
	}

	protected virtual void FinishSet () {
		if(OnFinishSet != null) {
			OnFinishSet();
		}
	}

	protected virtual void CaptureScreenshot () {
		Texture2D screenshot;
		if(Application.HasProLicense()){
			screenshot = CreateHighResolutionScreenshot();
		} else {
			screenshot = CreateStandardResolutionScreenshot();
		}
		screenshots.Add(screenshot);
		Destroy(screenshot);
		numScreenshots++;
	}

	protected Texture2D CreateHighResolutionScreenshot () {
		RenderTexture rt = new RenderTexture(width, height, 24);
		Camera.main.targetTexture = rt;
		Texture2D screenshot = new Texture2D(width, height, TextureFormat.RGB24, false);
		Camera.main.Render();
		RenderTexture.active = rt;
		screenshot.ReadPixels(new Rect(0,0, width, height), 0, 0);
		Camera.main.targetTexture = null;
		RenderTexture.active = null;
		MonoBehaviour.Destroy(rt);
		return screenshot;
	}

	protected Texture2D CreateStandardResolutionScreenshot () {
		Texture2D screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		screenshot.ReadPixels(new Rect(0,0,Screen.width,Screen.height), 0, 0 );
		return screenshot;
	}

	public virtual void FinishScreenshotCapture () {
		takingScreenshot = false;
		if(OnCapture != null) {
			OnCapture();
		}
	}
}
