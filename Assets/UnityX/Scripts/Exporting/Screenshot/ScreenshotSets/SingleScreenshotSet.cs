﻿using UnityEngine;
using System.Collections;

public class SingleScreenshotSet : ScreenshotSet {

	public override void Begin(){
		CreateScreenshot();
		base.Begin();
	}

	public override void FinishScreenshotCapture () {
		base.FinishScreenshotCapture();
		EndScreenshotSet(true);
	}
}
