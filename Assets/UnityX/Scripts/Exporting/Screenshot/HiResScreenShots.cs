using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
//Common resolutions 
//1280×720  (921,600)
//1920×1080 (2,073,600)
//2880×1800 (5,184,000)

public class HiResScreenShots : MonoBehaviour {
	public static HiResScreenShots inst;
	public List<Texture2D> screenshots;
	private string folderRoot = "Screenshots";
	private string folderName = "";

	public int resWidth = 1920;
	public int resHeight = 1080;
	
	public string ScreenshotFilePath() {
		#if !UNITY_WINRT
		if (!Directory.Exists(Directory.GetCurrentDirectory()+"/"+folderRoot)) {
			Directory.CreateDirectory(Directory.GetCurrentDirectory()+"/"+folderRoot);
		}

		return string.Format("{0}/{1}/Biome_{2}.png",
			Directory.GetCurrentDirectory(),
			folderRoot,
			System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
		#else
		return "ERROR. SCREENSHOTS NOT SUPPORTED ON WINDOWS RT";
		#endif
	}

	public delegate void OnPrepareEvent ();
   	public event OnPrepareEvent OnPrepare;

   	public delegate void OnCompleteEvent ();
   	public event OnCompleteEvent OnComplete;

	void Awake () {
		inst = this;
	}

	public string TakeLowResShot() {
		string path = ScreenshotFilePath();

		StartCoroutine(TakeLowResShotCoroutine(path));

		return path;
	}

	private IEnumerator TakeLowResShotCoroutine(string path){
		if(OnPrepare != null) OnPrepare();

		yield return new WaitForEndOfFrame();
		
		Rect captureRect = new Rect(0,0,Screen.width,Screen.height);
		Texture2D screenshot = new Texture2D((int)captureRect.width, (int)captureRect.height, TextureFormat.RGB24, false);
		screenshot.ReadPixels(captureRect, 0, 0 );
		screenshot.Apply();

		/*byte[] bytes = screenshot.EncodeToPNG ();
		folderRoot = folderName+"/"+((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000).ToString()+".png";
		File.WriteAllBytes(folderRoot, bytes);
		Destroy(screenshot);*/

		ExportImage(path, screenshot);

		yield return new WaitForEndOfFrame();

		//screenshots.Add(screenshot);
		
		if(OnComplete != null) OnComplete();
	}

	public string TakeHiResShot() {
		string path = ScreenshotFilePath();

		StartCoroutine(TakeHiResShotCoroutine(path));

		return path;
	}

	private IEnumerator TakeHiResShotCoroutine(string path){
		if(OnPrepare != null) OnPrepare();
		
		yield return new WaitForEndOfFrame();
		
		RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
		Camera.main.targetTexture = rt;
		Texture2D screenshot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
		Camera.main.Render();
		RenderTexture.active = rt;
		screenshot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
		Camera.main.targetTexture = null;
		RenderTexture.active = null; // JC: added to avoid errors
		Destroy(rt);
		ExportImage(path, screenshot);
		Destroy(screenshot);
		
		//screenshots.Add(screenshot);
		
		if(OnComplete != null) OnComplete();
	}

	private void ExportImage (string path, Texture2D texture) {
		byte[] bytes = texture.EncodeToPNG();
		File.WriteAllBytes(path, bytes);
	}

	public bool ExportImageSet(){
		CreateTimestampedDirectory();
	
		for(int i = 0; i < screenshots.Count; i++){
			string imageName = "screenshot "+(i+1);
			byte[] bytes = screenshots[i].EncodeToPNG ();
			File.WriteAllBytes(folderRoot+"/"+folderName+"/"+imageName+".png", bytes);
			//Destroy(screenshots[i]);
		}
		
		print(screenshots.Count + " hi res screenshots saved to "+(folderRoot+"/"+folderName));
		screenshots = new List<Texture2D>();
		return true;
		
	}

	private void CreateTimestampedDirectory(){
		CreateDirectory("HiRes_"+((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000).ToString());
	}

	private void CreateDirectory(string _folderName){
		#if UNITY_EDITOR||UNITY_STANDALONE_WIN||UNITY_STANDALONE_OSX
		folderName = _folderName;
		System.IO.Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory()+"/"+folderRoot+"/"+folderName);
		#endif
	}

	void OnApplicationQuit(){
		//ExportImageSet();
	}
}