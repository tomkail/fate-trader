﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CursorLocker))]
public class CursorLockerEditor : BaseEditor<CursorLocker> {
	
	public override void OnInspectorGUI () {
		base.OnInspectorGUI ();
		if(GUILayout.Button("Refresh")) {
			data.RefreshCursorLockState();
		}
	}
	
}
