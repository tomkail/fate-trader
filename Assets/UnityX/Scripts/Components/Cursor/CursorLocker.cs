﻿using UnityEngine;
using System.Collections;

public class CursorLocker : MonoBehaviour {
	
	public CursorLockMode cursorLockMode = CursorLockMode.Locked;
	
	public bool setOnAwake = true;
	public bool setOnUpdate = false;
	
	void Awake () {
		if(setOnAwake) {
			RefreshCursorLockState();
		}
	}
	
	void Update () {
		if(setOnUpdate) {
			RefreshCursorLockState();
		}
	}
	
	public void RefreshCursorLockState () {
		Cursor.lockState = cursorLockMode;
	}
}
