﻿using UnityEngine;
using System;
using System.Collections;

public class CoroutineHelper : MonoSingleton<CoroutineHelper> {
	// Protecting the constructor ensures this method can only be used as a singleton
	protected CoroutineHelper () {}
	
	/// <summary>
	/// Execute the specified coroutine.
	/// </summary>
	/// <param name="coroutine">Coroutine.</param>
	public static Coroutine Execute(IEnumerator coroutine) {
		return Instance.StartCoroutine(coroutine);
	}


	/// <summary>
	/// Execute the specified coroutines.
	/// </summary>
	/// <param name="coroutines">Coroutines.</param>
	//	CoroutineHelper.Execute(new IEnumerator[] {
	//		CoroutineHelper.WaitForSeconds(0.2f),
	//		GameController.Instance.historyManager.AddToSaveHistory()
	//	});
	public static void Execute(IEnumerator[] coroutines) {
		Instance.StartCoroutine(ExecuteCR(coroutines));
	}

	public static IEnumerator ExecuteCR(IEnumerator[] coroutines) {
		foreach(IEnumerator coroutine in coroutines)
			yield return Instance.StartCoroutine(coroutine);
	}

	public static IEnumerator WaitForSeconds (float delay) {
		yield return new WaitForSeconds(delay);
	}
	
	/// <summary>
	/// Delay the specified action and delay.
	/// </summary>
	/// <param name="action">Action.</param>
	/// <param name="delay">Delay.</param>
	
	// Can be used like this!
	//	CoroutineHelper.Delay(() => {
	//		
	//	}, 1.0f);
	// or
	//	CoroutineHelper.Delay(1.0f, Method());
	public static void Delay(Action action, float delay) {
		Execute(DelayCR(action, delay));
	}

	public static void DelayFrame(Action action, int numFrames = 1) {
		Execute(DelayFramesCR(action, numFrames));
	}

	private static IEnumerator DelayCR (Action action, float delay) {
		yield return new WaitForSeconds(delay);
		action();
	}
	
	private static IEnumerator DelayFramesCR (Action action, int numFrames) {
		for(int i = 0; i < numFrames; i++)
			yield return new WaitForEndOfFrame();
		action();
	}
	
	// delegate void DelayedMethod(params object[] objects); ?
}
