using UnityEngine;
using System.Collections;

public class TimeManager : MonoSingleton<TimeManager> {
	
	/// <summary>
	/// If the game is paused. Note that the time state can be paused while the game is not paused, although the game cannot be paused unless the time state is paused.
	/// </summary>
	[SerializeField]
	private static bool allowNegativeTime = false;
	public static bool AllowNegativeTime {
		get {
			return allowNegativeTime;
		}
		set {
			allowNegativeTime = value;
		}
	}
		
	/// <summary>
	/// The current time.
	/// </summary>
	[SerializeField]
	private static float time = 0;
	public static float Time {
		get {
			return time;
		} set {
			time = value;
			if(!allowNegativeTime && time <= 0){
				time = 0;
				if(State == TimeState.Rewinding){
					State = TimeState.Paused;
				}
			}
		}
	}
	
	
	/// <summary>
	/// The time scale.
	/// </summary>
	[SerializeField]
	private static float timeScale = 1;
	public static float TimeScale {
		get {
			return timeScale;
		} set {
//			if(timeScale == value) return;
			timeScale = value;
			UnityEngine.Time.timeScale = Mathf.Clamp(UnityEngine.Time.timeScale, 0,100);
//			UnityEngine.Time.fixedDeltaTime = 0.02f*TimeScale;
//			UnityEngine.Time.maximumDeltaTime = 0.333f*timeScale;  
			if(OnTimeScaleChange != null) OnTimeScaleChange(TimeScale);
			State = TimeManager.GetStateFromTimeScale(TimeScale, Time);
		}
	}
	
	/// <summary>
	/// The time scale tween.
	/// </summary>
	private static FloatTween timeScaleTween;
	public static FloatTween TimeScaleTween {
		get {
			if(timeScaleTween == null) timeScaleTween = new FloatTween();
			return timeScaleTween;
		} set {
			timeScaleTween = value;
		}
	}
	
	/// <summary>
	/// If the game is paused. Note that the time state can be paused while the game is not paused, although the game cannot be paused unless the time state is paused.
	/// </summary>
	[SerializeField]
	private static bool paused = false;
	public static bool Paused {
		get {
			return paused;
		}
		set {
			if(paused == value) return;
			if(value) {
				if(SavedTimeScale == null)
				SavedTimeScale = TimeScale;
				TimeScale = 0;
			} else {
				if(SavedTimeScale != null)
				TimeScale = (float)SavedTimeScale;
				SavedTimeScale = null;
			}
			paused = value;
			if(OnPauseStateChange != null) OnPauseStateChange(Paused);
		}
	}
	
	/// <summary>
	/// If sleeping, time is not updated.
	/// </summary>
	public static bool Sleeping {
		get {
			return sleepTimer != null;
		}
	}
	
	private static Timer sleepTimer;
	public static Timer SleepTimer {
		get {
			return sleepTimer;
		}
		set {
			if(value != null) {
				if(SavedTimeScale == null)
				SavedTimeScale = TimeScale;
				TimeScale = 0;
			} else {
				if(SavedTimeScale != null)
				TimeScale = (float)SavedTimeScale;
				SavedTimeScale = null;
			}
			sleepTimer = value;
		}
	}
	
	/// <summary>
	/// The saved time scale, used for sleep and pause.
	/// </summary>
	private static float? savedTimeScale;
	public static float? SavedTimeScale {
		get {
			return savedTimeScale;
		}
		set {
			savedTimeScale = value;
		}
	}
	
	/// <summary>
	/// The delta time.
	/// </summary>
	private static float deltaTime = 0;
	public static float DeltaTime {
		get {
			return deltaTime;
		}
		private set {
			deltaTime = value;
		}
	}
	
	/// <summary>
	/// The state of time.
	/// </summary>
	private static TimeState state = TimeState.Playing;
	public static TimeState State {
		get {
			return state;
		} set {
			if(State == value) return;
			TimeState oldState = State;
			state = value;
			if(OnTimeStateChange != null) OnTimeStateChange(oldState, State);
		}
	}
	
	
	public delegate void OnPauseStateChangeEvent(bool pauseState);
	public static event OnPauseStateChangeEvent OnPauseStateChange;
	
	public delegate void OnTimeScaleChangeEvent(float newTimeScale);
	public static event OnTimeScaleChangeEvent OnTimeScaleChange;
	
	public delegate void OnTimeStateChangeEvent(TimeState oldState, TimeState newState);
	public static event OnTimeStateChangeEvent OnTimeStateChange;
	
	
	public static void TogglePause () {
		Paused = !Paused;
	}
	
	/// <summary>
	/// Stops the clock for a specified length of time.
	/// </summary>
	/// <param name="sleepTime">Sleep time.</param>
	public static void Sleep (float sleepTime) {
		if(SleepTimer != null) TimeManager.DestroySleepTimer();
		SleepTimer = new Timer (sleepTime);
		SleepTimer.OnComplete += TimeManager.OnCompleteSleep;
		SleepTimer.Start ();
	}
	
	public static void TweenTimeScale (float targetTime, float length) {
		TimeScaleTween.Tween(targetTime, length);
	}
	
	public static void TweenTimeScale (float targetTime, float length, AnimationCurve curve) {
		TimeScaleTween.Tween(targetTime, length, curve);
	}
	
	/// <summary>
	/// Sets the state from time scale.
	/// </summary>
	/// <param name="_timeScale">_time scale.</param>
	/// <param name="_time">_time.</param>
	public static TimeState GetStateFromTimeScale(float _timeScale, float _time = Mathf.Infinity) {
		if(_timeScale > 0) {
			return TimeState.Playing;
		} else if(_timeScale < 0 && Time > 0) {
			return TimeState.Rewinding;
		} else {
			return TimeState.Paused;
		}
	}
	
	public new static string ToString () {
		return ("TimeController: State ["+TimeManager.State+"], " + "Paused ["+TimeManager.Paused+"], " + "Sleeping ["+TimeManager.Sleeping+"], " +(TimeManager.Sleeping?"Sleep Timer ["+TimeManager.SleepTimer.currentTime+"], ":"")+ "TimeScale ["+TimeManager.TimeScale+"], " + "DeltaTime ["+TimeManager.DeltaTime+"], " + "Time ["+TimeManager.Time+"].");
	}
	
	void Update () {
		DeltaTime = 0;
		if(!paused) {
			if(Sleeping) TimeManager.UpdateSleep();
			else TimeManager.UpdateTime();
		}
//		Debug();
	}
	
	private static void UpdateTime () {
		if(TimeScaleTween.tweening) TimeScale = TimeScaleTween.currentValue;
		
		DeltaTime = TimeScale * UnityEngine.Time.unscaledDeltaTime;
		// On the first frame, unscaledDeltaTime takes a long time to execute, which leads to a massive deltaTime. This fixes that.
		if(UnityEngine.Time.frameCount == 1) DeltaTime = TimeScale * UnityEngine.Time.deltaTime;
		Time += DeltaTime;
		
		timeScaleTween.Loop(UnityEngine.Time.unscaledDeltaTime);
		
	}
	
	private static void UpdateSleep () {
		SleepTimer.Loop (UnityEngine.Time.unscaledDeltaTime);
	}
	
	private static void OnCompleteSleep () {
		TimeManager.DestroySleepTimer();
	}
	
	private static void DestroySleepTimer () {
		SleepTimer.OnComplete -= OnCompleteSleep;
		SleepTimer = null;
	}	
	
	private void Debug () {
		if(!Paused) {
			if(Input.GetKeyDown("s")) {
				Sleep (0.5f);
			} else if(Input.GetKey("left")) {
				TimeScale -= UnityEngine.Time.deltaTime;
			} else if(Input.GetKey("right")) {
				TimeScale += UnityEngine.Time.deltaTime;
			} else if(Input.GetKeyDown("down")) {
				TimeScale = 0;
			} else if(Input.GetKeyDown("up")) {
				TimeScale = 1;
			}
		}
		
		if(Input.GetKeyDown("p")) {
			TimeManager.TogglePause ();
		}
	}
}