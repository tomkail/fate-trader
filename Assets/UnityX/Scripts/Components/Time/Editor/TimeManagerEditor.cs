﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(TimeManager))]
public class TimeManagerEditor : Editor {
	
	public override void OnInspectorGUI() {
		if(Application.isPlaying) {
			EditorGUILayout.LabelField("State", TimeManager.State.ToString());
			EditorGUILayout.LabelField("Time", TimeManager.Time.ToString());
			
			TimeManager.TimeScale = EditorGUILayout.FloatField("Time Scale", TimeManager.TimeScale);
			if(TimeManager.TimeScale > 100)
				EditorGUILayout.HelpBox("Unity's Time.timeScale only supports values from 0 to 100. Physics might not perform as expected when TimeScale exceeds 100.", MessageType.Warning);
			
			TimeManager.AllowNegativeTime = EditorGUILayout.Toggle("Allow Negative Time", TimeManager.AllowNegativeTime);
			if(TimeManager.Time == 0 && TimeManager.State == TimeState.Rewinding) {
				EditorGUILayout.HelpBox("Can't rewind when time is 0 when allowNegativeTime is set false.", MessageType.Info);
			}
			
			if(GUILayout.Button(TimeManager.Paused ? "Play" : "Pause")) {
				TimeManager.TogglePause();
			}
		} else {
			EditorGUILayout.HelpBox("TimeManager inspector is only visible in Play Mode.", MessageType.Info);
		}
		 
		EditorUtility.SetDirty(target);
	}
}
