﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode, DisallowMultipleComponent]
public class LockTransform : MonoBehaviour {
	public bool hideHandles = true;
//	public bool position = true;
//	public bool rotation = true;
//	public bool scale = true;
	
	void Update () {
		#if UNITY_EDITOR
		ComponentMenuX.MoveToTop(this);
		#endif
		if(!transform.IsDefault())
			transform.ResetTransform();
	}
}
