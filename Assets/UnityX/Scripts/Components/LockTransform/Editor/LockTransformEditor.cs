﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(LockTransform))]
public class LockTransformEditor : BaseEditor<LockTransform> {
	
	public override void OnInspectorGUI () {
		serializedObject.Update();
		
		EditorGUILayout.PropertyField(serializedObject.FindProperty("hideHandles"));
		
//		EditorGUILayout.PropertyField(serializedObject.FindProperty("position"));
//		EditorGUILayout.PropertyField(serializedObject.FindProperty("rotation"));
//		EditorGUILayout.PropertyField(serializedObject.FindProperty("scale"));
//		if(serializedObject.FindProperty("position").boolValue && serializedObject.FindProperty("rotation").boolValue && serializedObject.FindProperty("scale").boolValue) {
//			EditorGUILayout.PropertyField(serializedObject.FindProperty("heightFromFloor"));
//		}
		
		serializedObject.ApplyModifiedProperties();
		HideTools(data != null ? (data.enabled && data.hideHandles) : false);
	}
	
	public override void OnEnable() {
		base.OnEnable();
		HideTools(data.enabled);
	}
	
	void OnDisable() {
		HideTools(false);
	}
	
	private void HideTools (bool hideTools = true) {
		Tools.hidden = hideTools;
	}
	
	/*
	private void DrawIcon(GameObject gameObject, int idx)
	{
//		var largeIcons = GetTextures("sv_label_", string.Empty, 0, 8);
//		var largeIcons = GetTextures("LockTransform Icon", string.Empty, 0, 1);
		GUIContent[] largeIcons = new GUIContent[] {new GUIContent("LockTransform Icon")};
		var icon = largeIcons[idx];
		var egu = typeof(EditorGUIUtility);
		var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
		var args = new object[] { gameObject, icon.image };
		var setIcon = egu.GetMethod("SetIconForObject", flags, null, new Type[]{typeof(UnityEngine.Object), typeof(Texture2D)}, null);
		setIcon.Invoke(null, args);
	}
	
	private GUIContent[] GetTextures(string baseName, string postFix, int startIndex, int count)
	{
		GUIContent[] array = new GUIContent[count];
		for (int i = 0; i < count; i++)
		{
			array[i] = EditorGUIUtility.IconContent(baseName + (startIndex + i) + postFix);
		}
		return array;
	}
	*/
}
