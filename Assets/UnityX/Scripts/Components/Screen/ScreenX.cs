//This class provides functionality for converting between pixel and percentage values, designed for GUI
using UnityEngine;
using System.Collections;

/// <summary>
/// Manages screen properties. Must be attached to a GameObject to function.
/// </summary>
public class ScreenX : PersistentMonoSingleton<ScreenX> {
	
	public const float inchesToCentimeters = 2.54f;
	public const float centimetersToInches = 0.39370079f;
	
	/// <summary>
	/// Is full screen allowed?
	/// </summary>
	public static bool enableFullScreen = true;

	/// <summary>
	/// The length from the bottom-left to the top-right corners. 
	/// Note: measured in pixels, rather than the standard screen diagonal unit of inches.
	/// </summary>
	public static float diagonal {
		get {
			return screen.diagonal;
		}
	}

	/// <summary>
	/// The total area of the screen
	/// </summary>
	public static float area {
		get {
			return screen.area;
		}
	}

	/// <summary>
	/// The aspect ratio
	/// </summary>
	public static float aspectRatio {
		get {
			return screen.aspectRatio;
		}
	}

	/// <summary>
	/// The reciprocal of the screen width
	/// </summary>
	public static float widthReciprocal {
		get {
			return screen.widthReciprocal;
		}
	}

	/// <summary>
	/// The reciprocal of the screen height
	/// </summary>
	public static float heightReciprocal {
		get {
			return screen.heightReciprocal;
		}
	}

	/// <summary>
	/// The reciprocal of the screen size
	/// Note: measured in pixels, rather than the standard screen diagonal unit of inches.
	/// </summary>
	public static float diagonalReciprocal {
		get {
			return screen.diagonalReciprocal;
		}
	}

	/// <summary>
	/// The inverted aspect ratio
	/// </summary>
	public static float aspectRatioReciprocal {
		get {
			return screen.aspectRatioReciprocal;
		}
	}
	
	/// <summary>
	/// The size of the screen as a Vector.
	/// </summary>
	/// <value>The size.</value>
	public static Vector2 size {
		get {
			return screen.size;
		}
	}
	
	/// <summary>
	/// The center of the screen
	/// </summary>
	/// <value>The center.</value>
	public static Vector2 center {
		get {
			return screen.center;
		}
	}
	
	/// <summary>
	/// Gets the screen rect.
	/// </summary>
	/// <value>The screen rect.</value>
	public static Rect screenRect {
		get {
			return screen.rect;
		}
	}
	
	
	/// <summary>
	/// Is device DPI unavailiable? (as it is on many devices)
	/// </summary>
	public static bool usingDefaultDPI {
		get {
			return Screen.dpi == 0;
		}
	}
	
	/// <summary>
	/// The default DPI to use in the case of default DPI
	/// </summary>
	public const int defaultDPI = 166;
	
	/// <summary>
	/// Use an override for DPI.
	/// </summary>
	public static bool usingCustomDPI;
	public static int customDPI = defaultDPI;
	
	/// <summary>
	/// The DPI of the screen
	/// </summary>
	public static float dpi {
		get {
			if(usingCustomDPI){
				return customDPI;
			} else if(usingDefaultDPI){
				return defaultDPI;
			} else {
				return Screen.dpi;
			}
		}
	}
	
	/// <summary>
	/// The orientation of the screen last time the size was changed. Used for measuring screen size change.
	/// </summary>
	private ScreenOrientation lastScreenOrientation;
	
	/// <summary>
	/// Event called when the screen size is changed
	/// </summary>
	public delegate void OnScreenSizeChangeEvent();
	public static event OnScreenSizeChangeEvent OnScreenSizeChange;

	/// <summary>
	/// Event called when the orientation is changed
	/// </summary>
	public delegate void OnOrientationChangeEvent();
	public static event OnOrientationChangeEvent OnOrientationChange;

	public static ScreenProperties screen = new ScreenProperties();
	public static ScreenProperties viewport = new ScreenProperties();
	public static ScreenProperties inches = new ScreenProperties();
	public static ScreenProperties centimeters = new ScreenProperties();
	
	/// <summary>
	/// The width of the screen last time the size was changed. Used for measuring screen size change.
	/// </summary>
	private static int lastWidth;
	
	/// <summary>
	/// The height of the screen last time the size was changed. Used for measuring screen size change.
	/// </summary>
	private static int lastHeight;
	
	public override void Init () {
		StoreWidthAndHeight();
		CalculateScreenSizeProperties();
		lastScreenOrientation = Screen.orientation;
	}

	public void Loop () {
		if(enableFullScreen && Input.GetKeyDown("f")){
			Screen.fullScreen = !Screen.fullScreen;
		}
		CheckSizeChange();
		CheckOrientationChange();
	}
	

	public static Vector2 ScreenToViewport(Vector2 screen){
		return new Vector2(screen.x / Screen.width, screen.y / Screen.height);
	}
	
	public static Vector2 ScreenToInches(Vector2 screen){
		return new Vector2(screen.x / dpi, screen.y / dpi);
	}
	
	public static Vector2 ScreenToCentimeters(Vector2 screen){
		return InchesToCentimeters(ScreenToInches(screen));
	}
	
	
	public static Vector2 ViewportToScreen(Vector2 viewport){
		return new Vector2(viewport.x * Screen.width, viewport.y * Screen.height);
	}
	
	public static Vector2 ViewportToInches(Vector2 viewport){
		return ScreenToInches(ViewportToScreen(viewport));
	}
	
	public static Vector2 ViewportToCentimeters(Vector2 viewport){
		return ScreenToCentimeters(ViewportToScreen(viewport));
	}
	
	
	public static Vector2 InchesToScreen(Vector2 inches){
		return new Vector2(inches.x * dpi, inches.y * dpi);
	}
	
	public static Vector2 InchesToViewport(Vector2 inches){
		return ScreenToViewport(InchesToScreen(inches));
	}
	
	public static Vector2 InchesToCentimeters(Vector2 inches){
		return inches * inchesToCentimeters;
	}
	
	
	public static Vector2 CentimetersToScreen(Vector2 centimeters){
		return InchesToScreen(CentimetersToInches(centimeters));
	}
	
	public static Vector2 CentimetersToViewport(Vector2 centimeters){
		return InchesToViewport(CentimetersToInches(centimeters));
	}
	
	public static Vector2 CentimetersToInches(Vector2 centimeters){
		return centimetersToInches * centimeters;
	}
	
	
	public static Vector2 GetScreenScaleAtDistance (float distance) {
		return ScreenX.GetScreenScaleAtDistance(distance, new Rect(0, 0, 1, 1));
	}

	public static Vector2 GetScreenScaleAtDistance (float distance, Rect screenRect) {
		return ScreenX.GetScreenScaleAtDistance(distance, screenRect, Camera.main);
	}

	public static Vector2 GetScreenScaleAtDistance (float distance, Rect screenRect, Camera camera) {
		Vector3 worldTopLeft = camera.ViewportToWorldPoint(new Vector3(screenRect.x,((1-screenRect.y)-screenRect.height),distance));
		Vector3 worldBottomRight = camera.ViewportToWorldPoint(new Vector3(screenRect.x+screenRect.width,((1-screenRect.y)-screenRect.height)+screenRect.height,distance));
		return new Vector2((worldBottomRight.x-worldTopLeft.x).CeilTo(2), (worldBottomRight.y-worldTopLeft.y).CeilTo(2));
	}

	private void CheckSizeChange() {
		if(Screen.width != lastWidth || Screen.height != lastHeight){
			StoreWidthAndHeight();
			CalculateScreenSizeProperties();
			if(OnScreenSizeChange != null){
				OnScreenSizeChange();
			}
		}
	}
	
	private void CheckOrientationChange(){
		if(Screen.orientation != lastScreenOrientation){
			lastScreenOrientation = Screen.orientation;
			if(OnOrientationChange != null){
				OnOrientationChange();
			}
			StartCoroutine(WaitForOrientationChangeAndCalculateScreen());
		}
	}
	
	//Orentation is stagged on android devices I've tested on. Wait a tiny amount of time before getting screen size.
	private IEnumerator WaitForOrientationChangeAndCalculateScreen(){
		yield return new WaitForSeconds (0.05f);
		StoreWidthAndHeight();
		CalculateScreenSizeProperties();
		if(OnScreenSizeChange != null){
			OnScreenSizeChange();
		}
	}
	
	public static void CalculateScreenSizeProperties () {
		screen.CalculateScreenSizeProperties(Screen.width, Screen.height);
		viewport.CalculateScreenSizeProperties(1, 1);
		inches.CalculateScreenSizeProperties(ViewportToInches(Vector2.one));
		centimeters.CalculateScreenSizeProperties(ViewportToCentimeters(Vector2.one));
	}
	
	private static void StoreWidthAndHeight () {
		lastWidth = Screen.width;
		lastHeight = Screen.height;
	}
}


/// <summary>
/// Unitless screen properties. 
/// Can be used to store screen properties in various unit types (Screen, Viewport, Inches)
/// </summary>
[System.Serializable]
public class ScreenProperties {
	
	/// <summary>
	/// The width of the screen last time the size was changed. Used for measuring screen size change.
	/// </summary>
	public float width {
		get; private set;
	}
	
	/// <summary>
	/// The height of the screen last time the size was changed. Used for measuring screen size change.
	/// </summary>
	public float height {
		get; private set;
	}
	
	/// <summary>
	/// The length from the bottom-left to the top-right corners. 
	/// Note: measured in pixels, rather than the standard screen diagonal unit of inches.
	/// </summary>
	public float diagonal {
		get; private set;
	}
	
	/// <summary>
	/// The total area of the screen
	/// </summary>
	public float area {
		get; private set;
	}
	
	/// <summary>
	/// The aspect ratio
	/// </summary>
	public float aspectRatio {
		get; private set;
	}
	
	/// <summary>
	/// The reciprocal of the screen width
	/// </summary>
	public float widthReciprocal {
		get; private set;
	}
	
	/// <summary>
	/// The reciprocal of the screen height
	/// </summary>
	public float heightReciprocal {
		get; private set;
	}
	
	/// <summary>
	/// The reciprocal of the screen size
	/// Note: measured in pixels, rather than the standard screen diagonal unit of inches.
	/// </summary>
	public float diagonalReciprocal {
		get; private set;
	}
	
	/// <summary>
	/// The inverted aspect ratio
	/// </summary>
	public float aspectRatioReciprocal {
		get; private set;
	}
	
	/// <summary>
	/// The size of the screen as a Vector.
	/// </summary>
	/// <value>The size.</value>
	public Vector2 size {
		get {
			return new Vector2(width, height);
		}
	}
	
	/// <summary>
	/// The center of the screen
	/// </summary>
	/// <value>The center.</value>
	public Vector2 center {
		get {
			return new Vector2(width * 0.5f, height * 0.5f);
		}
	}
	
	/// <summary>
	/// Gets the screen rect.
	/// </summary>
	/// <value>The screen rect.</value>
	public Rect rect {
		get {
			return new Rect(0, 0, width, height);
		}
	}
	
	public void CalculateScreenSizeProperties (Vector2 size){
		CalculateScreenSizeProperties(size.x, size.y);
	}
	
	public void CalculateScreenSizeProperties (float width, float height){
//		if(this.width == width && this.height == height) return;
		this.width = width;
		this.height = height;
		CalculateDiagonal();
		CalculateArea();
		CalculateAspectRatio();
		CalculateReciprocals();
	}
	
	private void CalculateDiagonal () {
		diagonal = MathX.Pythagoras(width, height);
	}
	
	private void CalculateArea () {
		area = width * height;
	}
	
	private void CalculateAspectRatio () {
		aspectRatio = (float)width/height;
	}
	
	private void CalculateReciprocals () {
		widthReciprocal = 1f/width;
		heightReciprocal = 1f/height;
		diagonalReciprocal = 1f/diagonal;
		aspectRatioReciprocal = 1f/aspectRatio;
	}
	
	public override string ToString () {
		return string.Format("Width: {0}\nHeight: {1}\nDiagonal: {2}\nArea: {3}\nAspect Ratio: {4}", width.RoundToSig(5), height.RoundToSig(5), diagonal.RoundToSig(4), area.RoundToSig(4), aspectRatio.RoundToSig(4));
	}	
}