﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraFacingBillboard : MonoBehaviour {

	public bool updateInGame = true;
	public bool updateInEditor = true;

	public enum BillboardMode {
		CameraForward,
		CameraTargetDirection
	}
	public BillboardMode mode;
	
	public bool useMainCamera = true;
	public new Camera camera;
	
	[Vector3Toggle]
	public Vector3 axis;
	public bool smooth = false;
	public int damping = 4;
	
	
	private void Update() {
		if(!updateInEditor || Application.isPlaying) return;
		FaceCamera();
	}
	
	private void LateUpdate() {
		if(!updateInGame || !Application.isPlaying) return;
		FaceCamera();
	}
	
	public void FaceCamera() {
		Camera _camera = camera;
		if(useMainCamera) _camera = Camera.main;
		FaceCamera(_camera);
	}
	
	public void FaceCamera(Camera _camera) {
		if(!enabled || _camera == null || axis == Vector3.zero) return;
		Vector3 direction = Vector3.zero;
		if(mode == BillboardMode.CameraForward) {
			direction = _camera.transform.forward;
		} else if(mode == BillboardMode.CameraTargetDirection) {
			direction = transform.position - _camera.transform.position;
		}
		if(direction == Vector3.zero) return;
		if(axis != Vector3.one) 
			direction = Vector3.Scale(direction, new Vector3(1f-axis.x, 1f-axis.y, 1f-axis.z));
		Quaternion newRotation = Quaternion.LookRotation(direction);
		if(Application.isPlaying && smooth) {
			transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 8);
		} else {
			transform.rotation = newRotation;
		}
	}
}