﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CameraFacingBillboard))]
[CanEditMultipleObjects]
public class CameraFacingBillboardEditor : BaseEditor<CameraFacingBillboard> {
	
	public override void OnInspectorGUI () {
		serializedObject.Update();

		EditorGUILayout.PropertyField(serializedObject.FindProperty("updateInGame"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("updateInEditor"));

		EditorGUILayout.PropertyField(serializedObject.FindProperty("mode"));
		
		EditorGUILayout.PropertyField(serializedObject.FindProperty("useMainCamera"));
		if(!serializedObject.FindProperty("useMainCamera").boolValue) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("camera"));
		}
		
		EditorGUILayout.PropertyField(serializedObject.FindProperty("axis"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("smooth"));
		if(serializedObject.FindProperty("smooth").boolValue) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("damping"));
		}
		
		serializedObject.ApplyModifiedProperties();
	}
}
