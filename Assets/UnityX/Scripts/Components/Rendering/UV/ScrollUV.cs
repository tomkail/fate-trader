﻿using UnityEngine;
using System.Collections;

public class ScrollUV : MonoBehaviour {
	
	public Vector2 offsetChangeSpeed;
	private new Renderer renderer;
	
	void Awake () {
		renderer = GetComponent<Renderer>();
		
	}
	
	void Update () {
		Vector2 offset = renderer.sharedMaterial.GetTextureOffset ("_MainTex");
		offset += offsetChangeSpeed;
		renderer.sharedMaterial.SetTextureOffset ("_MainTex", offset);
	}
}