﻿using UnityEngine;
using System.Collections;

public class SpriteSheetUV : MonoBehaviour {
	
	public bool randomizeIndexOnStart = false;
	public int index;
	
		//vars for the whole sheet
	public int colCount =  4;
	public int rowCount =  4;
	 
	//vars for animation
//	public int  rowNumber  =  0; //Zero Indexed
//	public int colNumber = 0; //Zero Indexed
	public int totalCells = 4;
	public int  fps = 10;
	  //Maybe this should be a private var
	private Vector2 offset;
	private new Renderer renderer;
	private float timer = 0;
	
	void Awake () {
		renderer = GetComponent<Renderer>();
		
		if(randomizeIndexOnStart) {
			index = Random.Range(0,totalCells);
		}
	}
	
	//Update
	void Update () {
		SetSpriteAnimation();  
	}
	 
	//SetSpriteAnimation
	void SetSpriteAnimation(){
		timer += Time.deltaTime * fps;
		while(timer > 1) {
			timer = timer - 1;
			index = (index + 1) % totalCells;
		}
		
		Refresh ();
	}
	
	void Refresh () {
		// Size of every cell
		float sizeX = 1.0f / colCount;
		float sizeY = 1.0f / rowCount;
		Vector2 size =  new Vector2(sizeX,sizeY);
		
		// split into horizontal and vertical index
		var uIndex = index % colCount;
		var vIndex = index / colCount;
		
		// build offset
		// v coordinate is the bottom of the image in opengl so we need to invert.
		float offsetX = uIndex * size.x;
		float offsetY = (1.0f - size.y) - vIndex * size.y;
		Vector2 offset = new Vector2(offsetX, offsetY);
		
		renderer.sharedMaterial.SetTextureOffset ("_MainTex", offset);
		renderer.sharedMaterial.SetTextureScale  ("_MainTex", size);
	}
}