﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SpriteFlipper : MonoBehaviour {
	[Vector2Toggle]
	public Vector2 flip;
	public bool flipX {
		get {
			return flip.x == 1;
		} set {
			flip.x = (value == true) ? 1 : 0;
		}
	}
	public bool flipY {
		get {
			return flip.y == 1;
		} set {
			flip.y = (value == true) ? 1 : 0;
		}
	}
	
	private void Update () {
		RefreshFlip();
	}
	
	public void RefreshFlip () {
		Vector2 absScale = transform.localScale.Abs();
		transform.localScale = new Vector3(flip.x == 1 ? -absScale.x : absScale.x, flip.y == 1 ? -absScale.y : absScale.y, transform.localScale.z);
	}
}
