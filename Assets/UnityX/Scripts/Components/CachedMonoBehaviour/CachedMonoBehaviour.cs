﻿using UnityEngine;
using System.Collections;

public class CachedMonoBehaviour : MonoBehaviour {
	[HideInInspector]
	public new Transform transform;
	[HideInInspector]
	public new Renderer renderer;
	
	public virtual void Init () {
		CacheComponents();
	}
	
	public virtual void CacheComponents () {
		transform = GetComponent<Transform>();
		renderer = GetComponent<Renderer>();
	}
}
