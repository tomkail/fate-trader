﻿using UnityEngine;
using System;
using System.Collections;

public class GetMiscScript : MonoBehaviour {
	public string componentName = "Material";

	[ContextMenu("Find GameObjects containing component.")]
	private void PrintComponentGameObjects () {

		Type _type = Type.GetType(componentName);
		Component[] components = FindObjectsOfType(_type) as Component[];

		print("Printing "+components.Length+" gameObjects containing components of name "+componentName);

        for (int i = 0; i < components.Length; i++) {
            print((i+1)+") "+components[i].gameObject.name +" ("+components[i].GetType()+")");
        }

        print("Finished finding components on gameObject.");
	}
}
