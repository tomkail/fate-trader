﻿using UnityEngine;

public class DetectLeaks : MonoBehaviour {
    public bool showGUI = true;
    public int numObjects;
    public int numGameObjects;
    public int numMaterials;
    public int numTextures;
    public int numSounds;
    public int numMesh;

    void Awake(){

    }

    void Update(){
        numObjects = Resources.FindObjectsOfTypeAll(typeof(Object)).Length;
        numGameObjects = Resources.FindObjectsOfTypeAll(typeof(GameObject)).Length;
        numMaterials = Resources.FindObjectsOfTypeAll(typeof(Material)).Length;
        numTextures = Resources.FindObjectsOfTypeAll(typeof(Texture)).Length;
        numSounds = Resources.FindObjectsOfTypeAll(typeof(AudioClip)).Length;
        numMesh = Resources.FindObjectsOfTypeAll(typeof(Mesh)).Length;
    }

    void OnGUI() {
        if(!showGUI)return;
        
        if (GUILayout.Button("Unload Unused Assets")) {
            Resources.UnloadUnusedAssets();
        }

        if (GUILayout.Button("Mono Garbage Collect")) {
            System.GC.Collect();
        }

        if (GUILayout.Button("List Loaded Textures"))
        {
            ListLoadedTextures();
        }

        if (GUILayout.Button("List Loaded Skins"))
        {
            ListLoadedSkins();
        }

        if (GUILayout.Button("List Loaded Styles"))
        {
            ListLoadedStyles();
        }

        if (GUILayout.Button("List Loaded Sounds"))
        {
            ListLoadedAudio();
        }

        if (GUILayout.Button("List Loaded GameObjects"))
        {
            ListLoadedGameObjects();
        }
    }

    private void ListLoadedTextures()
    {
        Object[] textures = Resources.FindObjectsOfTypeAll(typeof(Texture));

        string list = string.Empty;

        for (int i = 0; i < textures.Length; i++)
        {
            if (textures[i].name == string.Empty)
            {
                continue;
            }

            list += (i.ToString() + ". " + textures[i].name + "\n");

            if (i == 500)
            {
                Debug.Log(list);
                list = string.Empty;
            }
        }

        Debug.Log(list);
    }

    private void ListLoadedSkins()
    {
        Object[] skins = Resources.FindObjectsOfTypeAll(typeof(GUISkin));

        string list = string.Empty;

        for (int i = 0; i < skins.Length; i++)
        {
            if (skins[i].name == string.Empty)
            {
                continue;
            }

            list += (i.ToString() + ". " + skins[i].name + "\n");

            if (i == 500)
            {
                Debug.Log(list);
                list = string.Empty;
            }
        }

        Debug.Log(list);
    }

    private void ListLoadedStyles()
    {
        Object[] styles = Resources.FindObjectsOfTypeAll(typeof(GUIStyle));

        string list = string.Empty;

        for (int i = 0; i < styles.Length; i++)
        {
            if (styles[i].name == string.Empty)
            {
                continue;
            }

            list += (i.ToString() + ". " + styles[i].name + "\n");

            if (i == 500)
            {
                Debug.Log(list);
                list = string.Empty;
            }
        }

        Debug.Log(list);
    }

    private void ListLoadedAudio()
    {
        Object[] sounds = Resources.FindObjectsOfTypeAll(typeof(AudioClip));

        string list = string.Empty;

        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == string.Empty)
            {
                continue;
            }
            list += (i.ToString() + ". " + sounds[i].name + "\n");
        }

        Debug.Log(list);
    }

    private void ListLoadedGameObjects()
    {
        Object[] gos = Resources.FindObjectsOfTypeAll(typeof(GameObject));

        string list = string.Empty;

        for (int i = 0; i < gos.Length; i++)
        {
            if (gos[i].name == string.Empty)
            {
                continue;
            }
            list += (i.ToString() + ". " + gos[i].name + "\n");
        }

        Debug.Log(list);
    }
}
