﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadSceneIfMissing : MonoBehaviour {
	[SceneNameAttribute]
	public string sceneName;

	// This has to run on start because scenes are not all considered loaded until Awake has been called.
	private void Start () {
		if(gameObject.scene.name == sceneName) {
			Debug.LogError("Could not load scene because it is the same as the active scene.");
			return;
		}
		Scene scene = SceneManager.GetSceneByName(sceneName);
		if(!scene.IsValid()) {
			SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
			DebugX.Log(this, "Loaded scene '"+sceneName+"' as it was required by "+transform.HierarchyPath()+" but not loaded.");
		}
	}
}
