//Scales the orthographic size by width or height to fit orthographic size and a set screen ratio

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class AspectRatioCamera : MonoBehaviour {
	public new Camera camera {
		get {
			Camera _camera = gameObject.GetComponent<Camera>();
			if(_camera == null) Debug.Log("Camera not found");
			return _camera;
		}
	}

	[SerializeField, SetPropertyAttribute("aspectRatio")]
	private Vector2 _aspectRatio = new Vector2(4,3);
	public Vector2 aspectRatio {
		get {
			return _aspectRatio;
		} set {
			_aspectRatio = value;
			Update();
		}
	}

	[SerializeField, SetPropertyAttribute("size")]
	private float _size;
	public float size {
		get {
			return _size;
		} set {
			_size = value;
			Update();
		}
	}

	private void Update () {
	    float targetAspect = aspectRatio.x/aspectRatio.y;
	    // determine the game window's current aspect ratio
		float aspect = (float)camera.pixelRect.width / (float)camera.pixelRect.height;
	    // current viewport height should be scaled by this amount
	    float scaleHeight = aspect / targetAspect;
	    // if scaled height is less than current height, letterbox screen size
	    
	    if (scaleHeight < 1.0f) {
			if(camera.orthographic) {
				camera.orthographicSize = size / scaleHeight;
	    	} else {
				transform.position = transform.position.SetZ(-((size * 0.5f) / scaleHeight) / Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad));
	    	}
	    } else {
			if(camera.orthographic) {
				camera.orthographicSize = size;
			} else {
				transform.position = transform.position.SetZ(-(size * 0.5f) / Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad));
			}
	    }
	}

	// void OnDrawGizmos () {
	//     Rect rect = camera.ViewportToWorldRect(new Rect(0,0,1,1), 0);
	// 	Gizmos.color = Color.red;
	// 	GizmosX.DrawWireCube(rect,0);
	// }
}
