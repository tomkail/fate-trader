//From https://www.youtube.com/watch?v=HM17mAmLd7k#t=2617

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PixelScaleWidthCamera : MonoBehaviour {

	public int targetWidth = 640;
	public float pixelsToUnits = 100;
	public float zoom = 1;

	void Update () {
		int height = Mathf.RoundToInt(targetWidth / (float)Screen.width * Screen.height);
		GetComponent<Camera>().orthographicSize = height / pixelsToUnits / 2 / zoom;
	}
}
