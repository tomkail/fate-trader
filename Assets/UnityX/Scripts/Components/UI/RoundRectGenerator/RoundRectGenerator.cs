using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Image))]
public class RoundRectGenerator : MonoBehaviour {	
	private Image image;
	
	public Color color = Color.white;
	
	public bool automatic = true;
	
	public int width;
	public int height;
	public int radius;
	
	public bool outline;
	public Color outlineColor = Color.black;
	
	private void OnEnable () {
		image = GetComponent<Image>();
		Generate();
	}
	
	public void Generate () {
		if(automatic) {
			width = height = Mathf.RoundToInt(GetComponent<RectTransform>().GetSize().Min());
			radius = Mathf.FloorToInt(width * 0.5f);
		}
		Color[] colors = GenerateRoundRect(width, height, radius);
//		Color[] colors = GenerateCircle(width, height);
		Texture2D texture = TextureX.Create(width, height, colors);
		Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100, 0, SpriteMeshType.FullRect, new Vector4(radius, radius, radius, radius));
		image.sprite = sprite;
		image.type = Image.Type.Sliced;
	}
	
	public Color[] GenerateRoundRect (int width, int height, int radius) {
		if(radius >= Mathf.Max (width, height) * 0.5f) {
			return GenerateCircle(width, height);
		}
		
		Color[] colors = new Color[width * height];
		colors.Fill(Color.clear);
		for (int y = radius; y < height - radius; y++) {
			for (int x = 0; x < width; x++) {
				colors[y * width + x] = color;
			}
		}
		
		for (int y = 0; y < height; y++) {
			for (int x = radius; x < width - radius; x++) {
				colors[y * width + x] = color;
			}
		}
		
		int startX = width - radius*2;
		int startY = height - radius*2;
		int circleWidth = radius*2;
		int circleHeight = radius*2;
		GenerateCircle(width, height, startX, startY, circleWidth, circleHeight, ref colors);
		startY = 0;
		GenerateCircle(width, height, startX, startY, circleWidth, circleHeight, ref colors);
		startX = 0;
		startY = 0;
		GenerateCircle(width, height, startX, startY, circleWidth, circleHeight, ref colors);
		startY = height - radius*2;
		GenerateCircle(width, height, startX, startY, circleWidth, circleHeight, ref colors);
		return colors;
	}
	
	public void GenerateCircle (int canvasWidth, int canvasHeight, int startX, int startY, int circleWidth, int circleHeight, ref Color[] colors) {
		float antiAliasingFraction = 0.1f;
		float halfAntiAliasingFraction = antiAliasingFraction * 0.5f;
		
		for (int _y = startY; _y < circleHeight + startY; _y++) {
			for (int _x = startX; _x < circleWidth + startX; _x++) {
				float _width = (circleWidth+1) * 0.5f;
				float _height = (circleHeight+1) * 0.5f;
				float x = _x - (circleWidth-1) * 0.5f - startX;
				float y = _y - (circleHeight-1) * 0.5f - startY;
				
				float dx = x / _width;
				float dy = y / _height;
				float val = dx*dx+dy*dy;
				if(val < 1-halfAntiAliasingFraction) {
					colors[_y * canvasWidth + _x] = color;
				} else if (val > 1+halfAntiAliasingFraction) {
					colors[_y * canvasWidth + _x] = colors[_y * canvasWidth + _x];
				} else {
					float normalizedVal = Mathf.InverseLerp(1-halfAntiAliasingFraction, 1+halfAntiAliasingFraction, val);
					colors[_y * canvasWidth + _x] = Color.Lerp (color, colors[_y * canvasWidth + _x], normalizedVal);
				}
			}
		}
	}
	
//	http://stackoverflow.com/questions/10322341/simple-algorithm-for-drawing-filled-ellipse-in-c-c
	public Color[] GenerateCircle (int width, int height) {
		float antiAliasingFraction = 0.1f;
		float halfAntiAliasingFraction = antiAliasingFraction * 0.5f;
		Color[] colors = new Color[width * height];
		for (int _y = 0; _y < height; _y++) {
			for (int _x = 0; _x < width; _x++) {
				float _width = (width+1) * 0.5f;
				float _height = (height+1) * 0.5f;
				float x = _x - (width-1) * 0.5f;
				float y = _y - (height-1) * 0.5f;
				
//				if(x*x*height*height+y*y*width*width <= height*height*width*width)
//					colors[_y * _width + _x] = Color.white;
//				else
//					colors[_y * _width + _x] = Color.clear;
				
				float dx = x / _width;
				float dy = y / _height;
//				Debug.Log (dx*dx+dy*dy);
				float val = dx*dx+dy*dy;
				if(val < 1-halfAntiAliasingFraction) {
					colors[_y * width + _x] = Color.white;
				} else if (val > 1+halfAntiAliasingFraction) {
					colors[_y * width + _x] = Color.clear;
				} else {
					float normalizedVal = Mathf.InverseLerp(1-halfAntiAliasingFraction, 1+halfAntiAliasingFraction, val);
					colors[_y * width + _x] = Color.Lerp (Color.white, Color.clear, normalizedVal);
				}
				
				
//				if(dx*dx+dy*dy <= 1)
//					colors[_y * _width + _x] = Color.white;
//				else
//					colors[_y * _width + _x] = Color.clear;
//				
			}
		}
		return colors;
	}
}

public class Bitmap {
	Color[] pixels;
	
	public Bitmap () {
	
	}
	
	public static int GridPointToArrayIndex (int x, int y, int width){
		return y * width + x;
	}
	
	public void GenerateRectangle (int x, int y, int _width, int _height) {
		
	}
	
	public void GenerateCircle (int xPos, int yPos, int width, int height) {
		float antiAliasingFraction = 0.1f;
		float halfAntiAliasingFraction = antiAliasingFraction * 0.5f;
		Color[] colors = new Color[width * height];
		for (int _y = 0; _y < height; _y++) {
			for (int _x = 0; _x < width; _x++) {
				float _width = (width+1) * 0.5f;
				float _height = (height+1) * 0.5f;
				float x = _x - (width-1) * 0.5f;
				float y = _y - (height-1) * 0.5f;
				
				float dx = x / _width;
				float dy = y / _height;
				
				float val = dx*dx+dy*dy;
				if(val < 1-halfAntiAliasingFraction) {
					colors[_y * width + _x] = Color.white;
				} else if (val > 1+halfAntiAliasingFraction) {
					colors[_y * width + _x] = Color.clear;
				} else {
					float normalizedVal = Mathf.InverseLerp(1-halfAntiAliasingFraction, 1+halfAntiAliasingFraction, val);
					colors[_y * width + _x] = Color.Lerp (Color.white, Color.clear, normalizedVal);
				}			
			}
		}
	}
}