using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(RoundRectGenerator))]
public class RoundRectGeneratorEditor : BaseEditor<RoundRectGenerator> {	
	public override void OnInspectorGUI() {
		serializedObject.Update();
		
		if(GUILayout.Button("Refresh")) {
			data.Generate();
		}
		
		EditorGUILayout.PropertyField(serializedObject.FindProperty("color"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("automatic"));
		if(data.automatic) {
			GUI.enabled = false;
		}
		EditorGUILayout.PropertyField(serializedObject.FindProperty("width"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("height"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("radius"));
		GUI.enabled = true;
		
		EditorGUILayout.PropertyField(serializedObject.FindProperty("outline"));
		if(data.outline) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("outlineColor"));
		}
		
		serializedObject.ApplyModifiedProperties();
	}
}