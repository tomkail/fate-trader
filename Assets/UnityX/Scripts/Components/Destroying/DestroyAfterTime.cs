﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {
	public bool startAutomatically = true;
	public float time = 1f;
	public Timer timer;
	
	public void StartTimer (float time) {
		this.time = time;
		timer = new Timer(this.time);
		timer.Start();
	}
	
	void Start () {
		if(startAutomatically)
			StartTimer(time);
	}
	
	void Update () {
		timer.Loop ();
		if(timer.timerState == TimerState.Stopped) {
			Destroy(gameObject);
		}
	}
}
