﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(QualityManager))]
public class QualityManagerEditor : BaseEditor<QualityManager> {
	
	public static bool showQualitySettingToggles = false;
	
	public override void OnInspectorGUI() {
		EditorGUILayout.HelpBox("Current Quality: "+QualityManager.QualitySettingName+" ("+QualitySettings.GetQualityLevel()+"/"+(QualitySettings.names.Length-1)+")", MessageType.None);
		
		bool overrideQS = EditorGUILayout.Toggle(new GUIContent("Override Detected Quality Settings", "Override Detected Quality Settings"), QualityManager.OverrideDetectedQualitySetting);
		if(overrideQS != QualityManager.OverrideDetectedQualitySetting) {
			QualityManager.OverrideDetectedQualitySetting = overrideQS;
		}
		if(overrideQS) {
			int overridenQL = Mathf.Clamp(EditorGUILayout.IntField(new GUIContent("Override Quality Level", "Override Quality Level"), QualityManager.OverridenQualityLevel), 0, QualitySettings.names.Length);
			if(overridenQL != QualityManager.OverridenQualityLevel) QualityManager.OverridenQualityLevel = overridenQL;
		}
		
		showQualitySettingToggles = EditorGUILayout.Foldout(showQualitySettingToggles, "Quality");
		if(showQualitySettingToggles) {
			EditorGUI.indentLevel++;
			for(int i = 0; i < QualitySettings.names.Length; i++) {
				if(QualitySettings.GetQualityLevel() == i) {
					if(EditorGUILayout.Toggle(QualitySettings.names[i], true))
						QualitySettings.SetQualityLevel(i);
				} else {
					if(EditorGUILayout.Toggle(QualitySettings.names[i], false))
						QualitySettings.SetQualityLevel(i);
				}
			}
			EditorGUI.indentLevel--;
		}
		
		data.automaticQualityDetector = EditorGUILayout.ObjectField("Automatic Quality Detector", data.automaticQualityDetector, typeof(QualityManagerAutoDetector), true) as QualityManagerAutoDetector;
		
		EditorUtility.SetDirty(target);
	}
}
