﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Manages the quality settings used in the game, depending on platform and Unity licence.
/// </summary>
public class QualityManager : MonoSingleton<QualityManager> {
	
	private static bool overrideAutomaticQualitySetting = false;
	public static bool OverrideDetectedQualitySetting {
		get {
			return overrideAutomaticQualitySetting;
		}
		set {
			overrideAutomaticQualitySetting = value;
			if(overrideAutomaticQualitySetting) {
				savedQualityLevel = QualitySettings.GetQualityLevel();
			} else {
				ApplyQualitySetting(savedQualityLevel);
			}
			ApplyAutoDetectQualitySetting();
		}
	}

	private static int overridenQualityLevel = 0;
	public static int OverridenQualityLevel {
		get {
			return overridenQualityLevel;
		}
		set {
			overridenQualityLevel = value;
			ApplyAutoDetectQualitySetting();
		}
	}
	
	private static int savedQualityLevel = 0;
	
	public static string QualitySettingName {
		get {
			return QualitySettings.names[QualitySettings.GetQualityLevel()];
		}
	}
	
	public QualityManagerAutoDetector automaticQualityDetector;
	
	public delegate void OnChangeQualitySettingsEvent(int lastQualitySetting, int newQualitySetting);
	public static event OnChangeQualitySettingsEvent OnChangeQualitySettings;
	
	public override void Init () {
		ApplyAutoDetectQualitySetting();
	}
	
	public static void ApplyAutoDetectQualitySetting () {
		ApplyQualitySetting(Instance.AutoDetectQualitySetting());
	}
	
	/// <summary>
	/// Applies the quality setting.
	/// </summary>
	/// <param name="_qualitySetting">_quality setting.</param>
	public static void ApplyQualitySetting (int _qualitySetting) {
		int lastQualityLevel = QualitySettings.GetQualityLevel();
		QualitySettings.SetQualityLevel(_qualitySetting, true);
		
		if(QualityManager.OnChangeQualitySettings != null)
			QualityManager.OnChangeQualitySettings(lastQualityLevel, _qualitySetting);
	}
	
	
	private int AutoDetectQualitySetting () {
		if(overrideAutomaticQualitySetting) 
			return overridenQualityLevel;
		if(automaticQualityDetector != null) {
			return automaticQualityDetector.AutoDetectQualitySetting();
		} else {
			return QualitySettings.GetQualityLevel();
		}
	}
}