﻿using UnityEngine;
using System.Collections;

public class EnforceChildIndex : MonoBehaviour {
	public enum SetChildIndexMode {
		First,
		Last,
		Index,
		Fraction
	}
	public SetChildIndexMode mode;
	public int index;
	[Range(0,1)]
	public float fraction;
	
	void LateUpdate () {
		RefreshChildIndex();
	}
	
	private void RefreshChildIndex () {
		switch (mode) {
			case SetChildIndexMode.First:
				transform.SetAsFirstSibling();
				break;
			case SetChildIndexMode.Last:
				transform.SetAsLastSibling();
				break;
			case SetChildIndexMode.Index:
				transform.SetSiblingIndex(index);
				break;
			case SetChildIndexMode.Fraction:
				transform.SetSiblingIndex(Mathf.RoundToInt(fraction * transform.childCount-1));
				break;
		}
	}
}
