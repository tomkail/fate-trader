﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class SmoothFollow2D : MonoBehaviour {
	
	
	public float dampTime = 0.15f;
	public Transform target;
	public Vector2 offset = Vector2.zero;
	public Vector2 viewportOffset = Vector2.zero;
	
	[Vector2Toggle]
	public Vector2 axisToTrack = Vector2.one;
	
	public bool useZ = false;
	
	private Camera _camera;
	private new Camera camera {
		get {
			if(_camera == null)
				camera = GetComponent<Camera>();
			return _camera;
		} set {
			_camera = value;
		}
	}
	
	private Vector3 velocity = Vector3.zero;
	
	void Update () {
		if (!target) return;
		if(dampTime <= 0) {
			SnapToTarget();
		} else {
			transform.position = Vector3.SmoothDamp(transform.position, GetTargetPosition(), ref velocity, dampTime);
		}
	}
	
	[ContextMenu("Snap To Target")]
	public void SnapToTarget () {
		if (!target) return;
		transform.position = GetTargetPosition();
		velocity = Vector3.zero;
	}
	
	public Vector3 GetTargetPosition () {
		Vector3 point = camera.WorldToViewportPoint(target.position);
		Vector3 delta = target.position - camera.ViewportToWorldPoint(new Vector3(0.5f + viewportOffset.x, 0.5f + viewportOffset.y, point.z));
		Vector3 destination = transform.position + delta.SetY (0) + new Vector3(offset.x, offset.y, 0);
		destination.x = axisToTrack.x == 1 ? destination.x : transform.position.x;
		destination.y = axisToTrack.y == 1 ? destination.y : transform.position.y;
		if(useZ) destination = destination.XZY();
		return destination;
	}
}