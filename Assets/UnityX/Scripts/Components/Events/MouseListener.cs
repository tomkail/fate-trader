﻿using UnityEngine;
using System.Collections;

public class MouseListener : MonoBehaviour {

   	public delegate void MouseDownEvent ();
   	public event MouseDownEvent MouseDown;

   	public delegate void MouseDragEvent ();
   	public event MouseDragEvent MouseDrag;

   	public delegate void MouseEnterEvent ();
   	public event MouseEnterEvent MouseEnter;

   	public delegate void MouseExitEvent ();
   	public event MouseExitEvent MouseExit;

   	public delegate void MouseOverEvent ();
   	public event MouseOverEvent MouseOver;

   	public delegate void MouseUpEvent ();
   	public event MouseUpEvent MouseUp;

   	public delegate void MouseUpAsButtonEvent ();
   	public event MouseUpAsButtonEvent MouseUpAsButton;

	void OnMouseEnter () {
		if(MouseEnter != null) MouseEnter();
	}
	
	void OnMouseExit () {
		if(MouseExit != null) MouseExit();
	}
	
	void OnMouseOver () {
		if(MouseOver != null) MouseOver();
	}

	void OnMouseDown () {
		if(MouseDown != null) MouseDown();
	}

	void OnMouseDrag () {
		if(MouseDrag != null) MouseDrag();
	}

	void OnMouseUp () {
		if(MouseUp != null) MouseUp();
	}

	void OnMouseUpAsButton () {
		if(MouseUpAsButton != null) MouseUpAsButton();
	}
}
