﻿using UnityEngine;
using System.Collections;

public class DeviceManager : MonoBehaviour {
	public static DeviceManager inst;

	private DeviceOrientation lastDeviceOrientation;
	public delegate void OnDeviceOrientationChangeEvent();
	public event OnDeviceOrientationChangeEvent OnDeviceOrientationChange;

	void Awake () {
		lastDeviceOrientation = Input.deviceOrientation;
	}
	
	void Update () {
		CheckOrientationChange();
	}

	private void CheckOrientationChange(){
		if (Input.deviceOrientation != lastDeviceOrientation){
			lastDeviceOrientation = Input.deviceOrientation;
			if(OnDeviceOrientationChange != null){
				OnDeviceOrientationChange();
			}
		}
	}
}
