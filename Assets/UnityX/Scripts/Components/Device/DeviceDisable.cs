﻿using UnityEngine;
using System.Collections;

public class DeviceDisable : MonoBehaviour {
	public DeviceDisableParameters parameters;
	public Component[] components;
	
	void Start () {
	
	}
	
	void Update () {
	
	}
}

[System.Serializable]
public class DeviceDisableParameters {
	public bool ifMobile = false;
	public bool ifDesktop = false;

	//This tickbox applies to the following:
	public bool ifDevice = false;
	//This tickbox applies to the following:
	public bool ifIOS = false;

	public bool iPhone1 = false;
	public bool iPhone2 = false;
	public bool iPhone3 = false;
	public bool iPhone4 = false;
	public bool iPhone4s = false;
	public bool iPhone5 = false;
	public bool iPhone5s = false;
	public bool iPad1 = false;
	public bool iPad2 = false;
	public bool iPad3 = false;
	public bool iPad4 = false;
	public bool iPadMini = false;
	public bool iPadAir1 = false;
	public bool iPadAir2 = false;

	//This tickbox applies to the following:
	public bool ifAndroid = false;

	//This tickbox applies to the following:
	public bool ifSpecs = false;
	//in MB
	public int minMemory = 1024;
	public int minCores = 1;
	public int minGraphics = 512;
}