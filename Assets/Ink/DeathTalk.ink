VAR player_death_alignment = 0
VAR death_card = false
VAR player_card = false

-> death_welcomes

=== death_welcomes

death-Welcome, it is not often one of your kind comes to me.

*	player-Who are you?
	death-I am Death, of course.
*	player-Where am I?
	death-The place between life and death. You are passing over. I am here to guide you.

- player-I am dying?
death-No my dear, you are already dead. This is the place between. There is no going back from here.
*	player-I can't be dead.
	death-Death is unknown to us all at some point, and only after are we all too familiar with it.
*	player-I see.
	death-I am not surprised that one of you is a lot more accepting than most.

-death-Ah, I see you still have your cards with you. This is unusual.

death-I do not believe they will help you beyond this, but it is good to see that what they say about your kind is true.

*	player-I want to read who you are.
	
	death-Surely reading Death has an easy answer.
	
	player-Let us see.
*	player-They must have a purpose here.
	
	death-Perhaps. Are you suggesting something?
	
	player-Do you desire to know your past, present and future?
	
- death-Amusing. The future of those who pass through here is easy to predict, but I would like to know more about the past of one such as yourself.

death-You may read me, provided I may read you in return.
*	player-You know how?
	death-I know the meaning of the cards. Surely you can help me with the rest.
	**	player-How have you learnt that?[] Those are closely guarded secrets.
		death-You learn much from guiding people. And I have guided the dead for some time.
	**	player-Let us begin, then.
		-> reading_begins
*	player-Very well.
	-> reading_begins


=== reading_begins

death-If I remember correctly, it is four cards.

death-That is some very elaborate shuffling.

*	player-The cards need to be aligned with Fate.
*	player-It's mainly theatrics[.], but this is how I do readings.

death-Do the cards have to be so perfectly aligned? Or can I nudge one? Or would that disturb Fate?

player-It's a lot easier this way.

player-Let's see your first card.
->first_death_card


=== first_death_card

= start
player-We are looking into your past.

death-Surprised?

*	player-Not at all.
	death-Really? That surprises me. Tell me what it means.
	-> interpretation
*	player-I didn't know what to expect.
	death-Will you tell me what it means, then?
	-> interpretation

= interpretation
player-The High Priestess represents someone keeping watch. A guardian.

death-It sounds fairly descriptive.

*	player-Who are you guarding?
	death-The dead, certainly. Although your card might be looking further back than that.
	player-The High Priestess represents an earthly force.
*	player-No, there is more.
	player-The High Priestess is an earthly figure, the divine's agent on Earth.
	death-Of course I am an enemy of the divine. One of the very things the agents of chaos seek is the eradication of death.
	**	player-You consider yourself an agent of the divine?
		death-I very much am. Without me there would be no renewal, only eternal decay.
		player-Does that make you a guardian of
	**	player-So Death itself is an earthly force?
		death-I hardly think so.

- player-So you were not always here, guarding the dead in this place?

death-At some point I was very much an earthly force, yes.

death-Our place in the great cycle can often shift and change. Being one thing today does not mean you will be the same tomorrow.

*	player-You were not always Death?
	-> not_always_death
*	player-What is your place in the Great Cycle?
//	-> place_in_cycle

= not_always_death

death-Indeed, I was not.

death-There is no beginning, and no end, so there is no telling how many came before, or how many will come after. But for now, I am Death.

*	player-What were you before?
	death-Someone very different. Nothing teaches you more than the wisdom of the dead.
*	player-How did you become Death?
	death-Why don't you see if your cards can tell you that?

- death-But first I want to see what the cards tell about you.

-> first_player_card

=== first_player_card

death-Isn't that interesting.

*	player-Do you know how to interpret?
	death-Not at all. You tell me.
*	player-Yes, it is.
	death-I'm looking forwards to hearing this one.

//TESTING:
-> second_death_card
//- player-



=== second_death_card

death-Ah, that seems disconcerting.

player-A great struggle.

death-The greatest struggle.

player-Divine order against eternal chaos.

death-Everything is part of it. Even Death.

*	player-This struggle was very real to you.
	death-It remains very real to me.
	player-You were close to the divine. Is that why you were chosen for this?
	death-Anyone can make wrong choices.
	-> wrong_choices
*	player-

= wrong_choices

player-What were the choices you made?

death-I am used to seeing regret. I can live with myself. Or rather...

*	player-It is not becoming Death you regret.
	death-Oh no, my regrets were why I did not even care if I became Death.
	**	player-Have you cared since?
		death-I do my part in the Great Cycle. There is no reason to regret.
	**	player-What were you then, before?[] To have such a great regret.
		death-I was someone quite similar. A servant of those who needed it. 
*	player-You paid dearly for your mistakes.
	death-Ah, you cannot begin to imagine. A loss far greater than dying. Of all the fates I have seen pass, mine is yet the only one I would undo.
	**	player-That sounds self-absorbed.
		death-It is the truth. And it is not my suffering I would assuage.
	**	player-And who paid the price?
		death-Someone else, yes. Those I was meant to serve.

//TESTING:
-> fourth_death_card

/*
=== second_player_card
-> DONE

=== third_death_card
-> DONE

== third_player_card
-> DONE
*/
=== fourth_death_card

death-This should surprise no one.

death-Our fates are what they are. This is mine, just as your fourth card will never be turned. This is the end for you.

*	player-Even Death has a place in the great cycle.[] If you are not the first Death, you cannot be the last.
	death-Indeed, but this is the fate I am bound to. I cannot right my wrongs, and I will not leave.
	player-Until someone else takes on your Fate.
	death-Until someone else takes on my Fate.
	-> the_trade
*	player-Death completes my part in the Great Cycle.
	death-So it is for us all. Even for me one day. Until then I live with my mistakes.
//	-> death_ending

//TESTING:
-> the_trade

=== the_trade

->END
