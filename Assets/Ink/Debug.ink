-> death_welcomes

=== death_welcomes

death-Welcome, it is not often one of your kind comes to me.

*	player-Who are you?
	-- death-I am Death, of course.
	-- REVEAL
	** 1
		--- death-The place between life and death. You are passing over. I am here to guide you.
		*** FLIP
			---- death-Flipped
		*** Talk
			---- death-Talk
	** 2
- death-END
-> END