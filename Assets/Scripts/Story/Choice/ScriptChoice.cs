using UnityEngine;
using System.Collections;
using Ink.Runtime;

public class ScriptChoice : ScriptLine {
	private Choice choice;
	public int choiceIndex {get; private set;}
	
	public ScriptChoice (Choice choice, int choiceIndex) : base () /* : base (choice.choiceText)*/ {
		this.choice = choice;
		this.choiceIndex = choiceIndex;
	}
	
	public override void Trigger () {
//		if(choiceType == ChoiceType.Arrive) {
//			Debug.Log("ARRIVE");
//			GameController.Instance.targetWaypointPath.SetTargetWaypoint(null);
//		} else if(choiceType == ChoiceType.Depart) {
//			Debug.Log("DEPART");
//			GameController.Instance.targetWaypointPath.SetTargetWaypoint(((IWaypointUser)subject.subject).waypoint);
//		} else if (choiceType == ChoiceType.Dialogue) {
//			ActionHistoryRecorder.Instance.AddLine(new ActionHistoryItem(choiceIndex));
//		}
//		GameController.Instance.storyReader.MakeChoice(this);
		base.Trigger ();
	}

	public override string ToString () {
		string s = "["+this.GetType()+"]\n";
		s += "Content: "+content+"\n";
		if(subject != null) s += "Subject: "+subject+"\n";
		s += "Choice Type: "+contentType+"\n";
		if(choice != null) s += "Choice: "+choice+"\n";
		s += "Choice Index: "+choiceIndex+"\n";
		s += "Raw Content: "+rawContent+"\n";
		return s;
	}
}