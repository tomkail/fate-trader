﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Ink.Runtime;

public class ScriptChoiceSet {
	
	public ScriptChoice[] choices { get; private set; }
	
	public bool containsDialogue;
	public bool containsLayout;
	public bool containsReveal;
	
	public ScriptChoiceSet (IList<Choice> storyChoices) {
		choices = new ScriptChoice[storyChoices.Count];
		for(int i = 0; i < storyChoices.Count; i++) {
			choices[i] = StoryUtils.ParseChoiceContent(storyChoices[i], i);
		}
//		Choice c = new Choice();
//		c.choiceText = "MOVE";
//		choices[storyChoices.Count-1] = StoryUtils.ParseChoiceContent(c, storyChoices.Count-1);
		
		containsDialogue = GetChoiceWithChoiceTypes(ScriptChoice.ContentType.Dialogue) != null;
		containsLayout = GetChoiceWithChoiceTypes(ScriptChoice.ContentType.Layout) != null;
		containsReveal = GetChoiceWithChoiceTypes(ScriptChoice.ContentType.Reveal) != null;
	}
	
	
	// With raw contenet
	public ScriptChoice GetChoiceWithRawContent (string rawContent) {
		return choices.Where(choice => choice.rawContent == rawContent).FirstOrDefault ();
	}
	
	public ScriptChoice[] GetChoicesWithRawContent (string rawContent) {
		return choices.Where(choice => choice.rawContent == rawContent).ToArray ();
	}
	
	// With choice type
	public ScriptChoice GetChoiceWithChoiceTypes (params ScriptChoice.ContentType[] choiceTypes) {
		return choices.Where(choice => choiceTypes.Contains(choice.contentType)).FirstOrDefault();
	}
	
	public ScriptChoice[] GetChoicesWithChoiceTypes (params ScriptChoice.ContentType[] choiceTypes) {
		return choices.Where(choice => choiceTypes.Contains(choice.contentType)).ToArray();
	}
	
	public bool ContainsChoiceWithChoiceTypes (params ScriptChoice.ContentType[] choiceTypes) {
		return GetChoiceWithChoiceTypes(choiceTypes) != null;
	}
	
	// With choice type
	public ScriptChoice GetChoiceWithoutChoiceTypes (params ScriptChoice.ContentType[] choiceTypes) {
		return choices.Where(choice => !choiceTypes.Contains(choice.contentType)).FirstOrDefault();
	}
	
	public ScriptChoice[] GetChoicesWithoutChoiceTypes (params ScriptChoice.ContentType[] choiceTypes) {
		return choices.Where(choice => !choiceTypes.Contains(choice.contentType)).ToArray();
	}
	
	public bool ContainsChoiceWithoutChoiceTypes (params ScriptChoice.ContentType[] choiceTypes) {
		return GetChoiceWithoutChoiceTypes(choiceTypes) != null;
	}
	
	
	public override string ToString () {
		string s = "[ScriptChoiceSet]\n";
		s += "Can Move: "+containsLayout+"\n";
		s += "Can Arrive: "+containsReveal+"\n";
		s += "Choices: \n";
		foreach(ScriptChoice choice in choices)
			s += "\t"+choice.ToString()+"\n\n";
		return s;
	}
}