using UnityEngine;
using System.Collections;

/// <summary>
/// Script content subject. Used by the owner of script content and the target.
/// </summary>
public class ScriptLineSubject {
	
	/// <summary>
	/// The ID of the subject, if one exists.
	/// </summary>
	public string subjectID;
	
	public ScriptLineSubject () {
		
	}
	
	public ScriptLineSubject (string ID) {
		this.subjectID = ID;
	}
	
	public override string ToString () {
		string s = "[StoryContent]\n";
		s += "ID: "+subjectID+"\n";
		return s;
	}
}
