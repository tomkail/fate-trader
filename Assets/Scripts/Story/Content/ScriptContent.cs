using UnityEngine;
using System.Collections;
using Ink.Runtime;

/// <summary>
/// Script content.
/// TODO - content type enum - dialog, stage direction, misc code commands are all different, currently only supports dialog.
/// </summary>
public class ScriptContent : ScriptLine {
	
	public override void Trigger () {
		TarotSceneController.Instance.storyReader.ContinueContent();
	}
	
	public override string ToString () {
		string s = "["+this.GetType()+"]\n";
		s += "Content: "+content+"\n";
		s += "Content Type: "+contentType+"\n";
		s += "Subject: "+subject+"\n";
		s += "Raw Content: "+rawContent+"\n";
		return s;
	}
}
