﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScriptContentChunk {
	public string rawContent;
	public ScriptContent[] contents;
	public ScriptContent currentContent {
		get {
			return contents[currentContentIndex];
		}
	}
	public int currentContentIndex;
	
	public ScriptContentChunk (string rawContent) {
		this.rawContent = rawContent;
	}
	
	public override string ToString () {
		string s = "[ScriptContentSet]\n";
		s += "Content \n";
		foreach(ScriptContent content in contents)
			s += content.ToString()+"\n";
		s += "Raw Content \n";
		s += rawContent;
		return s;
	}
}