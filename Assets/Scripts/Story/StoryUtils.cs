﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Ink.Runtime;
using System.Text.RegularExpressions;

public static class StoryUtils {

	public const string playerKeyword = "player";
	public const string deathKeyword = "death";
	
	// If these strings are found exactly, then this choice is a special command.
	public static Dictionary<string, ScriptChoice.ContentType> specialCommandsChoiceTypeDictionary = new Dictionary<string, ScriptChoice.ContentType>() {
		{"LAYOUT", ScriptChoice.ContentType.Layout},
		{"REVEAL", ScriptChoice.ContentType.Reveal},
		{"CLEAR", ScriptChoice.ContentType.Clear},
	};

	/// Turns raw content containing many lines into a ScriptContentChunk, creating ScriptContent for each line.
	/// </summary>
	/// <returns>The raw content into script content.</returns>
	/// <param name="rawContent">Raw content.</param>
	public static ScriptContentChunk ParseScriptContentChunk (string rawContent) {
		ScriptContentChunk chunk = new ScriptContentChunk(rawContent);
		
		string[] splitRawContents = rawContent.Split (new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

		var contents = new List<ScriptContent> ();
		foreach(string rawLine in splitRawContents) {
			var parsedContent = ParseContent(rawLine);
			if( parsedContent != null )
				contents.Add(parsedContent);
		}

		chunk.contents = contents.ToArray ();
		
		return chunk;
	}
	
	
	/// <summary>
	/// Turns raw content into ScriptContent.
	/// </summary>
	/// <returns>The content.</returns>
	/// <param name="rawContent">Raw content.</param>
	public static ScriptContent ParseContent (string rawContent) {
		ScriptContent scriptContent = new ScriptContent();
		if(rawContent == null) {
			DebugX.LogError("rawContent is null. This is not allowed.");
			return null;
		}
		scriptContent.rawContent = rawContent;
		if(specialCommandsChoiceTypeDictionary.ContainsKey(scriptContent.rawContent)){
			scriptContent.content = scriptContent.rawContent;
			scriptContent.contentType = specialCommandsChoiceTypeDictionary[scriptContent.rawContent];
		} else if (TryParseScriptLineAsDialogue (scriptContent)) {
			scriptContent.contentType = ScriptContent.ContentType.Dialogue;
		}
		return scriptContent;
	}
	
	/// <summary>
	/// Turns Ink.Runtime.Choice into ScriptChoice.
	/// </summary>
	/// <returns>The choice content.</returns>
	/// <param name="choice">Choice.</param>
	/// <param name="choiceIndex">Choice index.</param>
	public static ScriptChoice ParseChoiceContent (Choice choice, int choiceIndex) {
		ScriptChoice scriptChoice = new ScriptChoice(choice, choiceIndex);
		scriptChoice.rawContent = choice.text;
		
		if(!scriptChoice.rawContent.IsNullOrWhiteSpace()) {
			if(specialCommandsChoiceTypeDictionary.ContainsKey(scriptChoice.rawContent)){
				scriptChoice.content = scriptChoice.rawContent;
				scriptChoice.contentType = specialCommandsChoiceTypeDictionary[scriptChoice.rawContent];
			}
			else if(TryParseScriptLineAsDialogue(scriptChoice)) {
				scriptChoice.contentType = ScriptChoice.ContentType.Dialogue;
			}
		}
		return scriptChoice;
	}
	
	private static bool SetChoiceTypeIfTrue(bool conditional, ScriptChoice scriptChoice, ScriptChoice.ContentType type) {
		if(conditional) 
			scriptChoice.contentType = type;
		return conditional;
	}
	
	/// <summary>
	/// Tries the split raw content as dialog. Dialog is defined in the format "name: content"
	/// </summary>
	/// <returns><c>true</c>, if raw content could be split as dialog, <c>false</c> otherwise.</returns>
	/// <param name="content">ScriptLine.</param>
	private static bool TryParseScriptLineAsDialogue (ScriptLine content) {
		string subjectID = "";
		if(!TrySplitContentBySearchString(content.rawContent, "-", ref subjectID, ref content.content)) return false;
		content.subject = subjectID;
		return true;
	}
	
	public static bool TrySplitContentBySearchString (string rawContent, string searchString, ref string left, ref string right) {
		int firstSpecialCharacterIndex = rawContent.IndexOf(searchString);
		if(firstSpecialCharacterIndex == -1) return false;
		
		left = rawContent.Substring(0, firstSpecialCharacterIndex).Trim();
		right = rawContent.Substring(firstSpecialCharacterIndex+searchString.Length, rawContent.Length-firstSpecialCharacterIndex-searchString.Length).Trim();
		return true;
	}
}