using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void OnTriggerITriggerableEvent(ScriptLine line);

/// <summary>
/// A parsed script line. 
/// </summary>
public class ScriptLine {
	
	/// <summary>
	/// The raw, unparsed content.
	/// </summary>
	public string rawContent;
	
	/// <summary>
	/// The parsed content.
	/// </summary>
	public string content;

	/// <summary>
	/// The subject of the line. In dialog this is the speaker, in a choice this might be a prop.
	/// </summary>
	public string subject;

	public event OnTriggerITriggerableEvent OnTrigger;

	public enum ContentType {
		Dialogue,
		Layout,
		Reveal,
		Clear
	}
	public ContentType contentType;

	public virtual void Trigger () {
		if(OnTrigger != null) OnTrigger(this);
	}
	
	public override string ToString () {
		string s = "["+this.GetType()+"]\n";
		s += "Content: "+content+"\n";
		s += "Subject: "+subject+"\n";
		s += "Raw Content: "+rawContent+"\n";
		return s;
	}
}