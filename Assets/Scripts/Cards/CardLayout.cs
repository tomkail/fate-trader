﻿using UnityEngine;
using System.Collections;

public class CardLayout : MonoBehaviour {

//	void Start () {
//		transform.DestroyAllChildrenImmediate();
//		Card[] cards = new Card[4];
//		for (int i = 0; i < cards.Length; i++) {
//			cards [i] = CardFactory.GetRandomCard();
//		}
//		StartCoroutine(LayoutCardsCR(cards));
//	}

	public void LayoutCards (params Card[] cards) {
		StartCoroutine(LayoutCardsCR(cards));
//		for (int i = 0; i < cards.Length; i++) {
//			LayoutCard(cards [i], i);
//		}
	}

	private CardView LayoutCard (Card card, int index) {
		CardView cardView = ObjectX.InstantiateAsChild(PrefabDatabase.Instance.cardView, transform, false);
		cardView.Init(card, false);
		cardView.animator.Play("Deal "+(index+1));
		return cardView;
	}

	public IEnumerator LayoutCardsCR (params Card[] cards) {
		for (int i = 0; i < cards.Length; i++) {
			Card card = cards [i];
			LayoutCard(card, i);
			yield return new WaitForSeconds(Random.Range(0.075f,0.1f));
		}
	}

	private IEnumerator LayoutCardCR (Card card, Vector3 localPosition) {
		ObjectX.InstantiateAsChild(PrefabDatabase.Instance.cardView, transform, false);
		yield return new WaitForSeconds(0.5f);
	}
}
