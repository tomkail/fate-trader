﻿using UnityEngine;
using System.Collections;

public static class CardFactory {
	public static string[] cards = new string[] {
		"chariot",
		"death",
		"devil",
		"emperor",
		"empress",
		"fool",
		"fortune",
		"hangedman",
		"hermit",
		"judgement",
		"justice",
		"lover",
		"magician",
		"moon",
		"pope",
		"priestess",
		"star",
		"strength",
		"sun",
		"temperance",
		"tower",
		"world"
	};

	public static Card GetRandomCard () {
		return GetCard(cards.Random());
	}

	public static Card GetCard (string name) {
		Sprite image = Resources.Load<Sprite>("Cards/"+name);
		if(image == null) {
			Debug.Log("Texture could not be found with name "+name);
			return null;
		}
		Card card = new Card(name, image);
		return card;
	}
}

public class Card {
	public string name;
	public Sprite image;

	public Card (string name, Sprite image) {
		this.name = name;
		this.image = image;
	}
} 