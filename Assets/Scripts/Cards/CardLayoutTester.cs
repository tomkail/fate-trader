﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CardLayout))]
public class CardLayoutTester : MonoBehaviour {
	public CardLayout cardLayout {
		get {
			return GetComponent<CardLayout>();
		}
	}
	public int numCards = 4;
	[ContextMenu("Test")]
	public void Test () {
		transform.DestroyAllChildrenImmediate();
		Card[] cards = new Card[numCards];
		for (int i = 0; i < cards.Length; i++) {
			cards [i] = CardFactory.GetRandomCard();
		}
		cardLayout.LayoutCards(cards);
	}
}
