﻿using UnityEngine;
using System.Collections;

public class CardView : MonoBehaviour {

	public Card card;

	public bool interactable = false;

	private bool _hovered;
	public bool hovered {
		get {
			return _hovered;
		}
		set {
			_hovered = value;
			rendererAnimator.SetBool("hovered", hovered);
		}
	}

	private bool _flipped = false;
	public bool flipped {
		get {
			return _flipped;
		}
		set {
			if(_flipped == value) 
				return;
			_flipped = value;
			rendererAnimator.SetBool("flipped", flipped);
			if(flipped)
				interactable = false;
		}
	}

	public SpriteRenderer front;
	public SpriteRenderer back;
	public SpriteRenderer glow;

	public Animator animator;
	public Animator rendererAnimator;

	public void Init (Card card, bool flipped) {
		this.card = card;
		this._flipped = flipped;
		front.sprite = card.image;
		gameObject.name = card.name;
	}

	[ContextMenu("Flip")]
	public void Flip () {
		flipped = !flipped;
		if(flipped) 
			hovered = false;
	}

	public void HoverIn () {
		if(interactable) return;
		hovered = true;
	}

	public void HoverOut () {
		if(interactable) return;
		hovered = false;
	}

	void Update () {
		
	}
}
