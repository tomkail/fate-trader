﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Ink.Runtime;
using UnityEngine.UI;

using Debug = UnityEngine.Debug;

public class TarotSceneController : MonoSingleton<TarotSceneController> {
	public TextAsset storyJSON;

	public StoryReader storyReader;
	public TarotInputController inputController;
	public CardLayout cardLayout;

	public ContentView contentViewPrefab;
	public ChoiceGroupView choiceGroupViewPrefab;

	public Transform contentParent;

	public bool hasMadeAChoice = false;

	void Awake () {
		storyReader.OnReadContent += OnReadContent;
		storyReader.OnReadChoices += OnReadChoices;
		inputController.OnFlip += OnFlipCard;

	}

	void Start () {
		storyReader.StartStory(new Story(storyJSON.text));
	}

	void OnReadContent (ScriptContent content) {
		if(content.contentType == ScriptLine.ContentType.Layout) {
			cardLayout.LayoutCards(CardFactory.GetCard("death"), CardFactory.GetCard("death"), CardFactory.GetCard("death"), CardFactory.GetCard("death"));
		} else if(content.contentType == ScriptLine.ContentType.Reveal) {
			inputController.interactable = true;
		} else {
			CreateContentView(content);
		}
	}

	void OnReadChoices (ScriptChoiceSet choices) {
		if(choices.containsLayout) {
			cardLayout.LayoutCards(CardFactory.GetCard("death"), CardFactory.GetCard("death"), CardFactory.GetCard("death"), CardFactory.GetCard("death"));
		} else if(choices.containsReveal) {
			inputController.interactable = true;
		} else if(choices.containsDialogue) {
			var dialogueChoices = choices.GetChoicesWithChoiceTypes(ScriptChoice.ContentType.Dialogue);
		}
	}

	void OnFlipCard (Card card) {
		foreach(Choice choice in storyReader.story.currentChoices) {
			if(choice.text.Contains(card.name)) {
				storyReader.MakeChoice(choice.index);
				return;
			}
		}
		Debug.LogError("No choice for card "+card.name);
	}

	void Update () {
		Debug.Log(storyReader.state);
		if(storyReader.state == StoryState.Content && Input.GetMouseButtonDown(0)) {
			storyReader.ContinueContent();
		} else if(storyReader.state == StoryState.Choice && Input.GetMouseButtonDown(0)) {
			storyReader.MakeChoice(0);
		}
	}

	public void Clear () {
		StopAllCoroutines();
		ClearContent();
	}

	private void ClearContent () {
		for (int i = contentParent.childCount-1; i >= 0; i--) {
			Destroy(contentParent.GetChild(i).gameObject);
		}
	}

	IEnumerator OnAdvanceStory () {
//		if(story.canContinue) {
//			ChoiceGroupView choiceView = null;
//			ChevronButtonView chevronView = null;
//			while(story.canContinue) {
//				string content = story.Continue().Trim();
//				ContentView contentView = CreateContentView(content);
//				if(!story.canContinue) {
//					if(story.currentChoices.Count > 0) {
//						choiceView = CreateChoiceGroupView(story.currentChoices);
//					} else {
//						chevronView = CreateChevronView();
//					}
//
//				}
//				while(contentView.textTyper.typing)
//					yield return null;
//				if(story.canContinue)
//					yield return new WaitForSeconds(Mathf.Min(1.0f, contentView.textTyper.targetText.Length * 0.01f));
//			}
//			if(story.currentChoices.Count > 0) {
//				yield return new WaitForSeconds(1f);
//				choiceView.RenderChoices();
//				yield return new WaitForSeconds(0.5f);
//			} else {
//				chevronView.Render();
//				yield return new WaitForSeconds(2);
//			}
//		} else {
//			yield return new WaitForSeconds(2);
//			CreateChevronView();
//		}
		yield return new WaitForSeconds(2);
	}

	public void ChooseChoiceIndex (int choiceIndex) {
		DestroyEmpties();
		storyReader.MakeChoice(choiceIndex);
		hasMadeAChoice = true;
//		StartCoroutine(OnAdvanceStory());
	}

	private void DestroyEmpties () {
		EmptyView[] emptyViews = contentParent.GetComponentsInChildren<EmptyView>();
		for (int i = emptyViews.Length-1; i >= 0; i--) {
			Destroy(emptyViews[i].gameObject);
		}
	}

	ContentView CreateContentView (ScriptContent content) {
		ContentView contentView = Instantiate(contentViewPrefab);
		contentView.transform.SetParent(contentParent, false);
		contentView.LayoutText(content.content);
		return contentView;
	}

	ChoiceGroupView CreateChoiceGroupView (IList<Choice> choices) {
		ChoiceGroupView choiceGroupView = Instantiate(choiceGroupViewPrefab);
		choiceGroupView.transform.SetParent(contentParent, false);
		choiceGroupView.LayoutChoices(choices);
		return choiceGroupView;
	}

	void ClearText () {

	}
}

public enum StoryState {
	Content,
	Choice
}

