using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Ink.Runtime;
using UnityEngine.UI;

using Debug = UnityEngine.Debug;

/// <summary>
/// Story controller. Parses an Ink file and passes the current state of the story around via events.
/// </summary>
public class StoryReader : MonoBehaviour {
	
	public Story story;
	
	public bool isReading = false;
	
	public StoryState state;
	public ScriptContentChunk contentChunk;
	public ScriptChoiceSet choiceSet;

	#region Events
	// Called when the story starts
	public delegate void OnStartStoryEvent(Story story);
	public event OnStartStoryEvent OnStartStory;
	
	// Called when the story ends
	public delegate void OnEndStoryEvent(Story story);
	public event OnEndStoryEvent OnEndStory;
	
	// Called when the next set of content and choices are parsed - after the last set is cleared, and before the new set is read.
	public delegate void OnParseEvent();
	public event OnParseEvent OnParse;
	
	// Called when a content chunk is read
	public delegate void OnReadContentChunkEvent(ScriptContentChunk contentChunk);
	public event OnReadContentChunkEvent OnReadContentChunk;
	
	// Called when content is advanced
	public delegate void OnFinishContentChunkEvent(ScriptContentChunk contentChunk);
	public event OnFinishContentChunkEvent OnFinishContentChunk;
	
	// Called when a content line is read
	public delegate void OnReadContentEvent(ScriptContent content);
	public event OnReadContentEvent OnReadContent;
	
	// Called when content is advanced
	public delegate void OnContinueContentEvent(ScriptContent content);
	public event OnContinueContentEvent OnContinueContent;
	
	// Called when choices are read
	public delegate void OnReadChoicesEvent(ScriptChoiceSet choices);
	public event OnReadChoicesEvent OnReadChoices;
	
	// Called when a choice is made
	public delegate void OnMakeChoiceEvent(ScriptChoiceSet choices, int choiceIndex);
	public event OnMakeChoiceEvent OnMakeChoice;
	
	#endregion
	
	/// <summary>
	/// Determines whether this instance is in state the specified state.
	/// </summary>
	/// <returns><c>true</c> if this instance is in state the specified state; otherwise, <c>false</c>.</returns>
	/// <param name="state">State.</param>
	public bool IsInState (StoryState state) {
		return (isReading && this.state == state);
	}

	/// <summary>
	/// Begins the story.
	/// </summary>
	public void StartStory (Story story) {
		this.story = story;
		isReading = true;
		if(OnStartStory != null) {
			OnStartStory(story);
		}
		ContinueContent();
	}
	
	/// <summary>
	/// Continues the content.
	/// </summary>
	public void ContinueContent () {
		if(state != StoryState.Content) return;
		if(OnContinueContent != null) OnContinueContent(contentChunk.currentContent);
		this.story.Continue();
		StoryChanged();
	}

	/// <summary>
	/// Makes a choice.
	/// </summary>
	/// <param name="choiceIndex">Choice index.</param>
	public void MakeChoice (int choiceIndex) {
		if(!story.currentChoices.ContainsIndex(choiceIndex)) {
			DebugX.LogError(this, "Index "+choiceIndex+" is not valid as length of choice array is "+story.currentChoices.Count +". Current choice set: "+choiceSet.ToString());
			return;
		}
		if(OnMakeChoice != null) OnMakeChoice(choiceSet, choiceIndex);
		story.ChooseChoiceIndex(choiceIndex);
		StoryChanged();
	}
	
	/// <summary>
	/// Called when the story has changed. This happens when the story starts, and as a result of making a choice.
	/// </summary>
	private void StoryChanged () {
		contentChunk = null;
		choiceSet = null;
//		Debug.Log(story.canContinue+" "+story.currentChoices.IsNullOrEmpty());
		if(story.canContinue) {
			ReadContent(StoryUtils.ParseContent(story.currentText));
		} else if(!story.currentChoices.IsNullOrEmpty()) {
			choiceSet = new ScriptChoiceSet(story.currentChoices);
			ReadChoices();
		} else {
			EndStory();
		}
	}
	
	/// <summary>
	/// Called when the story ends.
	/// </summary>
	private void EndStory () {
		Debug.LogWarning("Ths story just ended! Whoa!");
		isReading = false;
		if(OnEndStory != null) {
			OnEndStory(story);
		}
	}

	private void FinishReadingContentChunk () {
		if(OnFinishContentChunk != null) OnFinishContentChunk(contentChunk);
		if(choiceSet != null) {
			ReadChoices();
		} else {
			EndStory();
		}
	}
	
	private bool ContentValid (ScriptContent content) {
		return true;
	}
	
	private void ReadContent (ScriptContent content) {
		state = StoryState.Content;
		if(OnReadContent != null) OnReadContent(content);
	}
	
	private void ReadChoices () {
		state = StoryState.Choice;
		if(OnReadChoices != null) OnReadChoices(choiceSet);
	}
}