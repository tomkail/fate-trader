﻿using UnityEngine;
using System.Collections;

public class TarotInputController : MonoBehaviour {

	public bool interactable = false;
	private CardView lastCardView;

	public delegate void OnFlipEvent(Card card);
	public event OnFlipEvent OnFlip;

	void Update () {
		RaycastHit hit;
		CardView hitCardView = null;
		if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)) {
			hitCardView = hit.transform.GetComponent<CardView>();
		}

		if(lastCardView != null && lastCardView != hitCardView) {
			lastCardView.HoverOut();
			lastCardView = null;
		}

		if(hitCardView != null) {
			if(lastCardView != hitCardView) {
				lastCardView = hitCardView;
				hitCardView.HoverIn();
			}
			if(Input.GetMouseButtonDown(0) && hitCardView.interactable) {
				hitCardView.Flip();
				if(OnFlip != null)
					OnFlip(hitCardView.card);
			}
		}
	}
}
