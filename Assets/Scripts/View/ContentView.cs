﻿using UnityEngine;
using System.Collections;

public class ContentView : StoryElementView {

	protected override void Awake () {
		base.Awake();
	}

	protected override void Update () {
		base.Update ();
	}

	public override void LayoutText (string content) {
		base.LayoutText (content);
		text.text = content;
	}
}
